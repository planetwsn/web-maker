<?php
require_once($_SERVER['DOCUMENT_ROOT'].'/functions/funcPostsMngt.php');
$posts = getPublishedPosts();
?>
<div class="row">
  <div class="col-xs-3">
    <?php $img = "http://".$_SERVER['SERVER_NAME']."/img/logo.png" ?>
    <img src="<?php echo $img ?>" width="100%" height="100%" style="margin-top:-5px; margin-right:auto; margin-left: auto; display: block;" alt="logo.jpg"></img>
  </div>
  <div class="col-xs-6">
  </br>
    <h4 class="text-center">DEMO WEBSITE</h4>
    <h5 class="text-center">Reusable, Revisable, Recyclable for Research</h5>
  </div>
  <div class="col-xs-3">
    <?php $img = "http://".$_SERVER['SERVER_NAME']."/img/logo.png" ?>
    <img src="<?php echo $img ?>" width="100%" height="100%" style="margin-top:-5px; margin-right:auto; margin-left: auto; display: block;" alt="logo.png"></img>
  </div>
</div>

<div class="row" style="margin-top:15px; padding-left:5%; padding-right:5%;">
  <div class="content">
    <h4>Announcements!</h4>
    <hr>
    <?php if ($posts){
    foreach ($posts as $post): ?>
  	<div style="margin-left: 0px;">
  		<div>
  			<h5><b><?php echo $post['title'] ?></b> - <?php echo $post['body'] ?></h5>
  			<div>
  				<span>Created: <?php echo date("F j, Y ", strtotime($post["created_at"])); ?> <i>Updated: <?php echo date("F j, Y ", strtotime($post["updated_at"]))?></i></span>
  			</div>
        <?php if(isset($_SESSION['login-user'])){
          if (getPrivilegeByAccountName($_SESSION['login-user']) < 3){?>
          <div class="row" style="margin-top:1%">
            <div class="col-xs-3">
              <a style="cursor:pointer;" data-toggle="modal" data-target="#modal-edit-post"><span class="glyphicon glyphicon-pencil"></span>  Edit post</a>
              <?php include($_SERVER['DOCUMENT_ROOT'].'/modals/modalEditPost.php'); ?>
            </div>
            <div class="col-xs-3">
              <a href="procs/procPostsMngt.php?delpost=<?php echo $post['post_id'] ?>"><span class="glyphicon glyphicon-trash"></span>  Delete post</a>
            </div>
            <div class="col-xs-6"></div>
          </div>
        <?php }} ?>
  		</div>
  	</div>
    <hr>
  <?php endforeach; } else{echo "Nothing to display<br>";}?>
    <?php if(isset($_SESSION['login-user'])){
      if (getPrivilegeByAccountName($_SESSION['login-user']) < 3){?>
    <div  style="margin-left: 0px;">
      <div class="row">
        <div class="col-xs-3">
          <a style="cursor:pointer;" data-toggle="modal" data-target="#modal-create-post"><span class="glyphicon glyphicon-plus"></span>  Create post</a>
          <?php include($_SERVER['DOCUMENT_ROOT'].'/modals/modalAddPost.php'); ?>
        </div>
        <div class="col-xs-9"></div>
      </div>
    </div>
      <?php }}?>
  </div>
</div>

<div class="row" style="margin-top:20px;padding-left:5%; padding-right:5%;">
  <h4>Want to browse?</h4>
<?php if(!isset($_SESSION['login-user'])){ ?>
  <p>Sign in through the login button above.</p>
<?php }else{ ?>
  <p>Browse the profile list to begin.</p>
<?php } ?>
</div>
