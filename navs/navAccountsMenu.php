<!-- search bar -->
<div class="row" style="padding-top:50px;padding-left:5%;padding-right:5%;">
  <?php include($_SERVER['DOCUMENT_ROOT'].'/forms/formSearch.php'); ?>
</div>

<?php require_once($_SERVER['DOCUMENT_ROOT'].'/functions/funcGenericFunctions.php');
if(getPrivilegeByAccountName($_SESSION['login-user']) < 3){ ?>
<!-- accounts options -->
<div class="row" style="padding-top:5%;padding-left:5%;padding-right:5%;">
  <div class="menu-group">
    <!-- group heading -->
    <h5 class="text-center">Account Management</h5>

    <!-- add account button -->
    <button type="button" class="btn btn-default btn-sm col-xs-6" data-toggle="modal" data-target="#modal-add-account-element">Add New Account</button>
    <?php include($_SERVER['DOCUMENT_ROOT'].'/modals/modalAddAccount.php'); ?>

    <!-- update account button -->
    <button type="button" class="btn btn-default btn-sm col-xs-6" data-toggle="modal" data-target="#modal-edit-account-element">Update Account</button>
    <?php include($_SERVER['DOCUMENT_ROOT'].'/modals/modalEditAccount.php'); ?>

    <!-- delete account button -->
    <button type="button" class="btn btn-default btn-sm col-xs-6" data-toggle="modal" data-target="#modal-delete-account-element">Remove Account</button>
    <?php include($_SERVER['DOCUMENT_ROOT'].'/modals/modalDeleteAccount.php'); ?>

  </div>
</div>
<?php } ?>
