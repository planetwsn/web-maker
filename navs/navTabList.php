<ul class="nav nav-tabs" role="tablist">
  <li role="presentation" class="active">
    <a href="#personal-info-tab" aria-controls="personal-info-tab" role="tab" data-toggle="tab">Personal Info</a>
  </li>

  <li role="presentation">
    <a href="#child-activities-tab" aria-controls="child-activities-tab" role="tab" data-toggle="tab">Child's Activities</a>
  </li>

  <li role="presentation">
    <a href="#other-info-tab" aria-controls="other-info-tab" role="tab" data-toggle="tab">Other Info</a>
  </li>
</ul>
