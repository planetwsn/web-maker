<?php
if(htmlentities($_SERVER['PHP_SELF'])=='/index.php'){
  echo "<li class=\"active nav-item\"><a href=\"#\">";
}
else {
  echo "<li class=\"nav-item\"><a href=\"../index.php\">";
}
?>
Home</a></li>

<!-- display the rest only when logged in -->
<?php
if(isset($_SESSION['login-user'])){
  require_once($_SERVER['DOCUMENT_ROOT'].'/functions/funcAccountsMngt.php');
  if(htmlentities($_SERVER['PHP_SELF'])=='/pageProfileList.php'){
    echo "<li class=\"active nav-item\"><a href=\"#\">";
  }
  else {
    echo "<li class=\"nav-item\"><a href=\"../pages/pageProfileList.php\">";
  }
?>
Profile List</a></li>
<?php
if(getPrivilegeByAccountName($_SESSION['login-user']) < 2){
  if(htmlentities($_SERVER['PHP_SELF'])=='../pages/pageAccounts.php'){
    echo "<li class=\"active nav-item\"><a href=\"#\">";
  }
  else {
    echo "<li class=\"nav-item\"><a href=\"../pages/pageAccounts.php\">";
  }
?>
Accounts</a></li>
<?php
  }
}
 ?>
