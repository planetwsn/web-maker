-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Jul 27, 2021 at 09:26 AM
-- Server version: 10.4.10-MariaDB
-- PHP Version: 7.3.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `test_db`
--

DELIMITER $$
--
-- Functions
--
DROP FUNCTION IF EXISTS `proper`$$
CREATE DEFINER=`root`@`localhost` FUNCTION `proper` (`str` VARCHAR(128)) RETURNS VARCHAR(128) CHARSET latin1 BEGIN
DECLARE c CHAR(1);
DECLARE s VARCHAR(128);
DECLARE i INT DEFAULT 1;
DECLARE bool INT DEFAULT 1;
DECLARE punct CHAR(17) DEFAULT ' ()[]{},.-_!@;:?/';
SET s = LCASE( str );
WHILE i <= LENGTH( str ) DO   
    BEGIN
SET c = SUBSTRING( s, i, 1 );
IF LOCATE( c, punct ) > 0 THEN
SET bool = 1;
ELSEIF bool=1 THEN
BEGIN
IF c >= 'a' AND c <= 'z' THEN
BEGIN
SET s = CONCAT(LEFT(s,i-1),UCASE(c),SUBSTRING(s,i+1));
SET bool = 0;
END;
ELSEIF c >= '0' AND c <= '9' THEN
SET bool = 0;
END IF;
END;
END IF;
SET i = i+1;
END;
END WHILE;
RETURN s;
END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `account`
--

DROP TABLE IF EXISTS `account`;
CREATE TABLE IF NOT EXISTS `account` (
  `account_id` int(11) NOT NULL AUTO_INCREMENT,
  `account_name` varchar(100) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(32) NOT NULL,
  `email` varchar(50) DEFAULT NULL,
  `phone` varchar(13) DEFAULT NULL,
  `privilege_id` int(1) NOT NULL,
  `photo` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`account_id`),
  UNIQUE KEY `account_id` (`account_id`),
  KEY `FK` (`privilege_id`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `account`
--

INSERT INTO `account` (`account_id`, `account_name`, `username`, `password`, `email`, `phone`, `privilege_id`, `photo`) VALUES
(28, 'Mark Anthony Cabilo', 'macabilo', '75b71aa6842e450f12aca00fdf54c51d', 'macabilo@usc.edu.ph', '09453815567', 3, '/photos/pp/default.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `active_profiles`
--

DROP TABLE IF EXISTS `active_profiles`;
CREATE TABLE IF NOT EXISTS `active_profiles` (
  `log_id` int(255) UNSIGNED NOT NULL AUTO_INCREMENT,
  `profile_id` int(11) NOT NULL,
  PRIMARY KEY (`log_id`),
  KEY `profile_restraint` (`profile_id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `activity`
--

DROP TABLE IF EXISTS `activity`;
CREATE TABLE IF NOT EXISTS `activity` (
  `activity_id` int(11) NOT NULL AUTO_INCREMENT,
  `activity_name` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`activity_id`),
  UNIQUE KEY `activity_id` (`activity_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `address`
--

DROP TABLE IF EXISTS `address`;
CREATE TABLE IF NOT EXISTS `address` (
  `address_id` int(11) NOT NULL AUTO_INCREMENT,
  `street_name` varchar(100) DEFAULT NULL,
  `barangay_id` int(11) DEFAULT NULL,
  `locality_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`address_id`),
  UNIQUE KEY `address_id` (`address_id`),
  KEY `FK` (`barangay_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `barangay`
--

DROP TABLE IF EXISTS `barangay`;
CREATE TABLE IF NOT EXISTS `barangay` (
  `barangay_id` int(11) NOT NULL AUTO_INCREMENT,
  `psgc` varchar(255) DEFAULT NULL,
  `barangay_name` text DEFAULT NULL,
  `locality_id` int(11) NOT NULL,
  `regional_code` varchar(255) DEFAULT NULL,
  `provincial_code` varchar(255) DEFAULT NULL,
  `locality_code` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`barangay_id`),
  KEY `locality` (`locality_id`)
) ENGINE=InnoDB AUTO_INCREMENT=42030 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `document`
--

DROP TABLE IF EXISTS `document`;
CREATE TABLE IF NOT EXISTS `document` (
  `document_id` int(11) NOT NULL AUTO_INCREMENT,
  `document_name` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`document_id`),
  UNIQUE KEY `document_id` (`document_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `education`
--

DROP TABLE IF EXISTS `education`;
CREATE TABLE IF NOT EXISTS `education` (
  `education_id` int(11) NOT NULL AUTO_INCREMENT,
  `level_id` int(11) NOT NULL,
  `last_year_attended` varchar(6) DEFAULT NULL,
  `last_school_attended` varchar(100) DEFAULT NULL,
  `still_studying` varchar(5) NOT NULL,
  PRIMARY KEY (`education_id`),
  UNIQUE KEY `education_id` (`education_id`),
  KEY `FK` (`level_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `educational_level`
--

DROP TABLE IF EXISTS `educational_level`;
CREATE TABLE IF NOT EXISTS `educational_level` (
  `level_id` int(11) NOT NULL AUTO_INCREMENT,
  `level_name` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`level_id`),
  UNIQUE KEY `level_id` (`level_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `inactive_profiles`
--

DROP TABLE IF EXISTS `inactive_profiles`;
CREATE TABLE IF NOT EXISTS `inactive_profiles` (
  `log_id` int(255) UNSIGNED NOT NULL AUTO_INCREMENT,
  `profile_id` int(11) NOT NULL,
  PRIMARY KEY (`log_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `locality`
--

DROP TABLE IF EXISTS `locality`;
CREATE TABLE IF NOT EXISTS `locality` (
  `locality_id` int(255) NOT NULL AUTO_INCREMENT,
  `psgc` varchar(255) DEFAULT NULL,
  `locality_name` text DEFAULT NULL,
  `province_id` int(11) NOT NULL,
  `regional_code` varchar(255) DEFAULT NULL,
  `provincial_code` varchar(255) DEFAULT NULL,
  `locality_code` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`locality_id`),
  KEY `province` (`province_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1648 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `password_reset`
--

DROP TABLE IF EXISTS `password_reset`;
CREATE TABLE IF NOT EXISTS `password_reset` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) NOT NULL,
  `token` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `token` (`token`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

DROP TABLE IF EXISTS `posts`;
CREATE TABLE IF NOT EXISTS `posts` (
  `post_id` int(11) NOT NULL AUTO_INCREMENT,
  `account_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `views` int(11) NOT NULL,
  `image` varchar(255) NOT NULL,
  `body` text NOT NULL,
  `published` tinyint(1) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`post_id`),
  UNIQUE KEY `slug` (`slug`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `privilege`
--

DROP TABLE IF EXISTS `privilege`;
CREATE TABLE IF NOT EXISTS `privilege` (
  `privilege_id` int(1) NOT NULL AUTO_INCREMENT,
  `privilege_name` varchar(20) NOT NULL,
  PRIMARY KEY (`privilege_id`),
  UNIQUE KEY `privilege_id` (`privilege_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `privilege`
--

INSERT INTO `privilege` (`privilege_id`, `privilege_name`) VALUES
(1, 'Admin'),
(2, 'Officer'),
(3, 'Guest');

-- --------------------------------------------------------

--
-- Table structure for table `profile`
--

DROP TABLE IF EXISTS `profile`;
CREATE TABLE IF NOT EXISTS `profile` (
  `profile_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `alias` varchar(20) DEFAULT NULL,
  `age` int(2) DEFAULT NULL,
  `birthdate` date NOT NULL,
  `birthplace` int(11) NOT NULL,
  `gender` varchar(10) NOT NULL,
  `religion` varchar(20) NOT NULL,
  `height` int(11) NOT NULL,
  `weight` int(11) NOT NULL,
  `child_phone` varchar(14) DEFAULT NULL,
  `distinguished_marks` varchar(100) DEFAULT NULL,
  `is_nourished` varchar(15) DEFAULT NULL,
  `fatherstat` varchar(10) NOT NULL,
  `motherstat` varchar(10) NOT NULL,
  `childstat` varchar(20) NOT NULL,
  `reason` varchar(500) NOT NULL,
  `status_id` varchar(10) NOT NULL,
  `address_id` int(11) DEFAULT NULL,
  `education_id` int(11) NOT NULL,
  `guardian_id` int(11) DEFAULT NULL,
  `volunteer_id` int(11) DEFAULT NULL,
  `assessment` varchar(1000) DEFAULT NULL,
  `profile_picture` varchar(255) DEFAULT '/photos/pp/default.jpg',
  `fbody_picture` varchar(255) DEFAULT '/photos/pp/default.jpg',
  `date_created` date NOT NULL,
  PRIMARY KEY (`profile_id`) USING BTREE,
  KEY `education` (`education_id`),
  KEY `guardian` (`guardian_id`),
  KEY `volunteer` (`volunteer_id`),
  KEY `birthplace` (`birthplace`),
  KEY `FK` (`address_id`,`education_id`,`guardian_id`,`volunteer_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `profile_activities`
--

DROP TABLE IF EXISTS `profile_activities`;
CREATE TABLE IF NOT EXISTS `profile_activities` (
  `profile_id` int(11) DEFAULT NULL,
  `activity_id` int(11) DEFAULT NULL,
  KEY `FK` (`profile_id`,`activity_id`),
  KEY `activity` (`activity_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `profile_documents`
--

DROP TABLE IF EXISTS `profile_documents`;
CREATE TABLE IF NOT EXISTS `profile_documents` (
  `profile_id` int(11) DEFAULT NULL,
  `document_id` int(11) DEFAULT NULL,
  KEY `FK` (`profile_id`,`document_id`),
  KEY `document` (`document_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `profile_relatives`
--

DROP TABLE IF EXISTS `profile_relatives`;
CREATE TABLE IF NOT EXISTS `profile_relatives` (
  `profile_id` int(11) DEFAULT NULL,
  `relative_id` int(11) DEFAULT NULL,
  KEY `FK` (`profile_id`,`relative_id`),
  KEY `relative` (`relative_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `profile_sacraments`
--

DROP TABLE IF EXISTS `profile_sacraments`;
CREATE TABLE IF NOT EXISTS `profile_sacraments` (
  `profile_id` int(11) DEFAULT NULL,
  `sacrament_id` int(11) DEFAULT NULL,
  KEY `FK` (`profile_id`,`sacrament_id`),
  KEY `sacrament` (`sacrament_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `profile_sleep_areas`
--

DROP TABLE IF EXISTS `profile_sleep_areas`;
CREATE TABLE IF NOT EXISTS `profile_sleep_areas` (
  `profile_id` int(11) DEFAULT NULL,
  `area_id` int(11) DEFAULT NULL,
  KEY `FK` (`profile_id`,`area_id`) USING BTREE,
  KEY `area` (`area_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `profile_work_areas`
--

DROP TABLE IF EXISTS `profile_work_areas`;
CREATE TABLE IF NOT EXISTS `profile_work_areas` (
  `profile_id` int(11) DEFAULT NULL,
  `area_id` int(11) DEFAULT NULL,
  KEY `FK` (`profile_id`,`area_id`) USING BTREE,
  KEY `area` (`area_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `province`
--

DROP TABLE IF EXISTS `province`;
CREATE TABLE IF NOT EXISTS `province` (
  `province_id` int(11) NOT NULL AUTO_INCREMENT,
  `psgc` varchar(255) DEFAULT NULL,
  `province_name` text DEFAULT NULL,
  `regional_code` varchar(255) DEFAULT NULL,
  `provincial_code` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`province_id`)
) ENGINE=InnoDB AUTO_INCREMENT=89 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `region`
--

DROP TABLE IF EXISTS `region`;
CREATE TABLE IF NOT EXISTS `region` (
  `region_id` int(11) NOT NULL AUTO_INCREMENT,
  `psgc` varchar(255) DEFAULT NULL,
  `region_name` text DEFAULT NULL,
  `region_code` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`region_id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `relationship`
--

DROP TABLE IF EXISTS `relationship`;
CREATE TABLE IF NOT EXISTS `relationship` (
  `relationship_id` int(11) NOT NULL AUTO_INCREMENT,
  `relationship_name` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`relationship_id`),
  UNIQUE KEY `relationship_id` (`relationship_id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `relative`
--

DROP TABLE IF EXISTS `relative`;
CREATE TABLE IF NOT EXISTS `relative` (
  `relative_id` int(11) NOT NULL AUTO_INCREMENT,
  `relative_name` varchar(100) DEFAULT NULL,
  `occupation` varchar(50) DEFAULT NULL,
  `marital_status` varchar(20) DEFAULT NULL,
  `salary` int(10) DEFAULT NULL,
  `remarks` varchar(300) DEFAULT NULL,
  `age` int(2) DEFAULT NULL,
  `relationship_id` int(11) DEFAULT NULL,
  `education_id` int(11) DEFAULT NULL,
  `address_id` int(11) DEFAULT NULL,
  `baddr_id` int(11) DEFAULT NULL,
  `contact` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`relative_id`),
  UNIQUE KEY `relative_id` (`relative_id`),
  KEY `FK` (`relationship_id`,`education_id`),
  KEY `address_constraint` (`address_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sacrament`
--

DROP TABLE IF EXISTS `sacrament`;
CREATE TABLE IF NOT EXISTS `sacrament` (
  `sacrament_id` int(11) NOT NULL AUTO_INCREMENT,
  `sacrament_name` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`sacrament_id`),
  UNIQUE KEY `sacrament_id` (`sacrament_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

DROP TABLE IF EXISTS `settings`;
CREATE TABLE IF NOT EXISTS `settings` (
  `setting_id` int(11) NOT NULL AUTO_INCREMENT,
  `show_active` tinyint(1) DEFAULT 0,
  PRIMARY KEY (`setting_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sleeping_area`
--

DROP TABLE IF EXISTS `sleeping_area`;
CREATE TABLE IF NOT EXISTS `sleeping_area` (
  `area_id` int(11) NOT NULL AUTO_INCREMENT,
  `area_name` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`area_id`),
  UNIQUE KEY `area_id` (`area_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `working_area`
--

DROP TABLE IF EXISTS `working_area`;
CREATE TABLE IF NOT EXISTS `working_area` (
  `area_id` int(11) NOT NULL AUTO_INCREMENT,
  `area_name` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`area_id`),
  UNIQUE KEY `area_id` (`area_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `account`
--
ALTER TABLE `account`
  ADD CONSTRAINT `privilege` FOREIGN KEY (`privilege_id`) REFERENCES `privilege` (`privilege_id`);

--
-- Constraints for table `active_profiles`
--
ALTER TABLE `active_profiles`
  ADD CONSTRAINT `profile_restraint` FOREIGN KEY (`profile_id`) REFERENCES `profile` (`profile_id`);

--
-- Constraints for table `address`
--
ALTER TABLE `address`
  ADD CONSTRAINT `barangay` FOREIGN KEY (`barangay_id`) REFERENCES `barangay` (`barangay_id`);

--
-- Constraints for table `barangay`
--
ALTER TABLE `barangay`
  ADD CONSTRAINT `locality` FOREIGN KEY (`locality_id`) REFERENCES `locality` (`locality_id`);

--
-- Constraints for table `education`
--
ALTER TABLE `education`
  ADD CONSTRAINT `educational_level` FOREIGN KEY (`level_id`) REFERENCES `educational_level` (`level_id`);

--
-- Constraints for table `locality`
--
ALTER TABLE `locality`
  ADD CONSTRAINT `province` FOREIGN KEY (`province_id`) REFERENCES `province` (`province_id`);

--
-- Constraints for table `profile`
--
ALTER TABLE `profile`
  ADD CONSTRAINT `address` FOREIGN KEY (`address_id`) REFERENCES `address` (`address_id`),
  ADD CONSTRAINT `birthplace` FOREIGN KEY (`birthplace`) REFERENCES `address` (`address_id`),
  ADD CONSTRAINT `education` FOREIGN KEY (`education_id`) REFERENCES `education` (`education_id`),
  ADD CONSTRAINT `guardian` FOREIGN KEY (`guardian_id`) REFERENCES `relative` (`relative_id`),
  ADD CONSTRAINT `volunteer` FOREIGN KEY (`volunteer_id`) REFERENCES `account` (`account_id`);

--
-- Constraints for table `profile_activities`
--
ALTER TABLE `profile_activities`
  ADD CONSTRAINT `activity` FOREIGN KEY (`activity_id`) REFERENCES `activity` (`activity_id`);

--
-- Constraints for table `profile_documents`
--
ALTER TABLE `profile_documents`
  ADD CONSTRAINT `document` FOREIGN KEY (`document_id`) REFERENCES `document` (`document_id`);

--
-- Constraints for table `profile_relatives`
--
ALTER TABLE `profile_relatives`
  ADD CONSTRAINT `relative` FOREIGN KEY (`relative_id`) REFERENCES `relative` (`relative_id`);

--
-- Constraints for table `profile_sacraments`
--
ALTER TABLE `profile_sacraments`
  ADD CONSTRAINT `sacrament` FOREIGN KEY (`sacrament_id`) REFERENCES `sacrament` (`sacrament_id`);

--
-- Constraints for table `profile_sleep_areas`
--
ALTER TABLE `profile_sleep_areas`
  ADD CONSTRAINT `sleep` FOREIGN KEY (`area_id`) REFERENCES `sleeping_area` (`area_id`);

--
-- Constraints for table `profile_work_areas`
--
ALTER TABLE `profile_work_areas`
  ADD CONSTRAINT `area` FOREIGN KEY (`area_id`) REFERENCES `working_area` (`area_id`);

--
-- Constraints for table `relative`
--
ALTER TABLE `relative`
  ADD CONSTRAINT `address_constraint` FOREIGN KEY (`address_id`) REFERENCES `address` (`address_id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
