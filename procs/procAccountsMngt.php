<?php
namespace ask;
include_once($_SERVER['DOCUMENT_ROOT'].'/phpmailer/PHPMailer.php');
include_once($_SERVER['DOCUMENT_ROOT'].'/procs/procSessionCheck.php');
include_once($_SERVER['DOCUMENT_ROOT'].'/functions/funcDatabaseConnection.php');
include_once($_SERVER['DOCUMENT_ROOT'].'/functions/funcAccountsMngt.php');
include_once($_SERVER['DOCUMENT_ROOT'].'/functions/funcGenericFunctions.php');

//RECORD NEW ACCOUNT
if(isset($_GET['addacc'])){
  unset($_GET['addacc']);
  try{
    $con = getDatabaseConnection();
    $fname = mysqli_real_escape_string($con, $_POST['fname']);
    $uname = mysqli_real_escape_string($con, $_POST['uname']);
    $pword = mysqli_real_escape_string($con, $_POST['pword']);
    $cnum = mysqli_real_escape_string($con, $_POST['cnum']);
    $email = mysqli_real_escape_string($con, $_POST['email']);
    $priv = mysqli_real_escape_string($con, $_POST['priv']);

    if(!accountExists($uname)){
      $query = "INSERT INTO account (account_id, username, password, account_name, privilege_id, phone, email) VALUES (NULL, '".$uname."', '".md5($pword)."', '".$fname."', ".$priv.", '".$cnum."', '".$email."')";

      if(mysqli_query($con, $query)){
        $_SESSION['query_success'] = "New account added!";
      }else{
        $_SESSION['query_error'] = "Error in: ".die(mysqli_error($con));
      }
    }else{
      $_SESSION['query_error'] = "Account already exists!";
    }
  }catch (Exception $e){
    $_SESSION['query_error']= "Error in saving changes:".die(mysqli_error($con));
  }catch (Error $e){
    $_SESSION['query_error']= "Error in saving changes:".die($e);
  }finally{
    mysqli_close($con);
    header("Location: /../pages/pageAccounts.php");
  }
}

//EDIT ACCOUNT INFO
if(isset($_GET['editacc'])){
  unset($_GET['editacc']);
  try{
    $con = getDatabaseConnection();
    $aid = mysqli_real_escape_string($con, $_POST['aid']);
    $priv = mysqli_real_escape_string($con, $_POST['priv']);

    $query = "UPDATE account SET privilege_id=".$priv." WHERE account_id=".$aid;
    if(mysqli_query($con,$query)){
      $_SESSION['query_success'] = "Account successfully updated!";
    }else{
      $_SESSION['query_error'] = "Error in saving account:".mysqli_error($con);
    }

  }catch(Exception $e){

  }catch(Error $e){

  }finally{
    mysqli_close($con);
    header('Location: /../pages/pageAccounts.php');
  }
}

//REMOVE ACCOUNT
if(isset($_GET['delacc'])){
  unset($_GET['delacc']);
  try{
    $con = getDatabaseConnection();
    if(isset($_POST['aid'])){
      $aid = cleanData($con, $_POST['aid']);
      if($aid != "NULL"){
        $query = "DELETE FROM account WHERE account_id=".$aid;
        if(mysqli_query($con, $query)){
          $_SESSION['query_success'] = "Account successfully removed!";
        }
        else{
          $_SESSION['query_error']= "Error in saving changes.";
        }
      }else{
        $_SESSION['query_error'] = "No account was properly selected!";
      }
    }else{
      $_SESSION['query_error'] = "No account was properly selected!";
    }
  }catch(Exception $e){

  }catch(Error $e){

  }finally{
    mysqli_close($con);
    header('Location: /../pages/pageAccounts.php');
  }
}

//UPLOAD PHOTO
if(isset($_GET['uploadPhoto'])){
  unset($_GET['uploadPhoto']);
  try{
    $con = getDatabaseConnection();

    $q = "SELECT photo FROM account WHERE account_name='".$_SESSION['login-user']."'";
    $r = mysqli_query($con, $q);
    $ed = mysqli_fetch_array($r);
    if($ed['photo'] != NULL){

    }

    if(isset($_FILES['photo'])){
      // print_r("pid");
      $file = pathinfo($_FILES['photo']['name']);
      $file_name = "pp-".getAccountIdByAccountName($_SESSION['login-user']).".".$file['extension'];
      $extension = $file['extension'];

      if (($extension != "jpg") && ($extension != "jpeg") && ($extension != "png") && ($extension != "gif")) {
        $_SESSION['query_error']='Only accepts jpg, jpeg, png, and gif';
        return;
      }

      $file_size = $_FILES['photo']['size'];
      $file_tmp_name = $_FILES['photo']['tmp_name'];

      $target_dir=$_SERVER['DOCUMENT_ROOT']."/photos/pp/";
      $data = "/photos/pp/".$file_name;
      if($file_size <= 52428800){
        //resize img
        if($extension=="jpg" || $extension=="jpeg" )
          $src = imagecreatefromjpeg($file_tmp_name);
        else if($extension=="png")
          $src = imagecreatefrompng($file_tmp_name);
        else
          $src = imagecreatefromgif($file_tmp_name);

        list($ow, $oh) = getimagesize($file_tmp_name);

        $nh = 400;
        $nw = ($ow/$oh)*$nh;
        $tmp = imagecreatetruecolor($nw,$nh);

        imagecopyresampled($tmp,$src,0,0,0,0,$nw,$nh,$ow,$oh);

        imagejpeg($tmp,$file_tmp_name,100);
        imagedestroy($src);
        imagedestroy($tmp);

        //copy resized img
        if(move_uploaded_file($file_tmp_name, $target_dir.$file_name)){
          $query = "UPDATE account SET photo='".$data."' WHERE account_name='".$_SESSION['login-user']."'";
          // print_r($query);
          if(mysqli_query($con,$query)){
            $_SESSION['query_success'] = "Photo successfully updated!";
            print_r("tmp:".$file_tmp_name.", tgt:".$target_dir.$file_name);
          }else{
            $_SESSION['query_error'] = "Error in saving profile picture:".mysqli_error($con);
          }
        }else{
          $_SESSION['query_error'] = "Photo upload failed! Consider choosing another picture.";
        }
      }else{
        $_SESSION['query_error'] = "Photo size too large! Consider choosing another picture.";
      }
    }

  }catch(Exception $e){

  }catch(Error $e){

  }finally{
    mysqli_close($con);
    header('Location: /../pages/pageProfile.php');
  }
}

//EDIT ACCOUNT INFO
if(isset($_GET['editpersonal'])){
  unset($_GET['editpersonal']);
  try{
    $con = getDatabaseConnection();
    if(checkInputByPost($_POST) > 0){
      $data = array("name"=>cleanData($con, $_POST['name']),
      "username"=>cleanData($con,$_POST['username']),
      "password"=>cleanData($con,$_POST['password']),
      "email"=>cleanData($con,$_POST['email']),
      "phone"=>cleanData($con,$_POST['phone']));

      $id = getAccountIdByAccountName($_SESSION['login-user']);
      $q = "SELECT * FROM account WHERE account_id=".$id;
      $r = mysqli_query($con,$q);
      $default = mysqli_fetch_array($r);

      $query = "UPDATE account SET ";

      if($data['name'] == "NULL"){
        $query .= "account_name='".$default['account_name']."', ";
      }else{
        $query .= "account_name='".$data['name']."', ";
      }

      if($data['username'] == "NULL"){
        $query .= "username='".$default['username']."', ";
      }else{
        $query .= "username='".$data['username']."', ";
      }

      if($data['password'] == "NULL"){
        $query .= "password='".$default['password']."', ";
      }else{
        $query .= "password='".md5($data['password'])."', ";
      }

      if($data['email'] == "NULL"){
        $query .= "email='".$default['email']."', ";
      }else{
        $query .= "email='".$data['email']."', ";
      }

      if($data['phone'] == "NULL"){
        $query .= "phone='".$default['phone']."' ";
      }else{
        $query .= "phone='".$data['phone']."' ";
      }

      $query .= "WHERE account_id=".$id;
      if(mysqli_query($con,$query)){
        $_SESSION['query_success'] = "Account successfully updated!";
      }else{
        $_SESSION['query_error'] = "Error in saving account:".mysqli_error($con);
      }
    }else{
      $_SESSION['query_error'] = "No changes detected. Nothing was saved.";

    }

  }catch(Exception $e){

  }catch(Error $e){

  }finally{
    mysqli_close($con);
    header('Location: /../pages/pageProfile.php');
  }
}

if(isset($_POST['newpass'])){
  unset($_POST['newpass']);

  $con = getDatabaseConnection();
  $newpass = cleanData($con, $_POST['new-pass']);
  $newpassc = cleanData($con, $_POST['new-pass-c']);


  if($newpass == "NULL" || $newpassc == "NULL"){
    $_SESSION['query_error'] = "Password is required!";
  }else if($newpass != $newpassc){
    $_SESSION['query_error'] = "Passwords do not match!";
  }else if(!isset($_SESSION['token'])){
    $_SESSION['query_error'] = "Token has expired!";
  }else{
    $token = $_SESSION['token'];
    $q = "SELECT email FROM password_reset WHERE token='$token' LIMIT 1";
    $r = mysqli_query($con, $q);
    $email = mysqli_fetch_assoc($r)['email'];

    if($email){
      $newpass = md5($newpass);
      $q = "UPDATE account SET password='$newpass' WHERE email='$email'";
      if(mysqli_query($con, $q)){
        $_SESSION['query_success'] = "Password updated successfully!";
      }else{
        $_SESSION['query_error'] = "Password not saved! ".mysqli_error($con);
      }
      $q = "DELETE FROM password_reset WHERE token='$token'";
      mysqli_query($con, $q);
      print_r($q);
    }else{
      $_SESSION['query_error'] = "Token has expired!";
    }
    unset($_SESSION['token']);
    print_r($_SESSION['query_error']);
  }

  mysqli_close($con);
  header('Location: /../pages/pageLogin.php');
}
?>
