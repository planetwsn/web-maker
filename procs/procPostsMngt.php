<?php
namespace ask;
include_once($_SERVER['DOCUMENT_ROOT'].'/procs/procSessionCheck.php');
include_once($_SERVER['DOCUMENT_ROOT'].'/functions/funcDatabaseConnection.php');
include_once($_SERVER['DOCUMENT_ROOT'].'/functions/funcPostsMngt.php');
include_once($_SERVER['DOCUMENT_ROOT'].'/functions/funcAccountsMngt.php');
include_once($_SERVER['DOCUMENT_ROOT'].'/functions/funcGenericFunctions.php');

//CREATE POST
if(isset($_GET['addpost'])){
  unset($_GET['addpost']);
  try{
    $con = getDatabaseConnection();
    $id = getAccountIdByAccountName($_SESSION['login-user']);
    $title = mysqli_real_escape_string($con, $_POST['title']);
    $body = mysqli_real_escape_string($con, $_POST['body']);
    $slug = preg_replace('/\s+/', '_', $title);

    $query = "INSERT INTO posts (post_id, account_id, title, views, image, body, slug, published, created_at, updated_at) VALUES (NULL, ".$id.", '".$title."', 0, 'None', '".$body."', '".$slug."', 1, CURRENT_TIMESTAMP(), CURRENT_TIMESTAMP())";

    if(mysqli_query($con, $query)){
      $_SESSION['query_success'] = "New announcement added!";
    }else{
      $_SESSION['query_error'] = "Error in: ".die(mysqli_error($con));
    }
  }catch (Exception $e){
    $_SESSION['query_error']= "Error in saving changes:".die(mysqli_error($con));
  }catch (Error $e){
    $_SESSION['query_error']= "Error in saving changes:".die($e);
  }finally{
    mysqli_close($con);
    header("Location: /../index.php");
  }
}

//EDIT POST
if(isset($_GET['editpost'])){
  try{
    $con = getDatabaseConnection();
    $post_id = cleanData($con, $_GET['editpost']);
    unset($_GET['editpost']);
    $id = getAccountIdByAccountName($_SESSION['login-user']);
    $title = mysqli_real_escape_string($con, $_POST['title']);
    $body = mysqli_real_escape_string($con, $_POST['body']);
    $slug = preg_replace('/\s+/', '_', $title);
    $date = getDateCreatedByPostId($post_id);

    $query = "UPDATE posts SET title='".$title."', body='".$body."', slug='".$slug."', created_at='".$date."', updated_at=CURRENT_TIMESTAMP(), account_id=".$id."  WHERE post_id=".$post_id;
    if(mysqli_query($con,$query)){
      $_SESSION['query_success'] = "Post successfully updated!";
    }else{
      $_SESSION['query_error'] = "Error in saving account:".mysqli_error($con);
    }

  }catch(Exception $e){

  }catch(Error $e){

  }finally{
    mysqli_close($con);
    // echo $query;
    header('Location: /../index.php');
  }
}

//DELETE POST
if(isset($_GET['delpost'])){
  try{
    $con = getDatabaseConnection();
    $id = cleanData($con, $_GET['delpost']);
    unset($_GET['delpost']);
    if($id != "NULL"){
      $query = "DELETE FROM posts WHERE post_id=".$id;
      if(mysqli_query($con, $query)){
        $_SESSION['query_success'] = "Post successfully removed!";
      }
      else{
        $_SESSION['query_error']= "Error in saving changes.";
      }
    }else{
      $_SESSION['query_error'] = "No such post existed!";
    }
  }catch(Exception $e){

  }catch(Error $e){

  }finally{
    mysqli_close($con);
    header('Location: /../index.php');
  }
}
?>
