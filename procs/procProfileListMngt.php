<?php
include_once($_SERVER['DOCUMENT_ROOT'].'/procs/procSessionCheck.php');
include_once($_SERVER['DOCUMENT_ROOT'].'/functions/funcDatabaseConnection.php');
include_once($_SERVER['DOCUMENT_ROOT'].'/functions/funcProfileListMngt.php');

//PRINT PROFILE
if(isset($_GET['printprofile'])){
  unset($_GET['printprofile']);
  require_once($_SERVER['DOCUMENT_ROOT'].'/functions/funcPrintFeature.php');
  require_once($_SERVER['DOCUMENT_ROOT'].'/functions/tcpdf/mypdf.php');
  require_once($_SERVER['DOCUMENT_ROOT'].'/functions/funcGenericFunctions.php');
  require_once($_SERVER['DOCUMENT_ROOT'].'/functions/funcAccountsMngt.php');

  $con = getDatabaseConnection();
  $user = getAccountDetailsByAccountName($_SESSION['login-user']);
  $profile = cleanData($con, $_POST['profile']);
  $size = cleanData($con, $_POST['size']);
  $orn = cleanData($con, $_POST['orientation']);

  if(($profile == "NULL") || ($size == "NULL") || ($orn == "NULL")){
    header("Location: /../pages/pageProfileList.php");
  }

  $pdf = new MYPDF($orn, PDF_UNIT, $size, true,'UTF-8',false);

  //document meta information
  $pdf->SetCreator('Foxit Reader');
  $pdf->SetAuthor('Abtanan sa Kaluoy');
  $pdf->SetTitle('Child Intake Form');
  $pdf->SetSubject('Print Intake Form');
  $pdf->SetKeywords('Intake Form, ASK');

  $pdf->SetMargins(25.4,25.4,25.4,25.4);
  $pdf->SetHeaderMargin(0);
  $pdf->SetFooterMargin(0);
  $pdf->SetAutoPageBreak(true, PDF_MARGIN_FOOTER);

  //Add a page
  $pdf->AddPage();

  $w = $pdf->getPageWidth() - 50.8;
  $l = $pdf->getPageHeight();

  $content = '<table id="print-table" style="padding-top:10px;">';
  $data = getProfileByProfileId($profile);

  $q = "SELECT * FROM address WHERE address_id =". $data['address_id'];
  $r = mysqli_query($con, $q);
  $addr = mysqli_fetch_array($r);
  $q = "SELECT * FROM barangay WHERE barangay_id =".$addr['barangay_id'];
  $r = mysqli_query($con, $q);
  $bgy = mysqli_fetch_array($r);
  $q = "SELECT * FROM locality WHERE locality_id =".$bgy['locality_id'];
  $r = mysqli_query($con, $q);
  $loc = mysqli_fetch_array($r);
  $q = "SELECT * FROM province WHERE province_id =".$loc['province_id'];
  $r = mysqli_query($con, $q);
  $pro = mysqli_fetch_array($r);
  $addr_str = ucwords($addr['street_name']).", ".ucwords($bgy['barangay_name']).", ".ucwords($loc['locality_name']).", ".ucwords($pro['province_name']);

  $q = "SELECT DATE_FORMAT('".$data['birthdate']."', '%d %M %Y') AS bd FROM profile WHERE profile_id=".$data['profile_id'];
  $r = mysqli_query($con, $q);
  $bd = mysqli_fetch_array($r, MYSQLI_ASSOC);
  $bd = $bd['bd'];

  $q = "SELECT * FROM address WHERE address_id =". $data['birthplace'];
  $r = mysqli_query($con, $q);
  $addr = mysqli_fetch_array($r);
  $q = "SELECT * FROM barangay WHERE barangay_id =".$addr['barangay_id'];
  $r = mysqli_query($con, $q);
  $bgy = mysqli_fetch_array($r);
  $q = "SELECT * FROM locality WHERE locality_id =".$bgy['locality_id'];
  $r = mysqli_query($con, $q);
  $loc = mysqli_fetch_array($r);
  $q = "SELECT * FROM province WHERE province_id =".$loc['province_id'];
  // print_r("q3:".$q);
  $r = mysqli_query($con, $q);
  $pro = mysqli_fetch_array($r);
  $baddr_str = ucwords($addr['street_name']).", ".ucwords($bgy['barangay_name']).", ".ucwords($loc['locality_name']).", ".ucwords($pro['province_name']);

  $q = "SELECT * FROM education WHERE education_id =".$data['education_id'];
  $r = mysqli_query($con, $q);
  $gdn = mysqli_fetch_array($r);
  $q = "SELECT * FROM educational_level WHERE level_id=".$gdn['level_id'];
  $r = mysqli_query($con, $q);
  $relg = mysqli_fetch_array($r);
  if($gdn['last_year_attended'] == 'N/A'){
    $edu_str = $relg['level_name'];
  }else if($gdn['still_studying'] == 'Yes'){
    $edu_str	= $relg['level_name']." (currently studying)";
  }else{
    $edu_str = $relg['level_name']." (".$gdn['last_year_attended'].", ".ucwords($gdn['last_school_attended']).")";
  }

  $profpic = $_SERVER['SERVER_NAME'].$data['profile_picture']."?".time();
  $bodypic = $_SERVER['SERVER_NAME'].$data['fbody_picture']."?".time();

  $content .= '<thead><tr><td> </td></tr></thead>';
  $content .= '
  <tbody>
    <tr>
      <td style="font-size:18.5; text-align: center; font-weight:bold;">CHILD INTAKE FORM</td>
    </tr>
    <tr>
      <td width="70%"></td>
      <td width="8%" style="font-size:10;">Date:</td>
      <td width="22%" style="font-size:10; border-bottom-width: 1;text-align:center;">'.$data['date_created'].'</td>
    </tr>';

    if($orn == 'L'){
      if($size == 'FOLIO'){
        $content .='<tr>
          <td width="20%"></td>
          <td width="25%"><img src="http://'. $profpic.'" alt="'.$data['name'].' profile picture"></td>
          <td width="10%"></td>
          <td width="25%"><img src="http://'. $bodypic.'" alt="'.$data['name'].' full body picture"></td>
          <td width="20%"></td>
        </tr>';
      }else{
        $content .='<tr>
          <td width="10%"></td>
          <td width="35%"><img src="http://'. $profpic.'" alt="'.$data['name'].' profile picture"></td>
          <td width="10%"></td>
          <td width="35%"><img src="http://'. $bodypic.'" alt="'.$data['name'].' full body picture"></td>
          <td width="10%"></td>
        </tr>';
      }
    }else{
      $content .='<tr>
        <td width="50%"><img src="http://'. $profpic.'" alt="'.$data['name'].' profile picture"></td>
        <td width="50%"><img src="http://'. $bodypic.'" alt="'.$data['name'].' full body picture"></td>
      </tr>';
    }

    $content .='<tr>
      <td width="18%" style="font-size:10;">Child\'s Name:</td>
      <td width="50%" style="font-size:10; border-bottom-width: 1;">'.$data['name'].'</td>
      <td width="12%" style="font-size:10;">  Alias(es):</td>
      <td width="20%" style="font-size:10; border-bottom-width:1;">'.$data['alias'].'</td>
    </tr>
    <tr>
      <td width="21%" style="font-size:10;">Complete Address:</td>
      <td width="79%" style="font-size:10; border-bottom-width:1;">'.$addr_str.'</td>
    </tr>
    <tr>
      <td width="8%" style="font-size:10;">Age:</td>
      <td width="10%" style="font-size:10; border-bottom-width:1;">'.$data['age'].' y.o.</td>
      <td width="17%"></td>
      <td width="10%" style="font-size:10;">Gender:</td>
      <td width="10%" style="font-size:10; border-bottom-width:1;">'.$data['gender'].'</td>
      <td width="8%"></td>
      <td width="11%" style="font-size:10;">Religion:</td>
      <td width="26%" style="font-size:10; border-bottom-width:1;">'.$data['religion'].'</td>
    </tr>
    <tr>
      <td width="9%" style="font-size:10;">Height:</td>
      <td width="11%" style="font-size:10; border-bottom-width:1;">'.$data['height'].' cm</td>
      <td width="15%"></td>
      <td width="10%" style="font-size:10;">Weight:</td>
      <td width="10%" style="font-size:10; border-bottom-width:1;">'.$data['weight'].' kg</td>
      <td width="5%"></td>
      <td width="15%" style="font-size:10;">Nourishment:</td>
      <td width="25%" style="font-size:10; border-bottom-width:1;">'.$data['is_nourished'].'</td>
    </tr>
    <tr>
      <td width="15%" style="font-size:10;">Date of Birth:</td>
      <td width="25%" style="font-size:10; border-bottom-width: 1;">'.$bd.'</td>
      <td width="60%"></td>
    </tr>
    <tr>
      <td width="16%" style="font-size:10;">Place of Birth:</td>
      <td width="84%" style="font-size:10; border-bottom-width:1;">'.$baddr_str.'</td>
    </tr>
    <tr>
      <td width="23%" style="font-size:10;">Distinguished Marks:</td>
      <td width="77%" style="font-size:10; border-bottom-width:1;">'.$data['distinguished_marks'].'</td>
    </tr>
    <tr>
      <td width="33%" style="font-size:10;">Highest Education Attainment:</td>
      <td width="67%" style="font-size:10; border-bottom-width:1;">'.$edu_str.'</td>
    </tr>
    <tr>
      <td width="100%" style="border-bottom-width:thick;"></td>
    </tr>';

  $q = "SELECT * FROM relative WHERE relative_id =".$data['guardian_id'];
  $r = mysqli_query($con, $q);
  $gdn = mysqli_fetch_array($r);
  $q = "SELECT * FROM relationship WHERE relationship_id=".$gdn['relationship_id'];
  $r = mysqli_query($con, $q);
  $relg = mysqli_fetch_array($r);

  $q = "SELECT * FROM address WHERE address_id =". $gdn['address_id'];
  $r = mysqli_query($con, $q);
  $addr = mysqli_fetch_array($r);
  $q = "SELECT * FROM barangay WHERE barangay_id =".$addr['barangay_id'];
  $r = mysqli_query($con, $q);
  $bgy = mysqli_fetch_array($r);
  $q = "SELECT * FROM locality WHERE locality_id =".$bgy['locality_id'];
  $r = mysqli_query($con, $q);
  $loc = mysqli_fetch_array($r);
  $q = "SELECT * FROM province WHERE province_id =".$loc['province_id'];
  // print_r("q3:".$q);
  $r = mysqli_query($con, $q);
  $pro = mysqli_fetch_array($r);
  $gaddr_str = ucwords($addr['street_name']).", ".ucwords($bgy['barangay_name']).", ".ucwords($loc['locality_name']).", ".ucwords($pro['province_name']);

  $content .= '
  <tr>
    <td width="18%" style="font-size:10;">Father Status:</td>
    <td width="12.5%" style="font-size:10; border-bottom-width: 1;">'.$data['fatherstat'].'</td>
    <td width="2%"></td>
    <td width="18%" style="font-size:10;">Mother Status:</td>
    <td width="12.5%" style="font-size:10; border-bottom-width: 1;">'.$data['motherstat'].'</td>
    <td width="2%"></td>
    <td width="25%" style="font-size:10;">Child Living w/ Parents:</td>
    <td width="10%" style="font-size:10; border-bottom-width: 1;">'.$data['childstat'].'</td>
  </tr>
  <tr>
    <td width="22%" style="font-size:10;">Name of Guardian:</td>
    <td width="35%" style="font-size:10; border-bottom-width: 1;">'.ucwords($gdn['relative_name']).'</td>
    <td width="3%"></td>
    <td width="15%" style="font-size:10;">Relationship:</td>
    <td width="25%" style="font-size:10; border-bottom-width: 1;">'.ucwords($relg['relationship_name']).'</td>
  </tr>
  <tr>
    <td width="21%" style="font-size:10;">Guardian Address:</td>
    <td width="79%" style="font-size:10; border-bottom-width:1;">'.$gaddr_str.'</td>
  </tr>';

  if($size == 'FOLIO' && $orn == 'P'){
    $content .= '<tr><td></td></tr><tr><td></td></tr><tr><td></td></tr>';
  }

  $content .='<tr>
    <td width="100%" style="font-size:10;">Family Composition:</td>
  </tr>
  <tr>
    <td width="12.5%" style="font-size:10; border-width: 1 1 1 1; padding: 0 0 0 0; text-align: center; font-weight:bold;">Name</td>
    <td width="7%" style="font-size:10; border-width: 1 1 1 1; padding: 0 0 0 0; text-align: center; font-weight:bold;">Age</td>
    <td width="10.5%" style="font-size:10; border-width: 1 1 1 1; padding: 0 0 0 0; text-align: center; font-weight:bold;">Relation</td>
    <td width="12%" style="font-size:10; border-width: 1 1 1 1; padding: 0 0 0 0; text-align: center; font-weight:bold;">Marital Status</td>
    <td width="12.5%" style="font-size:10; border-width: 1 1 1 1; padding: 0 0 0 0; text-align: center; font-weight:bold;">Educ. Attain.</td>
    <td width="10.5%" style="font-size:9; border-width: 1 1 1 1; padding: 0 0 0 0; text-align: center; font-weight:bold;">Job</td>
    <td width="10%" style="font-size:10; border-width: 1 1 1 1; padding: 0 0 0 0; text-align: center; font-weight:bold;">Monthly Income (Php)</td>
    <td width="12.5%" style="font-size:10; border-width: 1 1 1 1; padding: 0 0 0 0; text-align: center; font-weight:bold;">Contact</td>
    <td width="12.5%" style="font-size:10; border-width: 1 1 1 1; padding: 0 0 0 0; text-align: center; font-weight:bold;">Remarks</td>
  </tr>';

  $q = "SELECT * FROM profile_relatives WHERE profile_id =".$data['profile_id'];
  $rels = mysqli_query($con, $q);
  $n = mysqli_num_rows($rels);
  $rel_str = "";
  while($n > 0){
    $prof = mysqli_fetch_array($rels, MYSQLI_ASSOC);
    $q = "SELECT * FROM relative WHERE relative_id=".$prof['relative_id'];
    $r = mysqli_query($con, $q);
    $rel = mysqli_fetch_array($r);

    $content .= '<tr><td width="12.5%" style="font-size:10; border-width: 1 1 1 1; padding: 0 0 0 0; text-align: center;">'.$rel['relative_name'].'</td>
    <td width="7%" style="font-size:10; border-width: 1 1 1 1; padding: 0 0 0 0; text-align: center;">'.$rel['age'].'</td>';

    $q = "SELECT * FROM relationship WHERE relationship_id=".$rel['relationship_id'];
    $r = mysqli_query($con, $q);
    $rv = mysqli_fetch_array($r);

    $q = "SELECT * FROM educational_level WHERE level_id=".cleanData($con,$rel['education_id']);
    $r = mysqli_query($con, $q);
    if(mysqli_num_rows($r) > 0)
     $edu = mysqli_fetch_array($r);
    else
      $edu = NULL;

    $content .= '<td width="10.5%" style="font-size:10; border-width: 1 1 1 1; padding: 0 0 0 0; text-align: center;">'.$rv['relationship_name'].'</td><td width="12%" style="font-size:10; border-width: 1 1 1 1; padding: 0 0 0 0; text-align: center;">'.$rel['marital_status'].'</td><td width="12.5%" style="font-size:10; border-width: 1 1 1 1; padding: 0 0 0 0; text-align: center;">'.$edu['level_name'].'</td>
    <td width="10.5%" style="font-size:10; border-width: 1 1 1 1; padding: 0 0 0 0; text-align: center;">'.$rel['occupation'].'</td>
    <td width="10%" style="font-size:10; border-width: 1 1 1 1; padding: 0 0 0 0; text-align: center;">'.$rel['salary'].'</td>
    <td width="12.5%" style="font-size:8; border-width: 1 1 1 1; padding: 0 0 0 0; text-align: center;">'.$rel['contact'].'</td>
    <td width="12.5%" style="font-size:10; border-width: 1 1 1 1; padding: 0 0 0 0; text-align: center;">'.$rel['remarks'].'</td>';

    $n--;
    $content .= "</tr>";
  }

  $content .='<tr>
    <td width="100%" style="border-bottom-width:thick;"></td>
  </tr>';

  $q = "SELECT * FROM profile_sleep_areas WHERE profile_id =".$data['profile_id'];
  $rels = mysqli_query($con, $q);
  $n = mysqli_num_rows($rels);
  $slp_str = "";
  while($n > 0){
    $prof = mysqli_fetch_array($rels, MYSQLI_ASSOC);
    $q = "SELECT * FROM sleeping_area WHERE area_id=".$prof['area_id'];
    $r = mysqli_query($con, $q);
    $slp = mysqli_fetch_array($r);
    $slp_str .= ucwords($slp['area_name']);
    if($n != 1){
      $slp_str .= ", ";
    }
    $n--;
  }

  $q = "SELECT * FROM profile_work_areas WHERE profile_id =".$data['profile_id'];
  $rels = mysqli_query($con, $q);
  $n = mysqli_num_rows($rels);
  $wrk_str = "";
  while($n > 0){
    $prof = mysqli_fetch_array($rels, MYSQLI_ASSOC);
    $q = "SELECT * FROM working_area WHERE area_id=".$prof['area_id'];
    $r = mysqli_query($con, $q);
    $slp = mysqli_fetch_array($r);
    $wrk_str .= ucwords($slp['area_name']);
    if($n != 1){
      $wrk_str .= ", ";
    }
    $n--;
  }

  $q = "SELECT * FROM profile_activities WHERE profile_id =".$data['profile_id'];
  $rels = mysqli_query($con, $q);
  $n = mysqli_num_rows($rels);
  $act_str = "";
  while($n > 0){
    $prof = mysqli_fetch_array($rels, MYSQLI_ASSOC);
    $q = "SELECT * FROM activity WHERE activity_id=".$prof['activity_id'];
    $r = mysqli_query($con, $q);
    $slp = mysqli_fetch_array($r);
    $act_str .= ucwords($slp['activity_name']);
    if($n != 1){
      $act_str .= ", ";
    }
    $n--;
  }

  $q = "SELECT * FROM profile_documents WHERE profile_id =".$data['profile_id'];
  $rels = mysqli_query($con, $q);
  $n = mysqli_num_rows($rels);
  $doc_str = "";
  while($n > 0){
    $prof = mysqli_fetch_array($rels, MYSQLI_ASSOC);
    $q = "SELECT * FROM document WHERE document_id=".$prof['document_id'];
    $r = mysqli_query($con, $q);
    $slp = mysqli_fetch_array($r);
    $doc_str .= ucwords($slp['document_name']);
    if($n != 1){
      $doc_str .= ", ";
    }
    $n--;
  }

  $q = "SELECT * FROM profile_sacraments WHERE profile_id =".$data['profile_id'];
  $rels = mysqli_query($con, $q);
  $n = mysqli_num_rows($rels);
  $sac_str = "";
  while($n > 0){
    $prof = mysqli_fetch_array($rels, MYSQLI_ASSOC);
    $q = "SELECT * FROM sacrament WHERE sacrament_id=".$prof['sacrament_id'];
    $r = mysqli_query($con, $q);
    $slp = mysqli_fetch_array($r);
    $sac_str .= ucwords($slp['sacrament_name']);
    if($n != 1){
      $sac_str .= ", ";
    }
    $n--;
  }

  $content .= '
  <tr>
    <td width="21%" style="font-size:10;">Place of Sleep:</td>
    <td width="79%" style="font-size:10; border-bottom-width:1;">'.$slp_str.'</td>
  </tr>
  <tr>
    <td width="21%" style="font-size:10;">Frequented Areas:</td>
    <td width="79%" style="font-size:10; border-bottom-width:1;">'.$wrk_str.'</td>
  </tr>
  <tr>
    <td width="26%" style="font-size:10;">Child\'s Street Activities:</td>
    <td width="74%" style="font-size:10; border-bottom-width:1;">'.$act_str.'</td>
  </tr>
  <tr>
    <td width="13%" style="font-size:10;">Reason(s):</td>
    <td width="87%" style="font-size:10; border-bottom-width:1;">'.$data['reason'].'</td>
  </tr>
  <tr>
    <td width="26%" style="font-size:10;">Child\'s Legal Documents:</td>
    <td width="74%" style="font-size:10; border-bottom-width:1;">'.$doc_str.'</td>
  </tr>
  <tr>
    <td width="26%" style="font-size:10;">Sacraments Received:</td>
    <td width="74%" style="font-size:10; border-bottom-width:1;">'.$sac_str.'</td>
  </tr>
  <tr>
    <td width="100%" style="border-bottom-width:thick;"></td>
  </tr>
  <tr>
    <td width="18%" style="font-size:10;">Child\'s Status:</td>
    <td width="15%" style="font-size:10; border-bottom-width:1; text-align:center;">'.$data['status_id'].'</td>
  </tr>
  <tr>
    <td width="100%" style="font-size:10;">Assessment/Plan of Worker:</td>
  </tr>
  <tr>
    <td width="100%" style="font-size:10; border-bottom-width:1;">'.$data['assessment'].'</td>
  </tr>
  ';

  $content .= '</tbody>';
  $content .= '</table>';
  $pdf->writeHTMLCell($w, 0, 25.4, 51, $content);
  $pdf->Output("ask_child_intake_form_".trim($data['name']." ").".pdf", "I");
}

//RECORD NEW PROFILE
if(isset($_GET['additem'])){
  unset($_GET['additem']);

  require_once($_SERVER['DOCUMENT_ROOT'].'/functions/funcGenericFunctions.php');
  require_once($_SERVER['DOCUMENT_ROOT'].'/functions/funcAddressFunctions.php');
  require_once($_SERVER['DOCUMENT_ROOT'].'/functions/funcAccountsMngt.php');

  try{
    $con = getDatabaseConnection();

    #save all address data to the address table first then use then address id for profile storage
    $caddr = array("addr1" => cleanData($con, $_POST['caddr1']),
    "addr2" => cleanData($con, $_POST['caddr2']),
    "addr3" => 'NULL',
    "addr4" => 'NULL');

    $baddr = array("addr1" => cleanData($con, $_POST['baddr1']),
    "addr2" => cleanData($con, $_POST['baddr2']),
    "addr3" => 'NULL',
    "addr4" => 'NULL');

    if(!checkAddressByArray($caddr)){
      $query = "INSERT INTO address (address_id, street_name, barangay_id) VALUES (NULL, '".$caddr['addr1']."', ".$caddr['addr2'].")";
      if (!mysqli_query($con,$query)){
        $_SESSION['query_error'] = "Error: ".mysqli_error($con)." Original query:".$query;
        // echo "Cid query: ".$query." error: ".mysqli_error($con)."\n";
      }
    }

    if(!checkAddressByArray($baddr)){
      $query = "INSERT INTO address (address_id, street_name, barangay_id) VALUES (NULL, '".$baddr['addr1']."', ".$baddr['addr2'].")";
      if (!mysqli_query($con,$query)){
        $_SESSION['query_error'] = "Error: ".mysqli_error($con)." Original query:".$query;
        // echo "Bid query: ".$query." error: ".mysqli_error($con)."\n";
      }
    }

    $cid = getAddressIdByArray($caddr);
    $bid = getAddressIdByArray($baddr);

    #save education data
    $eduData = array("lvl"=>cleanData($con, $_POST['edu-level']), "stat"=>cleanData($con, $_POST['schoolingstatus']), "yr"=>cleanData($con, $_POST['yearstudied']), "school"=>cleanData($con, $_POST['schoolstudied']));

    if(!checkEducationByArray($eduData)){
      $query = "INSERT INTO education (education_id, level_id, last_year_attended, last_school_attended, still_studying) VALUES (NULL, ".$eduData['lvl'].", '".$eduData['yr']."', '".$eduData['school']."', '".$eduData['stat']."')";
      if (!mysqli_query($con,$query))
        $_SESSION['query_error'] = "Error: ".mysqli_error($con)." Original query:".$query;
    }

    $edu = getEducationIdByArray($eduData);

    #quickly check religion text
    if($_POST['prof-religion'] == 'Other'){
      $religion = cleanData($con, $_POST['prof-religion-new']);
    }else{
      $religion = cleanData($con, $_POST['prof-religion']);
    }

    #save info to profile_list
    $profdata = array("name" => cleanData($con, $_POST['prof-name']),"als" => cleanData($con, $_POST['prof-alias']),"age" => cleanData($con, $_POST['prof-age']),"caddr" => $cid,"bd" => cleanData($con, $_POST['prof-birthdate']),"phone" => cleanData($con, $_POST['prof-phone']),"baddr" => $bid,"gdr" => cleanData($con, $_POST['prof-gender']),"rel" => $religion,"ht" => cleanData($con, $_POST['prof-height']),"wt" => cleanData($con, $_POST['prof-weight']),"dm" => cleanData($con, $_POST['prof-distmarks']),"nrm" => cleanData($con, $_POST['prof-nourishment']),"fstat"=>cleanData($con,$_POST['fatherstat']),"mstat"=>cleanData($con,$_POST['motherstat']),"cstat" => cleanData($con, $_POST['prof-childstat']),"edu" => $edu, "reason"=>cleanData($con,$_POST['reason']),"childstat"=>cleanData($con,$_POST['childparentage']), "assessment"=>cleanData($con,$_POST['assessment']));

    if(!checkProfileByArray($profdata)){
      $query = "INSERT INTO profile (profile_id, name, alias, gender, height, weight,child_phone,address_id, age, birthdate, birthplace, status_id, education_id, volunteer_id, is_nourished, fatherstat, motherstat, distinguished_marks, reason, religion,childstat,assessment,date_created) VALUES (NULL, '".$profdata['name']."', '".$profdata['als']."', '".$profdata['gdr']."', ".$profdata['ht'].", ".$profdata['wt'].",'".$profdata['phone']."', ".$profdata['caddr'].", ".$profdata['age'].", STR_TO_DATE('".$profdata['bd']."','%m/%d/%Y'), ".$profdata['baddr'].",'".$profdata['cstat']."', ".$profdata['edu'].", ". getAccountIdByAccountName($_SESSION['login-user']).", '".$profdata['nrm']."', '".$profdata['fstat']."', '".$profdata['mstat']."', '".$profdata['dm']."', '".$profdata['reason']."','".$profdata['rel']."','".$profdata['childstat']."','".$profdata['assessment']."', CURDATE())";

      if(mysqli_query($con, $query)){
        $_SESSION['query_success']="Profile added successfully!";
        $profile = getProfileByArray($profdata);
      }else{
        $_SESSION['query_error'] = "Error: ".mysqli_error($con)." Original query:".$query;
        // echo "Profile saving error: ".mysqli_error($con)."\nOriginal query: ".$query;
        throw new Exception();
      }
    }else{
      $_SESSION['query_error']="Profile already exists! Check name and other data.";
      $profile = getProfileByArray($profdata);
    }

    #save relatives data first before guardian
    #save relative data if there are any
    $gdn=NULL;
    if((isset($_POST['relativeName'])) &&(!empty($_POST['relativeName']))){
      $relname = $_POST['relativeName'];
      $relage  = $_POST['relativeAge'];
      $relmar  = $_POST['maritalStat'];
      $relrel  = $_POST['relativeRel'];
      $reledu  = $_POST['relEducation'];
      $relocc  = $_POST['relativeOccupation'];
      $relcon  = $_POST['relativeContact'];
      $relinc  = $_POST['relativeIncome'];
      $relrmk  = $_POST['relativeRemarks'];

      $birthaddress = array();

      $reldata = array("name"=>$relname,"age"=>$relage,"marital_stat"=>$relmar,"relationship"=>$relrel,"education"=>$reledu,"occupation"=>$relocc,"contact"=>$relcon, "income"=>$relinc,"remark"=>$relrmk);

      $relatives=array();

      for($c = 0; $c < count($reldata['name']); $c++){
        $raddr = array("addr1" => 'NULL',
        "addr2" => 'NULL',
        "addr3" => cleanData($con, $_POST['relativeAddr2'][$c]),
        "addr4" => cleanData($con, $_POST['relativeAddr1'][$c]));
        // echo implode(",",$raddr);

        if(!checkAddressByArray($raddr)){
          $query = "INSERT INTO address (address_id, street_name, barangay_id, locality_id) VALUES (NULL, NULL, NULL, ".$raddr['addr3'].")";
          if (!mysqli_query($con,$query))
            $_SESSION['query_error'] = "Error: ".mysqli_error($con)." Original query:".$query;
        }
        $relbaddr = getAddressIdByArray($raddr);
        $check = array("name"=>$reldata['name'][$c],"rel"=>$reldata['relationship'][$c]);
        if(!checkRelativeByArray($check)){
          $query = "INSERT INTO relative (relative_id, relative_name,  relationship_id, occupation, salary, remarks, age, education_id, marital_status, baddr_id, contact) VALUES (NULL, '".$reldata['name'][$c]."', ".$reldata['relationship'][$c].", '".$reldata['occupation'][$c]."', ".$reldata['income'][$c].", '".$reldata['remark'][$c]."', ".$reldata['age'][$c].", ".$reldata['education'][$c].",'".$reldata['marital_stat'][$c]."',".$relbaddr.",'".$reldata['contact'][$c]."')";
          if (!mysqli_query($con,$query))
            $_SESSION['query_error'] = "Error: ".mysqli_error($con)." Original query:".$query;
          $rid = getRelativeIdByArray($check);
        }else{
          $rid = getRelativeIdByArray($check);
          $q = "UPDATE relative SET occupation='".$reldata['occupation'][$c]."', salary=".$reldata['income'][$c].", remarks='".$reldata['remark'][$c]."', age=".$reldata['age'][$c].", education_id=".$reldata['education'][$c].", marital_status='".$reldata['marital_stat'][$c]."', contact = '".$reldata['contact'][$c]."', baddr_id=".$relbaddr." WHERE relative_id=".$rid;
          if (!mysqli_query($con,$q))
            $_SESSION['query_error'] = "Error: ".mysqli_error($con)." Original query:".$q;
        }
        $relatives[] = $rid;
      }

      #save into other tables
      foreach($relatives as $entry){
        $check = array("profile"=>$profile, "relative"=>$entry);
        if(!checkProfileRelativesByArray($check)){
          $q = "INSERT INTO profile_relatives (profile_id, relative_id) VALUES (".$profile.",".$entry.")";
          if (!mysqli_query($con,$q)){
            $_SESSION['query_error'] = "Error: ".mysqli_error($con)." Original query:".$q;
            throw new Exception();
          }
        }
      }
    }

    #save guardian data
    if(!isset($_POST['gaddr1'])){
      $gaddr = $cid;
      $q = "SELECT relative_id FROM profile_relatives WHERE profile_id=".$profile;
      $r = mysqli_query($con, $q);
      while($a = mysqli_fetch_array($r)){
        $q = "SELECT relationship_id FROM relative WHERE relative_id=".$a['relative_id'];
        $nr = mysqli_query($con, $q);
        $na = mysqli_fetch_array($nr);
        $gdn = $a['relative_id'];
        if($na['relationship_id'] == 8 || $na['relationship_id'] == 9){
          break;
        }
      }
      $q = "UPDATE relative SET address_id=".$cid;
      if(!mysqli_query($con,$q)){
        $_SESSION['query_error'] = "Set guardian address to child address: ".mysqli_error($con);
        throw new Exception();
      }
    }else{
      $gaddr = array("addr1" => cleanData($con, $_POST['gaddr1']),
      "addr2" => cleanData($con, $_POST['gaddr2']),
      "addr3" => cleanData($con, $_POST['gaddr3']),
      "addr4" => cleanData($con, $_POST['gaddr4']));

      if(!checkAddressByArray($gaddr)){
        $query = "INSERT INTO address (address_id, street_name, barangay_id) VALUES (NULL, '".$gaddr['addr1']."', ".$gaddr['addr2'].")";
        if (!mysqli_query($con,$query))
          $_SESSION['query_error'] = "Error: ".mysqli_error($con)." Original query:".$query;
      }

      $gaddr = getAddressIdByArray($gaddr);

      $gdndata = array("name"=>cleanData($con, $_POST['guardian']), "rel"=>cleanData($con,$_POST['relguardian']), "gaddr"=>$gaddr);

      if(!checkRelativeByArray($gdndata)){
        $query = "INSERT INTO relative (relative_id, relative_name, relationship_id, address_id) VALUES (NULL, '".$gdndata['name']."', ".$gdndata['rel'].", ".$gdndata['gaddr'].")";
        if (!mysqli_query($con,$query))
          $_SESSION['query_error'] = "Error: ".mysqli_error($con)." Original query:".$query;
      }else{
        $query = "UPDATE relative SET address_id=".$gaddr." WHERE relative_id=". getRelativeIdByArray($gdndata);
        if (!mysqli_query($con,$query))
          $_SESSION['query_error'] = "Error: ".mysqli_error($con)." Original query:".$query;
      }
      $entry = getRelativeIdByArray($gdndat);

      $q = "INSERT INTO profile_relatives (profile_id, relative_id) VALUES (".$profile.",".$entry.")";
      if (!mysqli_query($con,$q)){
        $_SESSION['query_error'] = "Error: ".mysqli_error($con)." Original query:".$q;
        throw new Exception();
        // echo $_SESSION['query_error'];
      }
      $gdn = getRelativeIdByArray($gdndata);
    }

    #add guardian data into profile
    if(is_null($gdn)){
      $_SESSION['query_error'] = "No relatives listed. Check stored data for relatives or select child does not live with parents.";
    }else{
      $q = "UPDATE profile SET guardian_id=".$gdn." WHERE profile_id=". $profile;
      if (!mysqli_query($con,$q))
        $_SESSION['query_error'] = "Error: ".mysqli_error($con)." Original query:".$q;
    }

    #place of sleep
    $slumber = $_POST['sleepplace'];

    foreach($slumber as $place){
      if(!checkSleepingAreaByAreaName($place)){
        $q = "INSERT INTO sleeping_area (area_id, area_name) VALUES (NULL, '".$place."')";
        if (!mysqli_query($con,$q))
          $_SESSION['query_error'] = "Error: ".mysqli_error($con)." Original query:".$q;
      }
      $place_id = getSleepAreaIdByAreaName($place);
      $check = array("profile"=>$profile, "area_id"=>$place_id);
      if(!checkProfileSleepAreasByArray($check)){
        $q = "INSERT INTO profile_sleep_areas (profile_id, area_id) VALUES (".$profile.",".$place_id.")";
        if (!mysqli_query($con,$q))
          $_SESSION['query_error'] = "Error: ".mysqli_error($con)." Original query:".$q;
      }
    }

    #frequented areas
    $tambay = $_POST['freqarea'];

    foreach($tambay as $place){
      if(!checkWorkAreaByAreaName($place)){
        $q = "INSERT INTO working_area (area_id, area_name) VALUES (NULL, '".$place."')";
        if (!mysqli_query($con,$q))
          $_SESSION['query_error'] = "Error: ".mysqli_error($con)." Original query:".$q;
      }
      $place_id = getWorkAreaIdByAreaName($place);
      $check = array("profile"=>$profile, "area_id"=>$place_id);
      if(!checkProfileWorkAreasByArray($check)){
        $q = "INSERT INTO profile_work_areas (profile_id, area_id) VALUES (".$profile.",".$place_id.")";
        if (!mysqli_query($con,$q))
          $_SESSION['query_error'] = "Error: ".mysqli_error($con)." Original query:".$q;
      }
    }

    #street activities
    $lingaw = $_POST['streetact'];

    foreach($lingaw as $act){
      if(!checkActivityByActivityName($act)){
        $q = "INSERT INTO activity (activity_id, activity_name) VALUES (NULL, '".$act."')";
        if (!mysqli_query($con,$q))
          $_SESSION['query_error'] = "Error: ".mysqli_error($con)." Original query:".$q;
      }
      $act_id = getActivityIdByActivityName($act);
      $check = array("profile"=>$profile, "activity_id"=>$act_id);
      if(!checkProfileActivitiesByArray($check)){
        $q = "INSERT INTO profile_activities (profile_id, activity_id) VALUES (".$profile.",".$act_id.")";
        if (!mysqli_query($con,$q))
          $_SESSION['query_error'] = "Error: ".mysqli_error($con)." Original query:".$q;
      }
    }

    #documents
    $papers = $_POST['documents'];

    foreach($papers as $pap){
      if(!checkDocumentByDocumentName($pap)){
        $q = "INSERT INTO document (document_id, document_name) VALUES (NULL, '".$pap."')";
        if (!mysqli_query($con,$q))
          $_SESSION['query_error'] = "Error: ".mysqli_error($con)." Original query:".$q;
      }
      $doc_id = getDocumentIdByDocumentName($pap);
      $check = array("profile"=>$profile, "document_id"=>$doc_id);
      if(!checkProfileDocumentsByArray($check)){
        $q = "INSERT INTO profile_documents (profile_id, document_id) VALUES (".$profile.",".$doc_id.")";
        if (!mysqli_query($con,$q))
          $_SESSION['query_error'] = "Error: ".mysqli_error($con)." Original query:".$q;
      }
    }

    #sacraments
    $sacs = $_POST['sacraments'];

    foreach($sacs as $sac){
      if(!checkSacramentBySacramentName($sac)){
        $q = "INSERT INTO sacrament (sacrament_id, sacrament_name) VALUES (NULL, '".$sac."')";
        if (!mysqli_query($con,$q))
          $_SESSION['query_error'] = "Error: ".mysqli_error($con)." Original query:".$q;
      }
      $sac_id = getSacramentIdBySacramentName($sac);
      $check = array("profile"=>$profile, "sacrament_id"=>$sac_id);
      if(!checkProfileSacramentsByArray($check)){
        $q = "INSERT INTO profile_sacraments (profile_id, sacrament_id) VALUES (".$profile.",".$sac_id.")";
        if (!mysqli_query($con,$q))
          $_SESSION['query_error'] = "Error: ".mysqli_error($con)." Original query:".$q;
      }
    }

    $q = "INSERT INTO active_profiles (log_id, profile_id) VALUES (NULL,".$profile.")";
    if (!mysqli_query($con,$q))
      $_SESSION['query_error'] = "Error: ".mysqli_error($con)." Original query: ".$q;

  }catch (Exception $e){
    // echo "Exception has been thrown.";
  }catch (Error $e){
  }finally{
    mysqli_close($con);
    header("Location: /../pages/pageProfileList.php");
  }
}

//EDIT INVENTORY ITEM
if(isset($_GET['edititem'])){
  unset($_GET['edititem']);
  require_once($_SERVER['DOCUMENT_ROOT'].'/functions/funcGenericFunctions.php');
  require_once($_SERVER['DOCUMENT_ROOT'].'/functions/funcAccountsMngt.php');

  try{
    $con = getDatabaseConnection();
    if(checkInputByPost($_POST) > 1){
      $profile_id = cleanData($con, $_POST['profile']);
      $default = getProfileByProfileId($profile_id);

      #update profile
      $query = "UPDATE profile SET ";

      $val = cleanData($con, $_POST['name-new']);
      if($val != "NULL"){
        $query .= "name='".$val."', ";
      }else{
        $query .= "name='".$default['name']."', ";
      }

      $val = cleanData($con, $_POST['alias-new']);
      if($val != "NULL"){
        $query .= "alias='".$val."', ";
      }else{
        $query .= "alias='".$default['alias']."', ";
      }

      #update address if edited
      if((isset($_POST['caddr1-new']))&&(isset($_POST['caddr2-new']))&&(!empty($_POST['caddr1-new']))&&(!empty($_POST['caddr2-new']))){
        $caddr = array("addr1" => cleanData($con, $_POST['caddr1-new']),
        "addr2" => cleanData($con, $_POST['caddr2-new']),
        "addr3" => cleanData($con, $_POST['caddr3-new']),
        "addr4" => cleanData($con, $_POST['caddr4-new']));

        if(!checkAddressByArray($caddr)){
          $q = "INSERT INTO address (address_id, street_name, barangay_id) VALUES (NULL, '".$caddr['addr1']."', ".$caddr['addr2'].")";
          mysqli_query($con, $q);
        }

        $caddr = getAddressIdByArray($caddr);
        $query .= "address_id=".$caddr.", ";
      }else{
        $query .= "address_id=".$default['address_id'].", ";
      }

      $val = cleanData($con, $_POST['age-new']);
      if($val != "NULL"){
        $query .= "age=".$val.", ";
      }else{
        $query .= "age=".$default['age'].", ";
      }

      if(isset($_POST['gender-new'])){
        $val = cleanData($con, $_POST['gender-new']);
        if($val != "NULL"){
          $query .= "gender='".$val."', ";
        }
      }else{
        $query .= "gender='".$default['gender']."', ";
      }

      if(isset($_POST['religion-new'])){
        $val = cleanData($con, $_POST['religion-new']);
        if($val != "NULL"){
          if($_POST['religion-new'] == 'Other'){
            $val = cleanData($con, $_POST['religion-new-text']);
          }else{
            $val = cleanData($con, $_POST['religion-new']);
          }
          $query .= "religion='".$val."', ";
        }
      }else{
        $query .= "religion='".$default['religion']."', ";
      }

      $val = cleanData($con, $_POST['height-new']);
      if($val != "NULL"){
        $query .= "height=".$val.", ";
      }else{
        $query .= "height=".$default['height'].", ";
      }

      $val = cleanData($con, $_POST['weight-new']);
      if($val != "NULL"){
        $query .= "weight=".$val.", ";
      }else{
        $query .= "weight=".$default['weight'].", ";
      }

      $val = cleanData($con, $_POST['childphone-new']);
      if($val != "NULL"){
        $query .= "child_phone='".$val."', ";
      }else{
        $query .= "child_phone='".$default['child_phone']."', ";
      }

      $val = cleanData($con, $_POST['birthdate-new']);
      if($val != "NULL"){
        $query .= "birthdate=STR_TO_DATE('".$val."','%m/%d/%Y'), ";
      }else{
        $query .= "birthdate='".$default['birthdate']."', ";
      }

      if((isset($_POST['baddr1-new']))&&(isset($_POST['baddr2-new']))&&(!empty($_POST['baddr1-new']))&&(!empty($_POST['baddr2-new']))){
        $baddr = array("addr1" => cleanData($con, $_POST['baddr1-new']),
        "addr2" => cleanData($con, $_POST['baddr2-new']),
        "addr3" => cleanData($con, $_POST['baddr3-new']),
        "addr4" => cleanData($con, $_POST['baddr4-new']));

        if(!checkAddressByArray($caddr)){
          $q = "INSERT INTO address (address_id, street_name, barangay_id) VALUES (NULL, '".$baddr['addr1']."', ".$baddr['addr2'].")";
          mysqli_query($con, $q);
        }

        $baddr = getAddressIdByArray($baddr);
        $query .= "birthplace=".$baddr.", ";
      }else{
        $query .= "birthplace=".$default['birthplace'].", ";
      }

      $val = cleanData($con, $_POST['distmarks-new']);
      if($val != "NULL"){
        $query .= "distinguished_marks='".$val."', ";
      }else{
        $query .= "distinguished_marks='".$default['distinguished_marks']."', ";
      }

      if(isset($_POST['edu-level'])){
        $val = cleanData($con, $_POST['edu-level']);

        if($val != "NULL"){
          $eduDef = getEducationByEducationId($default['education_id']);

          $stat = cleanData($con, $_POST['schoolingstatus']);
          if($stat == "NULL"){
            $stat = $eduDef['still_studying'];
          }

          $yr = cleanData($con, $_POST['yearstudied']);
          if($yr == "NULL"){
            $yr = $eduDef['last_year_attended'];
          }

          $school = cleanData($con, $_POST['schoolstudied']);
          if($school == "NULL"){
            $school = $eduDef['last_school_attended'];
          }

          $eduData = array("lvl"=>$val, "stat"=>$stat, "yr"=>$yr, "school"=>$school);

          if(!checkEducationByArray($eduData)){
            $query = "UPDATE education SET level_id=".$eduData['lvl'].", still_studying='".$eduData['stat']."', last_year_attended='".$eduData['yr']."', last_school_attended='".$eduData['school']."' WHERE education_id=".$default['education_id'];
            mysqli_query($con, $query);
          }

          $query .= "education_id=".getEducationIdByArray($eduData).", ";
        }
      }

      if(isset($_POST['nourishment-new'])){
        $val = cleanData($con, $_POST['nourishment-new']);
        if($val != "NULL"){
          $query .= "is_nourished='".$val."'";
        }
      }

      #adding more relatives to the list
      if((isset($_POST['relativeName'])) &&(!empty($_POST['relativeName']))){
        $relname = $_POST['relativeName'];
        $relage  = $_POST['relativeAge'];
        $relmar  = $_POST['maritalStat'];
        $relrel  = $_POST['relativeRel'];
        $reledu  = $_POST['relEducation'];
        $relocc  = $_POST['relativeOccupation'];
        $relcon  = $_POST['relativeContact'];
        $relinc  = $_POST['relativeIncome'];
        $relrmk  = $_POST['relativeRemarks'];

        $reldata = array("name"=>$relname,"age"=>$relage,"marital_stat"=>$relmar,"relationship"=>$relrel,"education"=>$reledu,"occupation"=>$relocc,"income"=>$relinc,"remark"=>$relrmk,"contact"=>$relcon);

        $relatives=array();

        for($c = 0; $c < count($reldata['name']); $c++){
          $check = array("name"=>$reldata['name'][$c],"rel"=>$reldata['relationship'][$c]);
          if(!checkRelativeByArray($check)){
            $q = "INSERT INTO relative (relative_id, relative_name,  relationship_id, occupation, salary, remarks, age, education_id, marital_status, contact) VALUES (NULL, '".$reldata['name'][$c]."', ".$reldata['relationship'][$c].", '".$reldata['occupation'][$c]."', ".$reldata['income'][$c].", '".$reldata['remark'][$c]."', ".$reldata['age'][$c].", ".$reldata['education'][$c].",'".$reldata['marital_stat'][$c]."','".$reldata['contact'][$c]."')";
            mysqli_query($con, $q);
          }
          $relatives[]=getRelativeIdByArray($check);
        }

        #save into other tables
        foreach($relatives as $entry){
          $check = array("profile"=>$profile_id, "relative"=>$entry);
          if(!checkProfileRelativesByArray($check)){
            $q = "INSERT INTO profile_relatives (profile_id, relative_id) VALUES (".$profile_id.",".$entry.")";
            mysqli_query($con, $q);
          }
        }
      }

      #if existing relatives were edited
      if((isset($_POST['relativeId'])) &&(!empty($_POST['relativeId']))){
        $relid = $_POST['relativeId'];
        $relname = $_POST['relativeNameEdit'];
        $relage = $_POST['relativeAgeEdit'];
        $relmar = $_POST['maritalStatEdit'];
        $relocc = $_POST['relativeOccupationEdit'];
        $relcon  = $_POST['relativeContactEdit'];
        $relinc  = $_POST['relativeIncomeEdit'];

        if(isset($_POST['relativeRelEdit']))
          $relrel  = $_POST['relativeRelEdit'];
        else
          $relrel = NULL;

        if(isset($_POST['relativeEduEdit']))
          $reledu  = $_POST['relativeEduEdit'];
        else
          $reledu = NULL;

        if(isset($_POST['relativeRemarksEdit']))
          $relrmk  = $_POST['relativeRemarksEdit'];
        else
          $relrmk  = "NULL";

        $reldata = array("id"=>$relid,"name"=>$relname,"age"=>$relage,"marital_stat"=>$relmar,"relationship"=>$relrel,"education"=>$reledu,"occupation"=>$relocc,"income"=>$relinc,"remark"=>$relrmk, "contact"=>$relcon);

        $relatives=array();

        for($c = 0; $c < count($reldata['id']); $c++){
          $q = "SELECT * FROM relative WHERE relative_id=".$reldata['id'][$c];
          $r = mysqli_query($con, $q);
          $default = mysqli_fetch_array($r);

          $q = "UPDATE relative SET ";
          // echo $reldata['name'][$c];

          if(is_null($reldata['name'][$c])){
            $q .= " relative_name='".$reldata['name'][$c]."',";
          }else{
            $q .= " relative_name='".$default['relative_name']."',";
          }

          if($reldata['relationship'][$c] != NULL){
            $q .= " relationship_id=".$reldata['relationship'][$c].",";
          }else{
            $q .= " relationship_id=".$default['relationship_id'].",";
          }

          if($reldata['occupation'][$c] != NULL){
            $q .= " occupation='".$reldata['occupation'][$c]."',";
          }else{
            $q .= " occupation='".$default['occupation']."',";
          }

          if($reldata['contact'][$c] != NULL){
            $q .= " contact='".$reldata['contact'][$c]."',";
          }else{
            $q .= " contact='".$default['contact']."',";
          }

          if($reldata['income'][$c] != NULL){
            $q .= " salary=".$reldata['income'][$c].",";
          }else{
            if(is_null($default['salary']))
              $q .= " salary=NULL,";
            else
              $q .= " salary=".$default['salary'].",";
          }

          if(is_null($reldata['remark'][$c])){
            $q .= " remarks='".$reldata['remark'][$c]."',";
          }else{
            $q .= " remarks='".$default['remarks']."',";
          }


          if($reldata['age'][$c] != NULL){
            $q .= " age=".$reldata['age'][$c].",";
          }else{
            if(is_null($default['salary']))
              $q .= " age=NULL,";
            else
              $q .= " age=".$default['age'].",";
          }

          if($reldata['education'][$c] != NULL){
            $q .= " education_id=".$reldata['education'][$c];
          }else{
            if(is_null($default['salary']))
              $q .= " education_id=NULL";
            else
              $q .= " education_id=".$default['education_id'];

          }

          $q .= " WHERE relative_id=".$reldata['id'][$c];
          mysqli_query($con, $q);
        }
      }

      if(isset($_POST['fatherstat-new'])){
        $val = cleanData($con, $_POST['fatherstat-new']);
        if ($val != "NULL"){
          $query .= "fatherstat='".$val."', ";
        }
      }

      if(isset($_POST['motherstat-new'])){
        $val = cleanData($con, $_POST['motherstat-new']);
        if ($val != "NULL"){
          $query .= "motherstat='".$val."', ";
        }
      }

      if(isset($_POST['childstat-new'])){
        $val = cleanData($con, $_POST['childstat-new']);
        if ($val != "NULL"){
          $query .= "childstat='".$val."', ";
        }
      }

      #if a guardian is an existing relative
      if((isset($_POST['guardian-new'])) || ((isset($_POST['gaddr1-new']))&&(isset($_POST['gaddr2-new']))&&(!empty($_POST['gaddr1-new']))&&(!empty($_POST['gaddr2-new']))) || (isset($_POST['relguardian-new']))){
        if(isset($_POST['relguardian-new']))
          $relt = cleanData($con,$_POST['relguardian-new']);
        else
          $relt = 'NULL';
        $gdata = array("name"=>cleanData($con,$_POST['guardian-new']), "rel"=>$relt);

        if(checkRelativeByArray($gdata)){
          $q = "SELECT * FROM relative WHERE relative_name='".$gdata['name']."'";
          $r = mysqli_query($con, $q);
          $dft = mysqli_fetch_array($r);

          $gquery = "UPDATE relative SET ";
          if((isset($_POST['gaddr1-new']))&&(isset($_POST['gaddr2-new']))&&(!empty($_POST['gaddr1-new']))&&(!empty($_POST['gaddr2-new']))){
            $baddr = array("addr1" => cleanData($con, $_POST['gaddr1-new']),
            "addr2" => cleanData($con, $_POST['gaddr2-new']),
            "addr3" => cleanData($con, $_POST['gaddr3-new']),
            "addr4" => cleanData($con, $_POST['gaddr4-new']));

            if(!checkAddressByArray($caddr)){
              $q = "INSERT INTO address (address_id, street_name, barangay_id) VALUES (NULL, '".$gaddr['addr1']."', ".$gaddr['addr2'].")";
              mysqli_query($con, $q);
            }

            $gaddr = getAddressIdByArray($gaddr);
            $gquery .= "address_id=".$gaddr.", ";
          }else{
            $gquery .= "address_id=".$dft['address_id'].", ";
          }

          if(isset($_POST['guardian-new'])){
            $val = cleanData($con, $_POST['guardian-new']);
            if($val != "NULL"){
              $gquery .= "relative_name='".$val."', ";
            }
          }else{
            $gquery .= "relative_name='".$dft['name']."', ";
          }

          if(isset($_POST['relguardian-new'])){
            $val = cleanData($con, $_POST['relguardian-new']);
            if($val != "NULL"){
              $gquery .= "relationship_id=".$val;
            }
          }else{
            $gquery .= "relationship_id=".$dft['relationship_id'];
          }

          $currentGdn = getProfileByProfileId($profile_id);
          $gquery .=" WHERE relative_id=".$currentGdn['guardian_id'];
          mysqli_query($con, $gquery);
        }else{ #if guardian is a new relative
          if((isset($_POST['gaddr1-new']))&&(isset($_POST['gaddr2-new']))&&(!empty($_POST['gaddr1-new']))&&(!empty($_POST['gaddr2-new']))){
            $baddr = array("addr1" => cleanData($con, $_POST['gaddr1-new']),
            "addr2" => cleanData($con, $_POST['gaddr2-new']),
            "addr3" => cleanData($con, $_POST['gaddr3-new']),
            "addr4" => cleanData($con, $_POST['gaddr4-new']));

            if(!checkAddressByArray($caddr)){
              $q = "INSERT INTO address (address_id, street_name, barangay_id) VALUES (NULL, '".$gaddr['addr1']."', ".$gaddr['addr2'].")";
              mysqli_query($con, $q);
            }

            $gaddr = getAddressIdByArray($gaddr);
          }else{
            $gaddr = NULL;
          }

          if(isset($_POST['guardian-new'])){
            if((isset($_POST['gaddr1-new']))&&(isset($_POST['gaddr2-new']))&&(!empty($_POST['gaddr1-new']))&&(!empty($_POST['gaddr2-new']))){
              $gquery.=", ";
            }
            $gname = cleanData($con, $_POST['guardian-new']);
          }else{
            $gname = 'NULL';
          }

          if(isset($_POST['relguardian-new'])){
            $val = cleanData($con, $_POST['relguardian-new']);
            if($val != "NULL"){
              $grel = $val;
            }
          }else{
            $grel = NULL;
          }

          $gquery = "INSERT INTO relative (relative_id, relative_name, relationship_id, address_id) VALUES (NULL, '$gname', $grel, $gaddr)";
          mysqli_query($con, $gquery);
        }
      }

      if(isset($_POST['reason-new'])){
        $val = cleanData($con, $_POST['reason-new']);
        if($val != "NULL"){
          $query .= "reason='".$val."', ";
        }
      }else{
        $query .= "reason='".$default['reason']."', ";
      }

      if(isset($_POST['profilestat-new'])){
        $val = cleanData($con, $_POST['profilestat-new']);
        if($val != "NULL"){
          $query .= "status_id='".$val."', ";
        }else{
          $query .= "status_id='".$default['status_id']."', ";
        }
      }

      $p = getAccountIdByAccountName($_SESSION['login-user']);
      $query .= "volunteer_id=".$p." WHERE profile_id=".$profile_id;

      if(mysqli_query($con, $query)){
        $_SESSION['query_success'] = "Profile successfully updated!";
      }else{
        $_SESSION['query_error']=die(mysqli_error($con));
      }

      if(isset($_POST['sleepplace'])){
        $slumber = cleanData($con, $_POST['sleepplace']);

        foreach($slumber as $place){
          if($place != "NULL"){
            $q = "DELETE FROM profile_sleep_areas WHERE profile_id=".$profile_id;
            mysqli_query($con, $q);

            if(!checkSleepingAreaByAreaName($place)){
              $q = "INSERT INTO sleeping_area (area_id, area_name) VALUES (NULL, '".$place."')";
              mysqli_query($con, $q);
            }
            $place_id = getSleepAreaIdByAreaName($place);
            $check = array("profile"=>$profile_id, "area_id"=>$place_id);
            if(!checkProfileSleepAreasByArray($check)){
              $q = "INSERT INTO profile_sleep_areas (profile_id, area_id) VALUES (".$profile_id.",".$place_id.")";
              mysqli_query($con, $q);
            }
          }
        }
      }

      if(isset($_POST['freqarea'])){
        $work = cleanData($con, $_POST['freqarea']);

        foreach($work as $place){
          if($place != "NULL"){
            $q = "DELETE FROM profile_work_areas WHERE profile_id=".$profile_id;
            mysqli_query($con, $q);

            if(!checkWorkAreaByAreaName($place)){
              $q = "INSERT INTO working_area (area_id, area_name) VALUES (NULL, '".$place."')";
              print_r("insert query: ".$q);
              mysqli_query($con, $q);
              die(mysqli_error($con));
            }
            $place_id = getWorkAreaIdByAreaName($place);
            print_r("place id: ". $place_id);
            $check = array("profile"=>$profile_id, "area_id"=>$place_id);
            if(!checkProfileWorkAreasByArray($check)){
              $q = "INSERT INTO profile_work_areas (profile_id, area_id) VALUES (".$profile_id.",".$place_id.")";
              print_r($q);
              mysqli_query($con, $q);
            }
          }
        }
      }

      if(isset($_POST['streetact'])){
        $act = cleanData($con, $_POST['streetact']);

        foreach($act as $place){
          if($place != "NULL"){
            $q = "DELETE FROM profile_activities WHERE profile_id=".$profile_id;
            mysqli_query($con, $q);

            if(!checkActivityByActivityName($place)){
              $q = "INSERT INTO activity (activity_id, activity_name) VALUES (NULL, '".$place."')";
              mysqli_query($con, $q);
            }
            $activity_id = getActivityIdByActivityName($place);
            $check = array("profile"=>$profile_id, "activity_id"=>$activity_id);
            if(!checkProfileActivitiesByArray($check)){
              $q = "INSERT INTO profile_activities (profile_id, activity_id) VALUES (".$profile_id.",".$activity_id.")";
              mysqli_query($con, $q);
            }
          }
        }
      }

      if(isset($_POST['documents'])){
        $docs = cleanData($con, $_POST['documents']);

        foreach($docs as $doc){
          if($doc != "NULL"){
            $q = "DELETE FROM profile_documents WHERE profile_id=".$profile_id;
            mysqli_query($con, $q);

            if(!checkDocumentByDocumentName($doc)){
              $q = "INSERT INTO document (document_id, document_name) VALUES (NULL, '".$doc."')";
              mysqli_query($con, $q);
            }
            $doc_id = getWorkAreaIdByAreaName($doc);
            $check = array("profile"=>$profile_id, "document_id"=>$doc_id);
            if(!checkProfileDocumentsByArray($check)){
              $q = "INSERT INTO profile_documents (profile_id, document_id) VALUES (".$profile_id.",".$doc_id.")";
              mysqli_query($con, $q);
            }
          }
        }
      }

      if(isset($_POST['sacraments'])){
        $arr = cleanData($con, $_POST['sacraments']);

        foreach($arr as $sac){
          if($sac != "NULL"){
            $q = "DELETE FROM profile_sacraments WHERE profile_id=".$profile_id;
            mysqli_query($con, $q);

            if(!checkSacramentBySacramentName($sac)){
              $q = "INSERT INTO sacrament (sacrament_id, sacrament_name) VALUES (NULL, '".$sac."')";
              mysqli_query($con, $q);
            }
            $sac_id = getSacramentIdBySacramentName($sac);
            $check = array("profile"=>$profile_id, "sacrament_id"=>$sac_id);
            if(!checkProfileSacramentsByArray($check)){
              $q = "INSERT INTO profile_sacraments (profile_id, sacrament_id) VALUES (".$profile_id.",".$sac_id.")";
              mysqli_query($con, $q);
            }
          }
        }
      }

    }else{
      $_SESSION['query_error'] = "No changes detected.";
    }
  }catch(Exception $e){

  }catch(Error $e){

  }finally{
    mysqli_close($con);
    header('Location: /../pages/pageProfileList.php');
  }
}

//REACTIVATE PROFILE ITEM
if(isset($_GET['rstitem'])){
  unset($_GET['rstitem']);

  require_once($_SERVER['DOCUMENT_ROOT'].'/functions/funcGenericFunctions.php');

  try{
    $con = getDatabaseConnection();
    $profile_id = cleanData($con, $_POST['profile_id']);

    $query = "UPDATE profile SET status_id='Returnee' WHERE profile_id =".$profile_id;
    mysqli_query($con, $query);

    $query = "DELETE FROM inactive_profiles WHERE profile_id =".$profile_id;
    mysqli_query($con, $query);

    $query = "INSERT INTO active_profiles (log_id, profile_id) VALUES (NULL,".$profile_id.")";

    if(mysqli_query($con, $query)){
      $_SESSION['query_success'] = "Profile reactivated successfully!";
    }
  }catch(Exception $e){
    $_SESSION['query_error']= "Error in saving changes:".die(mysqli_error($con));
  }catch(Error $e){
    $_SESSION['query_error']= "Error in saving changes:".die(mysqli_error($con));
  }finally{
    mysqli_close($con);
    header('Location: /../pages/pageProfileList.php');
  }
}

//DEACTIVATE PROFILE ITEM
if(isset($_GET['delitem'])){
  unset($_GET['delitem']);

  require_once($_SERVER['DOCUMENT_ROOT'].'/functions/funcGenericFunctions.php');

  try{
    $con = getDatabaseConnection();
    $profile_id = cleanData($con, $_POST['profile_id']);

    $query = "UPDATE profile SET status_id='Inactive' WHERE profile_id =".$profile_id;
    mysqli_query($con, $query);

    $query = "DELETE FROM active_profiles WHERE profile_id =".$profile_id;
    mysqli_query($con, $query);

    $query = "INSERT INTO inactive_profiles (log_id, profile_id) VALUES (NULL,".$profile_id.")";

    if(mysqli_query($con, $query)){
      $_SESSION['query_success'] = "Profile deactivated successfully!";
    }

  }catch(Exception $e){
    $_SESSION['query_error']= "Error in saving changes:".die(mysqli_error($con));
  }catch(Error $e){
    $_SESSION['query_error']= "Error in saving changes:".die(mysqli_error($con));
  }finally{
    mysqli_close($con);
    header('Location: /../pages/pageProfileList.php');
  }
}

//PRINT INVENTORY (unused for now)
if(isset($_GET['printlist'])){
  unset($_GET['printlist']);
  require_once('/../functions/funcPrintFeature.php');
  require_once('/../functions/tcpdf/mypdf.php');
  require_once('/../functions/funcGenericFunctions.php');
  require_once('/../functions/funcAccountsMngt.php');

  $user = $_SESSION['login-user']; //retrieveDetails($_POST['owner']);
  $size = $_POST['size'];
  $orn = $_POST['orientation'];

  $pdf = new MYPDF($orn,PDF_UNIT,$size,true,'UTF-8',false);

  //document meta information
  $pdf->SetCreator(PDF_CREATOR);
  $pdf->SetAuthor('Special Applications Laboratory');
  $pdf->SetTitle('Inventory Sheet');
  $pdf->SetSubject('Print Inventory Sheet');
  $pdf->SetKeywords('SAL, Inventory');

  $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP+30, PDF_MARGIN_RIGHT, PDF_MARGIN_FOOTER + 60);
  $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
  $pdf->SetFooterMargin(PDF_MARGIN_FOOTER+60);
  $pdf->SetAutoPageBreak(true, PDF_MARGIN_FOOTER+60);

  $pdf->setWs('Steve Estoconing');
  $pdf->setmode(2);

  //Add a page
  $pdf->AddPage();

  $w = $pdf->getPageWidth();

  $content = '<table class="table" style="margin-top:20px; margin-bottom:100px; padding-top:10px;">';
  $content .= fetchInventory();
  $content .= '
  </table>
  ';
  $pdf->writeHTML($content);
  $pdf->Output("salborrowerslip.pdf", "I");
}

if(isset($_GET['uploadPhoto'])){
  unset($_GET['uploadPhoto']);
  try{
    $con = getDatabaseConnection();

    $q = "SELECT photo FROM account WHERE account_name='".$_SESSION['login-user']."'";
    $r = mysqli_query($con, $q);
    $ed = mysqli_fetch_array($r);
    if($ed['photo'] != NULL){

    }

    if(isset($_FILES['photo'])){
      // print_r("pid");
      $file = pathinfo($_FILES['photo']['name']);
      $file_name = "cp-".$_POST['profile'].".".$file['extension'];
      $extension = $file['extension'];

      if (($extension != "jpg") && ($extension != "jpeg") && ($extension != "png") && ($extension != "gif")) {
        $_SESSION['query_error']='Only accepts jpg, jpeg, png, and gif';
        return;
      }

      $file_size = $_FILES['photo']['size'];
      $file_tmp_name = $_FILES['photo']['tmp_name'];
      // print_r($file_size);

      $target_dir=$_SERVER['DOCUMENT_ROOT']."/photos/pp/";
      $data = "/photos/pp/".$file_name;
      if($file_size <= 52428800){
        //resize img
        if($extension=="jpg" || $extension=="jpeg" )
          $src = imagecreatefromjpeg($file_tmp_name);
        else if($extension=="png")
          $src = imagecreatefrompng($file_tmp_name);
        else
          $src = imagecreatefromgif($file_tmp_name);

        list($ow, $oh) = getimagesize($file_tmp_name);

        $nh = 400; $nw = 300;
        // $nw = ($ow/$oh)*$nh;
        $tmp = imagecreatetruecolor($nw,$nh);

        imagecopyresampled($tmp,$src,0,0,0,0,$nw,$nh,$ow,$oh);

        imagejpeg($tmp,$file_tmp_name,100);
        imagedestroy($src);
        imagedestroy($tmp);

        //copy img to local dir
        if(move_uploaded_file($file_tmp_name, $target_dir.$file_name)){
          $query = "UPDATE profile SET profile_picture='".$data."' WHERE profile_id='".$_POST['profile']."'";
          // print_r($query);
          if(mysqli_query($con,$query)){
            $_SESSION['query_success'] = "Photo successfully updated!";
            print_r("tmp:".$file_tmp_name.", tgt:".$target_dir.$file_name);
          }else{
            $_SESSION['query_error'] = "Error in saving profile picture:".mysqli_error($con);
          }
        }else{
          $_SESSION['query_error'] = "Photo upload failed! Consider choosing another picture.";
        }
      }else{
        $_SESSION['query_error'] = "Photo size too large! Consider choosing another picture.";
      }
    }

  }catch(Exception $e){

  }catch(Error $e){

  }finally{
    mysqli_close($con);
    header('Location: /../pages/pageProfileList.php');
  }
}

if(isset($_GET['uploadBPhoto'])){
  unset($_GET['uploadBPhoto']);
  try{
    $con = getDatabaseConnection();

    $q = "SELECT photo FROM account WHERE account_name='".$_SESSION['login-user']."'";
    $r = mysqli_query($con, $q);
    $ed = mysqli_fetch_array($r);
    if($ed['photo'] != NULL){

    }

    if(isset($_FILES['photo'])){
      // print_r("pid");
      $file = pathinfo($_FILES['photo']['name']);
      $file_name = "cbp-".$_POST['profile'].".".$file['extension'];
      $extension = $file['extension'];

      if (($extension != "jpg") && ($extension != "jpeg") && ($extension != "png") && ($extension != "gif")) {
        $_SESSION['query_error']='Only accepts jpg, jpeg, png, and gif';
        return;
      }

      $file_size = $_FILES['photo']['size'];
      $file_tmp_name = $_FILES['photo']['tmp_name'];
      // print_r($file_size);

      $target_dir=$_SERVER['DOCUMENT_ROOT']."/photos/pp/";
      $data = "/photos/pp/".$file_name;
      if($file_size <= 52428800){
        //resize img
        if($extension=="jpg" || $extension=="jpeg" )
          $src = imagecreatefromjpeg($file_tmp_name);
        else if($extension=="png")
          $src = imagecreatefrompng($file_tmp_name);
        else
          $src = imagecreatefromgif($file_tmp_name);

        list($ow, $oh) = getimagesize($file_tmp_name);

        $nh = 400; $nw = 300;
        // $nw = ($ow/$oh)*$nh;
        $tmp = imagecreatetruecolor($nw,$nh);

        imagecopyresampled($tmp,$src,0,0,0,0,$nw,$nh,$ow,$oh);

        imagejpeg($tmp,$file_tmp_name,100);
        imagedestroy($src);
        imagedestroy($tmp);

        //copy resized img
        if(move_uploaded_file($file_tmp_name, $target_dir.$file_name)){
          $query = "UPDATE profile SET fbody_picture='".$data."' WHERE profile_id='".$_POST['profile']."'";
          // print_r($query);
          if(mysqli_query($con,$query)){
            $_SESSION['query_success'] = "Photo successfully updated!";
            print_r("tmp:".$file_tmp_name.", tgt:".$target_dir.$file_name);
          }else{
            $_SESSION['query_error'] = "Error in saving profile picture:".mysqli_error($con);
          }
        }else{
          $_SESSION['query_error'] = "Photo upload failed! Consider choosing another picture.";
        }
      }else{
        $_SESSION['query_error'] = "Photo size too large! Consider choosing another picture.";
      }
    }

  }catch(Exception $e){

  }catch(Error $e){

  }finally{
    mysqli_close($con);
    header('Location: /../pages/pageProfileList.php');
  }
}


?>
