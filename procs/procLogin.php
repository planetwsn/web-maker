<?php
include($_SERVER['DOCUMENT_ROOT'].'/phpmail_include.php');
include($_SERVER['DOCUMENT_ROOT'].'/procs/procSessionCheck.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/functions/funcDatabaseConnection.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/functions/funcAccountsMngt.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/functions/funcGenericFunctions.php');

if(isset($_GET['logout'])){
  unset($_GET['logout']);
  $_SESSION['query_info'] = "You have successfully logged out.";
  unset($_SESSION['login-user']);
  header('Location: /../index.php');
}

if(isset($_GET['login'])){
  unset($_GET['login']);

  $con = getDatabaseConnection();
  $uname = cleanData($con,$_POST['username']);
  $pword = cleanData($con,$_POST['password']);
  mysqli_close($con);

  if(($uname == "NULL" )||($pword == "NULL")){
    $_SESSION['query_error'] = "Invalid username or password.";
    header('Location: /../pages/pageLogin.php');
  }
  else{
    $res = getPasswordByUsername($uname);
    if($res['password'] != md5($pword)){
      if($res['password']){
        $_SESSION['query_error'] = "Your username or password is incorrect!";
      }else{
        $_SESSION['query_error'] = "Your username does not exist!";
      }
      header('Location: /../pages/pageLogin.php');
    }else {
      $_SESSION['login-user'] = $res['name'];
      $_SESSION['query_info'] = "Welcome, ".$res['name']."!";
      header('Location: /../index.php');
    }
  }
}

// forgot password
if(isset($_GET['reset'])){
  unset($_GET['reset']);

  $con = getDatabaseConnection();
  $email = cleanData($con,$_POST['email']);
  $countError = 0;

  $q = "SELECT account_name FROM account WHERE email='".$email."'";
  $r = mysqli_query($con, $q);
  $recipient = mysqli_fetch_array($r)['account_name'];

  if(mysqli_num_rows($r) <= 0){
    $_SESSION['query_error'] = "Sorry, no user exists on our system with that email.";
    $countError++;
  }

  $token = bin2hex(random_bytes(50));

  if($countError == 0){
    $sql = "INSERT INTO password_reset(id, email, token) VALUES (NULL, '$email', '$token')";
    $results = mysqli_query($con, $sql);

    $msg = "<h4>Password Reset</h4>
    <p>Hi there,</p>
    <p>Click on this <a href=\"http://".$_SERVER['SERVER_NAME']."/pages/pageReset.php?token=" . $token . "\"><strong>link</strong></a> to reset your password on the ASK site.</p>
    <br /><br />
    <p>Please do not reply to this email with your sensitive information.</p>
    <br /><br />
    <p><strong>ASK Information Team</strong><br />
    <italic>Abtanan sa Kaluoy</italic><br />
    <italic>The Roman Catholic Archdiocese of Cebu, Inc</italic><br />
    <italic>Pope John Paul II Ave., Cor C. Borces St.</italic><br />
    <italic>Mabolo, 6000 Cebu City</italic><br />
    <italic>+63 32 254 0951   +63 32 255 4458</italic></p>
    ";
    $msg = wordwrap($msg,70);

    $mail = new PHPMailer\PHPMailer\PHPMailer(true);
    $mail->SMTPDebug = 2;
    $mail->isSMTP();
    $mail->Host = "smtp.gmail.com";
    $mail->SMTPAuth = true;
    $mail->Username = "abtanansakaluoy.info@gmail.com";
    $mail->Password = "45k-1nf0";//'369c3df22a9bc8';
    $mail->SMTPSecure = 'tls';
    $mail->Port=587;

    $mail->From = "abtanansakaluoy.info@gmail.com";
    $mail->FromName = "ASK-Info";
    $mail->addAddress($email, $recipient);

    $mail->isHTML(true);
    $mail->Subject = "Reset Password ASK";
    $mail->Body = $msg;

    try{
      $mail->send();
      $_SESSION['query_success'] = "An email has been sent to the following address: ".$email;
    }catch(Exception $e){
      // $stat = !extension_loaded('openssl')?"Not available":"Available";
      $_SESSION['query_error'] =  "PHPMailer error:".$mail->ErrorInfo." Contact your administrator instead to reset your password.";
    }
  }
  echo "<script>window.location.assign('/../index.php')</script>";
}

?>
