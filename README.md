**Website Template**

This is a website template used for any web-based projects; built using PHP, JS, and CSS. An SQL-based database is also included.

Author: Mark Anthony Cabilo

Further documentation is currently in progress.
