<form action="../procs/procAccountsMngt.php" method="post">
  <div class="form-group row">
    <label class="col-sm-4 col-form-label">New password</label>
    <div class="col-sm-8">
      <input type="password" class="form-control" name="new-pass" placeholder="Provide new password">
    </div>
  </div>

  <div class="form-group row">
    <label for="password" class="col-sm-4 col-form-label">Confirm new password</label>
    <div class="col-sm-8">
      <input type="password" class="form-control" name="new-pass-c" placeholder="Confirm new password">
    </div>
  </div>

  <?php $_SESSION['token']=$_GET['token']; ?>

  <div class="text-center row">
    <div class="col-sm-12">
      <input type="submit" name="newpass" class="btn btn-primary" value="Reset" />
    </div>
  </div>
</form>
