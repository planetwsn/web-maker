<form action="/../procs/procProfileListMngt?additem" method="post">

  <!-- child's full name -->
  <div class="form-group">
    <label class="control-label" for="name-element">Child's Name:</label>
    <input type="text" class="form-control" id="name-element" name="prof-name" placeholder="Enter child's name" required>
  </div>

  <!-- child's nickname -->
  <div class="form-group">
    <label class="control-label" for="alias-element">Nickname:</label>
    <input type="text" class="form-control" id="alias-element" name="prof-alias" placeholder="Enter child's nickname(s); separated by commas" required>
  </div>

  <!-- address lines -->
  <div class="form-group">
    <label class="control-label" for="addr1-element">Address:</label>
    <input type="text" class="form-control" id="caddr1-element" name="caddr1" placeholder="Enter house no. / street name / sitio" required>
    <select class="form-control selectpicker" id="caddr4-element" name="caddr4" data-live-search="true" data-size="5" required>
      <?php
      require_once($_SERVER['DOCUMENT_ROOT'].'/functions/funcAddressFunctions.php');
      getProvinceSelection();
      ?>
    </select>
    <select class="form-control selectpicker" id="caddr3-element" name="caddr3" data-live-search="true" data-size="5" disabled required>
      <option selected>-- city / municipality --</option>
    </select>
    <select class="form-control selectpicker" id="caddr2-element" name="caddr2" data-live-search="true" data-size="5" disabled required>
      <option selected>-- barangay --</option>
    </select>
  </div>

  <!-- age -->
  <div class="form-group">
    <label class="control-label" for="age-element">Age:</label>
    <input type="number" min="0" max="25" class="form-control" id="age-element" name="prof-age" placeholder="Enter child's age" required>
  </div>

  <!-- gender -->
  <div class="form-group">
    <label class="control-label" for="gender-element">Gender:</label>
    <div class="panel panel-default">
      <label class="radio-form"><input type="radio" class="form-check-input radio-form gender-element" name="prof-gender" value="Male" checked>Male</label>
      <label class="radio-form"><input type="radio" class="form-check-input radio-form gender-element" name="prof-gender" value="Female">Female</label>
    </div>
  </div>

  <!-- religion  -->
  <div class="form-group">
    <label class="control-label" for="religion-element">Religion:</label>
    <div class="panel panel-default">
      <label class="radio-form"><input type="radio" class="form-check-input radio-form" name="prof-religion" value="Roman Catholicism" checked>Roman Catholicism</label>
      <label class="radio-form"><input type="radio" class="form-check-input radio-form" name="prof-religion" value="Islam">Islam</label>
      <label class="radio-form"><input type="radio" class="form-check-input radio-form" name="prof-religion" value="Protestantism">Protestantism</label>
      <label class="radio-form"><input type="radio" class="form-check-input radio-form" name="prof-religion" value="Christianity">Christianity</label>
      <label class="radio-form"><input type="radio" class="form-check-input radio-form" name="prof-religion" value="Tribal Religion">Tribal Religion</label>
      <label class="radio-form"><input type="radio" class="form-check-input radio-form" name="prof-religion" value="Other">Other</label>
      <input class="form-control" name="prof-religion-new" placeholder="Only fill-in this input when 'Other' is selected" id="rel-other-text" disabled>
    </div>
  </div>

  <!-- height  -->
  <div class="form-group">
    <label class="control-label" for="height-element">Height (cm):</label>
    <input type="number" min="0" max="220" class="form-control" id="height-element" name="prof-height" placeholder="Enter child's height in centimeters" required>
  </div>

  <!-- weight  -->
  <div class="form-group">
    <label class="control-label" for="weight-element">Weight (kg):</label>
    <input type="number" min="0" max="150" class="form-control" id="weight-element" name="prof-weight" placeholder="Enter child's weight in kilograms" required>
  </div>

  <!-- contact number -->
  <div class="form-group">
    <label class="control-label" for="childphone-element">Phone number:</label>
    <input type="text" class="form-control" id="childphone-element" name="prof-phone" placeholder="Enter child's phone number">
  </div>

  <!-- date of birth -->
  <div class="form-group">
    <label class="control-label" for="birthdate-element">Date of Birth:</label>
    <input class="form-control" id="birthdate-element" name="prof-birthdate" placeholder="mm-dd-yyyy" data-provide="datepicker" date-date-format="mm-dd-yyyy" required>
  </div>

  <!-- place of birth -->
  <div class="form-group">
    <label class="control-label">Place of Birth:</label>
    <input type="text" class="form-control" id="baddr1-element" name="baddr1" placeholder="Enter house no. / street name / sitio" required>
    <select class="form-control selectpicker" id="baddr4-element" name="baddr4" data-live-search="true" data-size="5" required>
      <?php
      require_once($_SERVER['DOCUMENT_ROOT'].'/functions/funcAddressFunctions.php');
      getProvinceSelection();
      ?>
    </select>
    <select class="form-control selectpicker" id="baddr3-element" name="baddr3" data-live-search="true" data-size="5" disabled required>
      <option selected>-- city / municipality --</option>
    </select>
    <select class="form-control selectpicker" id="baddr2-element" name="baddr2" data-live-search="true" data-size="5" disabled required>
      <option selected>-- barangay --</option>
    </select>
  </div>

  <!-- distinguishing marks -->
  <div class="form-group">
    <label class="control-label" for="marks-element">Distinguishing Marks:</label>
    <textarea class="form-control" rows="3" id="marks-element" name="prof-distmarks" placeholder="Provide any distinguishing marks present on the child" style="resize:none" required></textarea>
  </div>

  <!-- highest educational attainment -->
  <div class="form-group">
    <label class="control-label" for="education-element">Highest Educational Attainment:</label>
    <select class="form-control selectpicker" id="education-element" name="edu-level" data-live-search="true" data-size="5" required>
      <?php
      require_once($_SERVER['DOCUMENT_ROOT'].'/functions/funcProfileListMngt.php');
      getEducationSelection();
      ?>
    </select>
  </div>

  <!-- schooling status -->
  <div class="form-group">
    <label class="control-label" for="schooling-element">Currently studying?</label>
    <div class="panel panel-default">
      <label class="radio-form"><input type="radio" class="form-check-input radio-form" name="schoolingstatus" value="Yes" checked>Yes</label>
      <label class="radio-form"><input type="radio" class="form-check-input radio-form" name="schoolingstatus" value="No">No</label>
    </div>
  </div>

  <!-- year last attended -->
  <div class="form-group">
    <label class="control-label" for="year-element">Year Last Attended:</label>
    <input type="text" class="form-control" id="year-element" name="yearstudied" placeholder="Enter year last studied" required>
  </div>

  <!-- school last attended -->
  <div class="form-group">
    <label class="control-label" for="school-element">School Last Attended:</label>
    <input type="text" class="form-control" id="school-element" name="schoolstudied" placeholder="Enter school last studied in" required>
  </div>

  <!-- nourishment  -->
  <div class="form-group">
    <label class="control-label" for="nourishment-element">Nourishment:</label>
    <div class="panel panel-default">
      <label class="radio-form"><input type="radio" class="form-check-input radio-form" name="prof-nourishment" value="Normal" checked>Normal</label>
      <label class="radio-form"><input type="radio" class="form-check-input radio-form" name="prof-nourishment" value="Undernourished">Undernourished</label>
    </div>
  </div>

  <!-- family composition -->
  <div class="form-group">
    <label class="control-label" for="fam-ctl-element">Family Composition:</label>
    <div>
      <button type="button" class="btn btn-primary btn-sm" id="add-fam-element">Add New Family Member</button>
    </div>
    <ul class="list-group-flush fam-list"></ul>
  </div>

  <!-- status of father -->
  <div class="form-group-inline">
    <label class="control-label">Status of Father:</label>
    <div class="panel panel-default">
      <label class="radio-form"><input type="radio" class="form-check-input radio-form" id="father-stat1-element" name="fatherstat" value="Living" >Living</label>
      <label class="radio-form"><input type="radio" class="form-check-input radio-form" id="father-stat2-element" name="fatherstat" value="Deceased">Deceased</label>
      <label class="radio-form"><input type="radio" class="form-check-input radio-form" id="father-stat3-element" name="fatherstat" value="N/A" checked>Not Applicable</label>
    </div>
  </div>

  <!-- status of mother -->
  <div class="form-group-inline">
    <label class="control-label">Status of Mother:</label>
    <div class="panel panel-default">
      <label class="radio-form"><input type="radio" class="form-check-input radio-form" id="mother-stat1-element" name="motherstat" value="Living">Living</label>
      <label class="radio-form"><input type="radio" class="form-check-input radio-form" id="mother-stat2-element" name="motherstat" value="Deceased">Deceased</label>
      <label class="radio-form"><input type="radio" class="form-check-input radio-form" id="mother-stat3-element" name="motherstat" value="N/A" checked>Not Applicable</label>
    </div>
  </div>

  <!-- child living with parents -->
  <div class="form-group-inline">
    <label class="control-label">Child Living with Parents?</label>
    <div class="panel panel-default">
      <label class="radio-form"><input type="radio" class="form-check-input radio-form" id="child-parentage1-element" name="childparentage" value="Yes">Yes</label>
      <label class="radio-form"><input type="radio" class="form-check-input radio-form" id="child-parentage2-element" name="childparentage" value="No" checked>No</label>
    </div>
  </div>

  <!-- child companion/guardian -->
  <div class="form-group">
    <label class="control-label" for="guardian-element">Guardian's  Name:</label>
    <input type="text" class="form-control" id="guardian-element" name="guardian" placeholder="Enter child\'s current guardian" required>
  </div>

  <!-- relationship to child -->
  <div class="form-group">
    <label class="control-label" for="relguardian-element">Guardian's Relationship to Child:</label>
    <select class="form-control selectpicker" id="relguardian-element" name="relguardian" data-live-search="true" data-size="5" required>
      <?php
      require_once($_SERVER['DOCUMENT_ROOT'].'/functions/funcProfileListMngt.php');
      getRelationshipSelection();
       ?>
    </select>
  </div>

  <!-- guardian address -->
  <div class="form-group" id="gdn-addr">
    <label class="control-label" >Guardian's Current Address:</label>
    <input type="text" class="form-control" id="gaddr1-element" name="gaddr1" placeholder="Enter house no. / street name / sitio" required>
    <select class="form-control selectpicker" id="gaddr4-element" name="gaddr4" data-live-search="true" data-size="5" required>
      <?php
      require_once($_SERVER['DOCUMENT_ROOT'].'/functions/funcAddressFunctions.php');
      getProvinceSelection();
      ?>
    </select>
    <select class="form-control selectpicker" id="gaddr3-element" name="gaddr3" data-live-search="true" data-size="5" disabled required>
      <option selected>-- city / municipality --</option>
    </select>
    <select class="form-control selectpicker" id="gaddr2-element" name="gaddr2" data-live-search="true" data-size="5" disabled required>
      <option selected>-- barangay --</option>
    </select>
  </div>

  <!-- place of sleep -->
  <div class="form-group">
    <label class="control-label">Place of Sleep:</label>
    <div class="panel panel-default">
      <label class="checkbox-form"><input type="checkbox" class="form-check-input checkbox-form" name="sleepplace[]" value="Home">Home</label>
      <label class="checkbox-form"><input type="checkbox" class="form-check-input checkbox-form" name="sleepplace[]" value="Street">Street</label>
      <label class="checkbox-form"><input type="checkbox" class="form-check-input checkbox-form" name="sleepplace[]" value="Institution">Institution</label>
      <label class="checkbox-form"><input type="checkbox" class="form-check-input checkbox-form" name="sleepplace[]" value="Parks">Parks</label>
      <label class="checkbox-form"><input type="checkbox" class="form-check-input radio-form" id="sleepplace-other-element">Others</label>
      <input class="form-control" name="sleepplace[]" placeholder="Only fill in this input when 'Others' is selected" id="sleepplace-other-text" disabled>
    </div>
  </div>

  <!-- areas frequented -->
  <div class="form-group">
    <label class="control-label" for="freq-area-element">Areas Frequented:</label>
    <div class="panel panel-default">
      <label class="checkbox-form"><input type="checkbox" class="form-check-input checkbox-form" name="freqarea[]" value="Market">Market</label>
      <label class="checkbox-form"><input type="checkbox" class="form-check-input radio-form" name="freqarea[]" value="National Road">National Road</label>
      <label class="checkbox-form"><input type="checkbox" class="form-check-input checkbox-form" name="freqarea[]" value="Parks">Parks</label>
      <label class="checkbox-form"><input type="checkbox" class="form-check-input checkbox-form" name="freqarea[]" id="freqarea-other-element">Others</label>
      <input class="form-control" name="freqarea[]" placeholder="Only fill in this input when 'Others' is selected" id="freqarea-other-text" disabled>
    </div>
  </div>

  <!-- street activities -->
  <div class="form-group">
    <label class="control-label">Street Activities:</label>
    <div class="panel panel-default">
      <label class="checkbox-form"><input type="checkbox" class="form-check-input radio-form" id="streetact1-element" name="streetact[]" value="Begging">Begging</label>
      <label class="checkbox-form"><input type="checkbox" class="form-check-input checkbox-form" id="streetact2-element" name="streetact[]" value="Vending">Vending</label>
      <label class="checkbox-form"><input type="checkbox" class="form-check-input checkbox-form" id="streetact3-element" name="streetact[]" value="Car Washing">Car Washing</label>
      <label class="checkbox-form"><input type="checkbox" class="form-check-input checkbox-form" name="streetact[]" value="Loitering">Loitering</label>
      <label class="checkbox-form"><input type="checkbox" class="form-check-input checkbox-form" name="streetact[]" value="Scavenging">Scavenging</label>
      <label class="checkbox-form"><input type="checkbox" class="form-check-input checkbox-form" id="streetact-other-element">Others</label>
      <input class="form-control" name="streetact[]" placeholder="Only fill in this input when 'Others' is selected" id="streetact-other-text" disabled>
    </div>
  </div>

  <!-- reason for activities -->
  <div class="form-group">
    <label class="control-label" for="">Reason for Activities:</label>
    <textarea class="form-control" rows="3" id="reason-element" name="reason" placeholder="Write here reasons for child\'s activities" style="resize:none" reason></textarea>
  </div>

  <!-- child status -->
  <div class="form-group-inline">
    <label class="control-label">Child Status:</label>
    <div class="panel panel-default">
      <label class="radio-form"><input type="radio" class="form-check-input radio-form" id="child-stat1-element" name="prof-childstat" value="New" checked>New</label>
      <label class="radio-form"><input type="radio" class="form-check-input radio-form" id="child-stat2-element" name="prof-childstat" value="Old">Old</label>
      <label class="radio-form"><input type="radio" class="form-check-input radio-form" id="child-stat3-element" name="prof-childstat" value="Inactive">Inactive</label>
      <label class="radio-form"><input type="radio" class="form-check-input radio-form" id="child-stat4-element" name="prof-childstat" value="Returnee">Returnee</label>
    </div>
  </div>

  <!-- legal documents available -->
  <div class="form-group">
    <label class="control-label">Documents Available:</label>
    <div class="panel panel-default">
      <label class="checkbox-form"><input type="checkbox" class="form-check-input checkbox-form" name="documents[]" value="Birth Certificate">Birth Certificate</label>
      <label class="checkbox-form"><input type="checkbox" class="form-check-input checkbox-form" name="documents[]" value="Baptismal Certificate">Baptismal Certificate</label>
    </div>
  </div>

  <!-- sacraments received -->
  <div class="form-group">
    <label class="control-label">Sacraments Received:</label>
    <div class="panel panel-default">
      <label class="checkbox-form"><input type="checkbox" class="form-check-input checkbox-form" name="sacraments[]" value="Baptism">Baptism</label>
      <label class="checkbox-form"><input type="checkbox" class="form-check-input checkbox-form" name="sacraments[]" value="First Communion">First Communion</label>
      <label class="checkbox-form"><input type="checkbox" class="form-check-input checkbox-form" name="sacraments[]" value="Confirmation">Confirmation</label>
    </div>
  </div>

  <!-- assessment/worker plans -->
  <div class="form-group">
    <label class="control-label" for="assessment-element">Assessment / Worker Plans:</label>
    <textarea class="form-control" rows="3" id="assessment-element" name="assessment" placeholder="Write here worker's assessment regarding the child" style="resize:none"></textarea>
  </div>

  <!-- Social worker / Volunteer assigned -->
  <div class="form-group">
    <label class="control-label" for="underwriter-element">Prepared By:</label>
    <input type="text" class="form-control" id="underwriter-element" placeholder="<?php echo $_SESSION['login-user']; ?>" disabled>
  </div>

  <!-- buttons -->
  <div class="form-group">
    <button type="submit" class="btn btn-primary btn-sm">Submit</button>
    <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Cancel</button>
  </div>
</form>
