<form action="../procs/procRegister.php?register" method="post">
  <div class="form-group row">
    <label for="username" class="col-sm-4 col-form-label">Username: </label>
    <div class="col-sm-8">
      <input type="text" class="form-control" id="username" name="username" placeholder="Enter username" maxlength="30" required value="<?php if(isset($_POST['username'])) echo $_POST['username']; ?>">
    </div>
  </div>

  <div class="form-group row">
    <label for="password" class="col-sm-4 col-form-label">Password: </label>
    <div class="col-sm-8">
      <input type="password" class="form-control" id="password" name="password" placeholder="Enter password 8-20 characters long" maxlength="20" required value="<?php if(isset($_POST['password'])) echo $_POST['password']; ?>">
    </div>
  </div>

  <div class="form-group row">
    <label for="realname" class="col-sm-4 col-form-label">Full Name: </label>
    <div class="col-sm-8">
      <input type="text" class="form-control" id="realname" name="realname" placeholder="Enter full name" maxlength="50" required value="<?php if(isset($_POST['realname'])) echo $_POST['realname']; ?>">
    </div>
  </div>

  <div class="form-group row">
    <label for="contactnumber" class="col-sm-4 col-form-label">Contact Number: </label>
    <div class="col-sm-8">
      <input type="number" class="form-control" id="contactnumber" name="contactnumber" placeholder="Enter contact number" required value="<?php if(isset($_POST['contactnumber'])) echo $_POST['contactnumber']; ?>">
    </div>
  </div>

  <div class="form-group row">
    <label for="email" class="col-sm-4 col-form-label">E-mail: </label>
    <div class="col-sm-8">
      <input type="email" class="form-control" id="email" name="email" placeholder="Enter e-mail address" maxlength="40" required value="<?php if(isset($_POST['email'])) echo $_POST['email']; ?>">
    </div>
  </div>

  <div class="text-center row">
    <div class="col-sm-12">
      <input type="submit" class="btn btn-primary" value="Register" />
    </div>
  </div>
</form>
