<form id="edit-form" action="/../procs/procProfileListMngt?edititem" method="post">

  <!-- profile select -->
  <div class="form-group">
    <label class="control-label" for="edit-item-element">Child's Name</label>
    <select class="form-control selectpicker" id="edit-item-element" name="profile" data-live-search="true" data-size="5" required>
      <?php
      require_once($_SERVER['DOCUMENT_ROOT'].'/functions/funcProfileListMngt.php');
      getProfileListSelection();
      ?>
    </select>
  </div>

  <!-- name -->
  <div class="form-group">
    <label class="control-label">Name:</label>
    <div class="form-split">
      <input class="form-control" type="text" id="name-old-element" readonly>
    </div>
    <div class="form-split">
      <input class="form-control" type="text" id="name-new-element" name="name-new" placeholder="Enter new child's name" disabled>
    </div>
  </div>

  <!-- alias -->
  <div class="form-group">
    <label class="control-label">Nickname:</label>
    <div class="form-split">
      <input type="text" class="form-control" id="alias-old-element" readonly>
    </div>
    <div class="form-split">
      <input type="text" class="form-control" id="alias-new-element" name="alias-new" placeholder="Enter child's new nickname(s), separated by commas" disabled>
    </div>
  </div>

  <!-- address -->
  <div class="form-group">
    <label class="control-label">Current residence:</label>
    <div class="form-split">
      <input type="text" class="form-control" id="addr-old-element" readonly>
    </div>
    <div class="form-split">
      <input type="text" class="form-control" id="addr1-new-element" name="caddr1-new" placeholder="Enter house no. / street name / sitio" disabled>
      <select class="form-control selectpicker" id="addr4-new-element" name="caddr4-new" data-live-search="true" data-size="5" disabled>
        <?php
        require_once($_SERVER['DOCUMENT_ROOT'].'/functions/funcAddressFunctions.php');
        getProvinceSelection();
        ?>
      </select>
      <select class="form-control selectpicker" id="addr3-new-element" name="caddr3-new" data-live-search="true" data-size="5" disabled>
        <option selected>-- city / municipality --</option>
      </select>
      <select class="form-control selectpicker" id="addr2-new-element" name="caddr2-new" data-live-search="true" data-size="5" disabled>
        <option selected>-- barangay --</option>
      </select>
    </div>
  </div>

  <!-- age -->
  <div class="form-group">
    <label class="control-label">Age:</label>
    <div class="form-split">
      <input type="text" class="form-control" id="age-old-element" readonly>
    </div>
    <div class="form-split">
      <input type="number" min="0" max="25" class="form-control" id="age-new-element" name="age-new" placeholder="Enter child's age" disabled>
    </div>
  </div>

  <!-- gender -->
  <div class="form-group">
    <label class="control-label" for="gender-element">Gender:</label>
    <div class="form-split">
      <input class="form-control" type="text" id="gender-old-element" disabled>
    </div>
    <div class="form-split">
      <div class="panel panel-default">
        <label class="radio-form"><input type="radio" class="form-check-input radio-form gender-new-element" name="gender-new" value="Male" disabled>Male</label>
        <label class="radio-form"><input type="radio" class="form-check-input radio-form gender-new-element" name="gender-new" value="Female" disabled>Female</label>
      </div>
    </div>
  </div>

  <!-- religion  -->
  <div class="form-group">
    <label class="control-label" for="religion-element">Religion:</label>
    <div class="form-split">
      <input type="text" class="form-control" id="religion-old-element" readonly>
    </div>
    <div class="form-split">
      <div class="panel panel-default">
        <label class="radio-form"><input type="radio" class="form-check-input radio-form" name="religion-new" value="Roman Catholicism" disabled>Roman Catholicism</label>
        <label class="radio-form"><input type="radio" class="form-check-input radio-form" name="religion-new" value="Islam" disabled>Islam</label>
        <label class="form-check-input radio-form"><input type="radio" class="form-check-input radio-form" name="religion-new" value="Protestantism" disabled>Protestantism</label>
        <label class="radio-form"><input type="radio" class="form-check-input radio-form" name="religion-new" value="Christianity" disabled>Christianity</label>
        <label class="radio-form"><input type="radio" class="form-check-input radio-form" name="religion-new" value="Tribal Religion" disabled>Tribal Religion</label>
        <label class="radio-form"><input type="radio" class="form-check-input radio-form" name="religion-new" value="Other" disabled>Other</label>
        <input class="form-control" name="religion-new-text" placeholder="Only fill-in this input when 'Other' is selected" id="rlgtext" disabled>
      </div>
    </div>
  </div>

  <!-- height -->
  <div class="form-group">
    <label class="control-label">Height (in cm):</label>
    <div class="form-split">
      <input type="text" class="form-control" id="height-old-element" readonly>
    </div>
    <div class="form-split">
      <input type="number" min="0" class="form-control" id="height-new-element" name="height-new" placeholder="Enter child's new height" disabled>
    </div>
  </div>

  <!-- weight -->
  <div class="form-group">
    <label class="control-label">Weight (in kg):</label>
    <div class="form-split">
      <input type="text" class="form-control" id="weight-old-element" readonly>
    </div>
    <div class="form-split">
      <input type="number" min="0" class="form-control" id="weight-new-element" name="weight-new" placeholder="Enter child's new weight" disabled>
    </div>
  </div>

  <!-- contact number -->
  <div class="form-group">
    <label class="control-label">Phone Number:</label>
    <div class="form-split">
      <input type="text" class="form-control" id="childphone-old-element" readonly>
    </div>
    <div class="form-split">
      <input type="text" class="form-control" id="childphone-new-element" name="childphone-new" placeholder="Enter child's new contact number" disabled>
    </div>
  </div>

  <!-- date of birth -->
  <div class="form-group">
    <label class="control-label">Date of Birth:</label>
    <div class="form-split">
      <input type="text" class="form-control" id="birthdate-old-element" readonly>
    </div>
    <div class="form-split">
      <input class="form-control" id="birthdate-new-element" name="birthdate-new" placeholder="Enter new birthdate (mm-dd-yyyy)" data-provide="datepicker" date-date-format="mm-dd-yyyy" disabled>
    </div>
  </div>

  <!-- birthplace -->
  <div class="form-group">
    <label class="control-label">Birthplace:</label>
    <div class="form-split">
      <input type="text" class="form-control" id="birthplace-old-element" readonly>
    </div>
    <div class="form-split">
      <input type="text" class="form-control" id="baddr1-new-element" name="baddr1-new" placeholder="Enter house no. / street name / sitio" disabled>
      <select class="form-control selectpicker" id="baddr4-new-element" name="baddr4-new" data-live-search="true" data-size="5" disabled>
        <?php
        require_once($_SERVER['DOCUMENT_ROOT'].'/functions/funcAddressFunctions.php');
        getProvinceSelection();
        ?>
      </select>
      <select class="form-control selectpicker" id="baddr3-new-element" name="baddr3-new" data-live-search="true" data-size="5" disabled>
        <option selected>-- city / municipality --</option>
      </select>
      <select class="form-control selectpicker" id="baddr2-new-element" name="baddr2-new" data-live-search="true" data-size="5" disabled>
        <option selected>-- barangay --</option>
      </select>
    </div>
  </div>

  <!-- distinguishing marks -->
  <div class="form-group">
    <label class="control-label">Distinguishing Marks:</label>
    <div class="form-split">
      <textarea class="form-control" rows="3" id="marks-old-element" style="resize:none" readonly></textarea>
    </div>
    <div class="form-split">
      <textarea class="form-control" rows="3" id="marks-new-element" name="distmarks-new" placeholder="Provide any distinguishing marks present on the child" style="resize:none"disable></textarea>
    </div>
  </div>

  <!-- highest educational attainment -->
  <div class="form-group">
    <label class="control-label">Highest Educational Attainment:</label>
    <div class="form-split">
      <input type="text" class="form-control" id="education-old-element" readonly>
    </div>
    <div class="form-split">
      <select class="form-control selectpicker" id="education-new-element" name="edulevel-new" data-live-search="true" data-size="5" disabled>
        <?php
        require_once($_SERVER['DOCUMENT_ROOT'].'/functions/funcProfileListMngt.php');
        getEducationSelection();
        ?>
      </select>
    </div>
  </div>

  <!-- schooling status -->
  <div class="form-group">
    <label class="control-label">Schooling Status (Is currently studying?):</label>
    <div class="form-split">
      <input type="text" class="form-control" id="studying-old-element" readonly>
    </div>
    <div class="form-split">
      <div class="panel panel-default">
        <label class="radio-form"><input type="radio" class="form-check-input radio-form" name="study-new" value="Yes" disabled>Yes</label>
        <label class="radio-form"><input type="radio" class="form-check-input radio-form" name="study-new" value="No" disabled>No</label>
      </div>
    </div>
  </div>

  <!-- year last attended -->
  <div class="form-group">
    <label class="control-label">Year Last Attended:</label>
    <div class="form-split">
      <input type="text" class="form-control" id="year-old-element" readonly>
    </div>
    <div class="form-split">
      <input type="text" class="form-control" id="year-new-element" name="year-new" placeholder="Enter year last studied" disabled>
    </div>
  </div>

  <!-- school last attended -->
  <div class="form-group">
    <label class="control-label">School Last Attended:</label>
    <div class="form-split">
      <input type="text" class="form-control" id="school-old-element" readonly>
    </div>
    <div class="form-split">
      <input type="text" class="form-control" id="school-new-element" name="schoolstudied" placeholder="Enter school last studied in" disabled>
    </div>
  </div>

  <!-- nourishment  -->
  <div class="form-group">
    <label class="control-label" for="nourishment-element">Nourishment:</label>
    <div class="form-split">
      <input type="text" class="form-control" id="nourishment-old-element" readonly>
    </div>
    <div class="form-split">
      <div class="panel panel-default">
        <label class="radio-form"><input type="radio" class="form-check-input radio-form" name="nourishment-new" value="Normal" disabled>Normal</label>
        <label class="radio-form"><input type="radio" class="form-check-input radio-form" name="nourishment-new" value="Undernourished" disabled>Undernourished</label>
      </div>
    </div>
  </div>

  <!-- family composition -->
  <div class="form-group">
    <label class="control-label" for="fam-ctl-element">Family Composition:</label>
    <div class="form-split">
      <textarea class="form-control" rows="3" id="family-old-element" style="resize:none" readonly></textarea>
      <div class="button-list">
        <button type="button" class="btn btn-primary btn-sm" id="add-new-element" disabled>Add New Family Member</button>
        <button type="button" class="btn btn-primary btn-sm" id="edit-new-element" disabled>Edit Family Member Info</button>
      </div>
    </div>
    <div class="form-split" id="edit-area"></div>
  </div>

  <!-- status of father -->
  <div class="form-group">
    <label class="control-label">Status of Father:</label>
    <div class="form-split">
      <input type="text" class="form-control" id="fatherstat-old-element" readonly>
    </div>
    <div class="form-split">
      <div class="panel panel-default">
        <label class="radio-form"><input type="radio" class="form-check-input radio-form" name="fatherstat-new" value="Living" disabled>Living</label>
        <label class="radio-form"><input type="radio" class="form-check-input radio-form" name="fatherstat-new" value="Deceased" disabled>Deceased</label>
        <label class="radio-form"><input type="radio" class="form-check-input radio-form" name="fatherstat-new" value="N/A" disabled>Not Applicable</label>
      </div>
    </div>
  </div>

  <!-- status of mother -->
  <div class="form-group">
    <label class="control-label">Status of Mother:</label>
    <div class="form-split">
      <input type="text" class="form-control" id="motherstat-old-element" readonly>
    </div>
    <div class="form-split">
      <div class="panel panel-default">
        <label class="radio-form"><input type="radio" class="form-check-input radio-form" name="motherstat-new" value="Living" disabled>Living</label>
        <label class="radio-form"><input type="radio" class="form-check-input radio-form" name="motherstat-new" value="Deceased" disabled>Deceased</label>
        <label class="radio-form"><input type="radio" class="form-check-input radio-form"  name="motherstat-new" value="N/A" disabled>Not Applicable</label>
      </div>
    </div>
  </div>

  <!-- child living with parents -->
  <div class="form-group">
    <label class="control-label">Child Living with Parents?</label>
    <div class="form-split">
      <input type="text" class="form-control" id="childstat-old-element" readonly>
    </div>
    <div class="form-split">
      <div class="panel panel-default">
        <label class="radio-form"><input type="radio" class="form-check-input radio-form" name="childstat-new" value="Yes" disabled>Yes</label>
        <label class="radio-form"><input type="radio" class="form-check-input radio-form" name="childstat-new" value="No" disabled>No</label>
      </div>
    </div>
  </div>

  <!-- child companion/guardian -->
  <div class="form-group">
    <label class="control-label" >Guardian's  Name:</label>
    <div class="split-form">
      <input type="text" class="form-control" id="guardian-old-element" readonly>
    </div>
    <div class="split-form">
      <input type="text" class="form-control" id="guardian-new-element" name="guardian-new" placeholder="Enter child\'s current guardian" disabled>
    </div>
  </div>

  <!-- relationship to child -->
  <div class="form-group">
    <label class="control-label">Guardian's Relationship to Child:</label>
    <div class="form-split">
      <input type="text" class="form-control" id="relguardian-old-element" readonly>
    </div>
    <div class="form-split">
      <select class="form-control selectpicker" id="relguardian-new-element" name="relguardian-new" data-live-search="true" data-size="5" disabled>
        <?php
        require_once($_SERVER['DOCUMENT_ROOT'].'/functions/funcProfileListMngt.php');
        getRelationshipSelection();
        ?>
      </select>
    </div>
  </div

  <!-- guardian address -->
  <div class="form-group">
    <label class="control-label" >Guardian's Current Address:</label>
    <div class="form-split">
      <input type="text" class="form-control" id="gaddr-old-element" readonly>
    </div>
    <div class="form-split">
      <input type="text" class="form-control" id="gaddr1-new-element" name="gaddr1-new" placeholder="Enter house no. / street name / sitio" disabled>
      <select class="form-control selectpicker" id="gaddr4-new-element" name="gaddr4-new" data-live-search="true" data-size="5" disabled>
        <?php
        require_once($_SERVER['DOCUMENT_ROOT'].'/functions/funcAddressFunctions.php');
        getProvinceSelection();
        ?>
      </select>
      <select class="form-control selectpicker" id="gaddr3-new-element" name="gaddr3-new" data-live-search="true" data-size="5" disabled>
        <option selected>-- city / municipality --</option>
      </select>
      <select class="form-control selectpicker" id="gaddr2-new-element" name="gaddr2-new" data-live-search="true" data-size="5" disabled>
        <option selected>-- barangay --</option>
      </select>
    </div>
  </div>

  <!-- place of sleep -->
  <div class="form-group">
    <label class="control-label">Place of Sleep:</label>
    <div class="form-split">
      <input type="text" class="form-control" id="sleepplace-old-element" readonly>
    </div>
    <div class="form-split">
      <div class="panel panel-default">
        <label class="checkbox-form"><input type="checkbox" class="form-check-input checkbox-form" name="sleepplace[]" value="Home" disabled>Home</label>
        <label class="checkbox-form"><input type="checkbox" class="form-check-input checkbox-form" name="sleepplace[]" value="Street" disabled>Street</label>
        <label class="checkbox-form"><input type="checkbox" class="form-check-input checkbox-form" name="sleepplace[]" value="Institution" disabled>Institution</label>
        <label class="checkbox-form"><input type="checkbox" class="form-check-input checkbox-form" name="sleepplace[]" value="Parks" disabled>Parks</label>
        <label class="checkbox-form"><input type="checkbox" class="form-check-input radio-form" id="sleepplace-other-new-element" name="sleepplace[]" disabled>Others</label>
        <input class="form-control" name="sleepplace[]" placeholder="Only fill in this input when 'Others' is selected" id="sleepplace-other-new-text" disabled>
      </div>
    </div>
  </div>

  <!-- areas frequented -->
  <div class="form-group">
    <label class="control-label">Areas Frequented:</label>
    <div class="form-split">
      <input type="text" class="form-control" id="workarea-old-element" readonly>
    </div>
    <div class="form-split">
      <div class="panel panel-default">
        <label class="checkbox-form"><input type="checkbox" class="form-check-input checkbox-form" name="freqarea[]" value="Market" disabled>Market</label>
        <label class="checkbox-form"><input type="checkbox" class="form-check-input radio-form" name="freqarea[]" value="National Road" disabled>National Road</label>
        <label class="checkbox-form"><input type="checkbox" class="form-check-input checkbox-form" name="freqarea[]" value="Parks" disabled>Parks</label>
        <label class="checkbox-form"><input type="checkbox" class="form-check-input checkbox-form" name="freqarea[]" id="freqarea-other-new-element" disabled>Others</label>
        <input class="form-control" name="freqarea[]" placeholder="Only fill in this input when 'Others' is selected" id="freqarea-other-new-text" disabled>
      </div>
    </div>
  </div>

  <!-- street activities -->
  <div class="form-group">
    <label class="control-label">Street Activities:</label>
    <div class="form-split">
      <input type="text" class="form-control" id="activity-old-element" readonly>
    </div>
    <div class="form-split">
      <div class="panel panel-default">
        <label class="checkbox-form"><input type="checkbox" class="form-check-input radio-form" name="streetact[]" value="Begging" disabled>Begging</label>
        <label class="checkbox-form"><input type="checkbox" class="form-check-input checkbox-form" name="streetact[]" value="Vending" disabled>Vending</label>
        <label class="checkbox-form"><input type="checkbox" class="form-check-input checkbox-form" name="streetact[]" value="Car Washing" disabled>Car Washing</label>
        <label class="checkbox-form"><input type="checkbox" class="form-check-input checkbox-form" name="streetact[]" value="Loitering" disabled>Loitering</label>
        <label class="checkbox-form"><input type="checkbox" class="form-check-input checkbox-form" name="streetact[]" value="Scavenging" disabled>Scavenging</label>
        <label class="checkbox-form"><input type="checkbox" class="form-check-input checkbox-form" name="streetact[]" id="activity-other-element" disabled>Others</label>
        <input class="form-control" name="streetact[]" placeholder="Only fill in this input when 'Others' is selected" id="activity-other-text" disabled>
      </div>
    </div>
  </div>

  <!-- reason for activities -->
  <div class="form-group">
    <label class="control-label" for="">Reason for Activities:</label>
    <div class="form-split">
      <textarea class="form-control" rows="3" id="reason-old-element" style="resize:none" readonly></textarea>
    </div>
    <div class="form-split">
      <textarea class="form-control" rows="3" id="reason-new-element" name="reason-new" placeholder="Write here reasons for child\'s activities" style="resize:none" disabled></textarea>
    </div>
  </div>

  <!-- child status -->
  <div class="form-group-inline">
    <label class="control-label">Child Status:</label>
    <div class="form-split">
      <input type="text" class="form-control" id="profilestat-old-element" readonly>
    </div>
    <div class="form-split">
      <div class="panel panel-default">
        <label class="radio-form"><input type="radio" class="form-check-input radio-form" name="profilestat-new" value="New" disabled>New</label>
        <label class="radio-form"><input type="radio" class="form-check-input radio-form" name="profilestat-new" value="Old" disabled>Old</label>
        <label class="radio-form"><input type="radio" class="form-check-input radio-form" name="profilestat-new" value="Inactive" disabled>Inactive</label>
        <label class="radio-form"><input type="radio" class="form-check-input radio-form" name="profilestat-new" value="Returnee" disabled>Returnee</label>
      </div>
    </div>
  </div>

  <!-- legal documents available -->
  <div class="form-group">
    <label class="control-label">Documents Available:</label>
    <div class="form-split">
      <input type="text" class="form-control" id="document-old-element" readonly>
    </div>
    <div class="form-split">
      <div class="panel panel-default">
        <label class="checkbox-form"><input type="checkbox" class="form-check-input checkbox-form" name="documents[]" value="Birth Certificate" disabled>Birth Certificate</label>
        <label class="checkbox-form"><input type="checkbox" class="form-check-input checkbox-form" name="documents[]" value="Baptismal Certificate" disabled>Baptismal Certificate</label>
      </div>
    </div>
  </div>

  <!-- sacraments received -->
  <div class="form-group">
    <label class="control-label">Sacraments Received:</label>
    <div class="form-split">
      <input type="text" class="form-control" id="sacrament-old-element" readonly>
    </div>
    <div class="form-split">
      <div class="panel panel-default">
        <label class="checkbox-form"><input type="checkbox" class="form-check-input checkbox-form" name="sacraments[]" value="Baptism" disabled>Baptism</label>
        <label class="checkbox-form"><input type="checkbox" class="form-check-input checkbox-form" name="sacraments[]" value="First Communion" disabled>First Communion</label>
        <label class="checkbox-form"><input type="checkbox" class="form-check-input checkbox-form" name="sacraments[]" value="Confirmation" disabled>Confirmation</label>
      </div>
    </div>
  </div>

<!-- assessment -->
  <div class="form-group">
    <label class="control-label" for="assessment-element">Assessment / Worker Plans:</label>
    <div class="form-split">
      <textarea class="form-control" rows="3" id="assessment-old-element" style="resize:none" readonly></textarea>
    </div>
    <div class="form-split">
      <textarea class="form-control" rows="3" id="assessment-new-element" name="assessment" placeholder="Write here worker's assessment regarding the child" style="resize:none" disabled></textarea>
    </div>
  </div>

  <!-- buttons -->
  <div class="form-group">
    <button type="submit" class="btn btn-primary btn-sm" id="edit-submit" disabled>Submit</button>
    <button type="button" class="btn btn-default btn-sm" id="edit-reset" disabled>Reset</button>
    <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Cancel</button>
  </div>
</form>
