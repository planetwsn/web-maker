<form action="/../procs/procLogin.php?reset" method="post">

  <div class="form-group">
    <label class="control-label" for="recovery-element">Recovery email</label>
    <input type="text" class="form-control" id="recovery-element" name="email">
  </div>

  <!-- buttons -->
  <div class="form-group">
    <button type="submit" name="reset" class="btn btn-primary btn-sm">Submit</button>
    <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Cancel</button>
  </div>
</form>
