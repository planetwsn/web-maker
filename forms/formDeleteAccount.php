<form action="/../procs/procAccountsMngt?delacc" method="post">
  <!-- account to remove -->
  <div class="form-group">
    <label class="control-label" for="rem-account">Select Account</label>
    <select class="form-control selectpicker" id="rem-account" name="aid" data-live-search="true" data-size="7" required>
      <option selected disabled>Select an account to remove...</option>
      <?php
      require_once($_SERVER['DOCUMENT_ROOT'].'/functions/funcAccountsMngt.php');
      getAccountSelection(); ?>
    </select>
  </div>

  <!-- buttons -->
  <div class="form-group">
    <button type="submit" class="btn btn-primary btn-sm">Submit</button>
    <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Cancel</button>
  </div>
</form>
