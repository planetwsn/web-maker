<form action="/../procs/procAccountsMngt?editpersonal" method="post">

  <!-- Name -->
  <div class="form-group">
    <label class="control-label" for="account-name-element">Name</label>
    <input type="text" class="form-control" id="account-name-element" name="name" placeholder="Change your account name">
  </div>

  <div class="form-group">
    <label class="control-label" for="username-element">Username</label>
    <input type="text" class="form-control" id="username-element" name="username" placeholder="Change your username">
  </div>

  <div class="form-group">
    <label class="control-label" for="password-element">Password</label>
    <input type="password" class="form-control" id="password-element" name="password" placeholder="Change your password">
  </div>

  <div class="form-group">
    <label class="control-label" for="email-element">Email</label>
    <input type="text" class="form-control" id="email-element" name="email" placeholder="Update your email">
  </div>

  <div class="form-group">
    <label class="control-label" for="phone-element">Contact Number</label>
    <input type="text" class="form-control" id="phone-element" name="phone" placeholder="Update your contact number">
  </div>

  <!-- buttons -->
  <div class="form-group">
    <button type="submit" class="btn btn-primary btn-sm" id="edit-personal-submit">Submit</button>
    <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Cancel</button>
  </div>
</form>
