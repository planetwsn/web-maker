<form action="/../procs/procProfileListMngt?printprofile" method="post">

  <!-- profile -->
  <div class="form-group">
    <label class="control-label" for="print-item-element">Profile</label>
    <select class="form-control selectpicker" id="print-item-element" name="profile" data-live-search="true" data-size="5" required>
      <?php
      require_once($_SERVER['DOCUMENT_ROOT'].'/functions/funcProfileListMngt.php');
      getProfileListSelection();
      ?>
    </select>
  </div>


  <!-- paper size; take note THAT FOLIO IS PREDEFINED IN PDF LIBRARY TO BE 8.5 x 13 in. -->
  <div class="form-group">
    <label class="control-label" for="size-element">Paper Size</label>
    <select class="selectpicker form-control" id="size-element" name="size" data-live-search="true" data-size="4">
      <option selected value='A4'>A4</option>
      <option value='LETTER'>Letter (8.5 x 11.0)</option>
      <option value='FOLIO'>Legal (8.5 x 13.0)</option>
    </select>
  </div>

  <!-- orientation -->
  <div class="form-group">
    <label class="control-label" for="orientation-element">Paper Size</label>
    <select class="selectpicker form-control" id="orientation-element" name="orientation" data-live-search="true" data-size="4">
      <option selected value='P'>Portrait</option>
      <option value='L'>Landscape</option>
    </select>
  </div>

  <!-- Social worker / Volunteer assigned -->
  <div class="form-group">
    <label class="control-label" for="printer-element">Prepared By:</label>
    <input type="text" class="form-control" id="printer-element" placeholder="<?php echo $_SESSION['login-user']; ?>" disabled>
  </div>

  <!-- buttons -->
  <div class="form-group">
    <button type="submit" class="btn btn-primary btn-sm">Print Preview</button>
    <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Cancel</button>
  </div>
</form>
