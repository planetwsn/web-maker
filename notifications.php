<?php
if(isset($_SESSION['query_error'])){
  ?>
  <script>
  console.log("Updating error");
  if(sessionStorage){
    sessionStorage.setItem('warn', "<?php echo $_SESSION['query_error']; ?>");
  }
  else{
    $.alert("Your browser does not support session storage.");
  }
  </script>
  <?php
  unset($_SESSION['query_error']);
} elseif (isset($_SESSION['query_info'])){
  ?>
  <script>
  if(sessionStorage){
    sessionStorage.setItem('info', "<?php echo $_SESSION['query_info']; ?>");
  }
  else{
    $.alert("Your browser does not support session storage.");
  }
  </script>
  <?php
  unset($_SESSION['query_info']);
} elseif (isset($_SESSION['query_success'])){
  ?>
  <script>
  console.log("Updating success");
  if(sessionStorage){
    sessionStorage.setItem('check', "<?php echo $_SESSION['query_success']; ?>");
  }
  else{
    $.alert("Your browser does not support session storage.");
  }
  </script>
  <?php
  unset($_SESSION['query_success']);
}elseif (isset($_SESSION['query_debug'])){
  ?>
  <script>
  console.log("Updating debug");
  if(sessionStorage){
    sessionStorage.setItem('debug', "<?php echo $_SESSION['query_debug']; ?>");
    $.alert(sessionStorage.getItem('disp'),{type: 'warning', duration: 5000});
  }
  else{
    $.alert("Your browser does not support session storage.");
  }
  </script>
  <?php
  unset($_SESSION['query_debug']);
}else{ ?>
  <script>
  // console.log(sessionStorage.getItem('disp'));
  </script>
<?php
}
?>
