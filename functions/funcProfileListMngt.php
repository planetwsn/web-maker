<?php
include_once($_SERVER['DOCUMENT_ROOT'].'/functions/funcDatabaseConnection.php');

function getProfileListSearchResults(){
  require_once($_SERVER['DOCUMENT_ROOT'].'/functions/funcGenericFunctions.php');
  try{
    $con = getDatabaseConnection();

    $q = "SELECT show_active FROM settings ORDER BY setting_id DESC LIMIT 1";
    $r = mysqli_query($con, $q);
    $res = mysqli_fetch_array($r);
    $stat = $res['show_active'];

    if(isset($_GET['search'])){
      $search = mysqli_real_escape_string($con, $_GET['search']); //do not use cleanData function to allow blank queries
      unset($_GET['search']);

      if (""==trim($search)) {
        unset($_SESSION['searchitem']);
        return 1;
      }
      elseif (!isset($_SESSION['searchitem'])||($search != $_SESSION['searchitem']))
        $_SESSION['searchitem'] = $search;
      else {
        $search = $_SESSION['searchitem'];
      }
    }else{
      if(isset($_SESSION['searchitem']))
        $search = $_SESSION['searchitem'];
      else {
        $search = "";
      }
    }

    if($search == mb_strtolower('Active')){
      $ss1 = 'New';
      $ss2 = 'Returnee';
      $ss3 = 'Old';
    }else{
      $ss1 = $search;
      $ss2 = 0;
      $ss3 = 0;
    }

    $query = "SELECT address_id FROM address WHERE street_name LIKE '%".$search."%'";
    $r = mysqli_query($con, $query);
    if (mysqli_num_rows($r) > 0){
      $row = mysqli_fetch_array($r);
      $addr1 = $row['address_id'];
    }
    else{
      $addr1 = 0; //surely there is no entry with id = 0
    }

    $query = "SELECT barangay_id FROM barangay WHERE barangay_name LIKE '%".$search."%'";
    $r = mysqli_query($con, $query);
    if (mysqli_num_rows($r)>0){
      $row = mysqli_fetch_array($r);
      $addr2 = $row['barangay_id'];
    }
    else{
      $addr2 = 0;
    }

    $query = "SELECT locality_id FROM locality WHERE locality_name LIKE '%".$search."%'";
    $r=mysqli_query($con, $query);
    if(mysqli_num_rows($r)>0){
  	  $row = mysqli_fetch_array($r);
  	  $addr3 = $row['locality_id'];
    }
    else{
      $addr3 = 0;
    }

    $query = "SELECT province_id FROM province WHERE province_name LIKE '%".$search."%'";
    $r=mysqli_query($con, $query);
    if(mysqli_num_rows($r)>0){
  	  $row = mysqli_fetch_array($r);
  	  $addr4 = $row['province_id'];
    }
    else{
      $addr4 = 0;
    }

    $query = "SELECT activity_id FROM activity WHERE activity_name LIKE '%".$search."%'";
    $r=mysqli_query($con, $query);
    if(mysqli_num_rows($r)>0){
  	  $row = mysqli_fetch_array($r);
  	  $act = $row['activity_id'];
    }
    else{
      $act = 0;
    }

    $query = "SELECT document_id FROM document WHERE document_name LIKE '%".$search."%'";
    $r=mysqli_query($con, $query);
    if(mysqli_num_rows($r)>0){
  	  $row = mysqli_fetch_array($r);
  	  $doc = $row['document_id'];
    }
    else{
      $doc = 0;
    }

    $query = "SELECT area_id FROM sleeping_area WHERE area_name LIKE '%".$search."%'";
    $r=mysqli_query($con, $query);
    if(mysqli_num_rows($r)>0){
  	  $row = mysqli_fetch_array($r);
  	  $saq = $row['area_id'];
    }
    else{
      $saq = 0;
    }

    $query = "SELECT area_id FROM working_area WHERE area_name LIKE '%".$search."%'";
    $r=mysqli_query($con, $query);
    if(mysqli_num_rows($r)>0){
  	  $row = mysqli_fetch_array($r);
  	  $waq = $row['area_id'];
    }
    else{
      $waq = 0;
    }

    $query = "SELECT relative_id FROM relative WHERE relative_name LIKE '%".$search."%' OR contact LIKE '%".$search."%'";
    $r = mysqli_query($con, $query);
    $rel_fltr = "OR (";
    while($row = mysqli_fetch_array($r)){
  	  $rel_fltr .= "pr.relative_id=".$row['relative_id']." OR ";
    }
    $rel_fltr .= "0)";

    if(isset($_GET['results'])){
      $results = $_GET['results'];
    }else{
      $results = 1;
    }
    $offset = ($results - 1)*10;

    if($stat == 1){ //active profiles
      $preq = "SELECT profile_id  FROM active_profiles";
      $r = mysqli_query($con, $preq);
      $filter = "AND ("; //conditions to filter active profiles in subsequent search query
      while($profs = mysqli_fetch_array($r)){
        $filter .= " p.profile_id =".$profs['profile_id']." OR";
      }
      $filter .=" 0)";

      $queryc = "SELECT * FROM profile AS p JOIN address AS a ON a.address_id=p.address_id JOIN barangay AS b ON b.barangay_id=a.barangay_id JOIN locality AS l ON l.locality_id=b.locality_id JOIN province as v ON v.province_id=l.province_id JOIN profile_activities AS pa ON p.profile_id = pa.profile_id JOIN profile_documents AS pd ON p.profile_id = pd.profile_id JOIN profile_sleep_areas AS sa ON p.profile_id = sa.profile_id JOIN profile_work_areas AS wa JOIN profile_relatives AS pr ON p.profile_id = pr.profile_id WHERE (p.name LIKE '%".$search."%' OR p.alias LIKE '%".$search."%' OR p.age LIKE '%".$search."%' OR p.child_phone LIKE '%".$search."%' OR p.status_id='".$ss1."' OR p.status_id='".$ss2."' OR p.status_id='".$ss3."' OR p.address_id=".$addr1." OR a.barangay_id=".$addr2." OR b.locality_id=".$addr3." OR l.province_id=".$addr4." OR pa.activity_id=".$act." OR pd.document_id=".$doc." OR sa.area_id=".$saq." OR wa.area_id=".$waq." ".$rel_fltr.") ".$filter." GROUP BY p.profile_id";

      $query = "SELECT p.profile_id,p.status_id,p.name FROM profile AS p JOIN address AS a ON a.address_id=p.address_id JOIN barangay AS b ON b.barangay_id=a.barangay_id JOIN locality AS l ON l.locality_id=b.locality_id JOIN province as v ON v.province_id=l.province_id JOIN profile_activities AS pa ON p.profile_id = pa.profile_id JOIN profile_documents AS pd ON p.profile_id = pd.profile_id JOIN profile_sleep_areas AS sa ON p.profile_id = sa.profile_id JOIN profile_work_areas AS wa JOIN profile_relatives AS pr ON p.profile_id = pr.profile_id WHERE (p.name LIKE '%".$search."%' OR p.alias LIKE '%".$search."%' OR p.age LIKE '%".$search."%' OR p.child_phone LIKE '%".$search."%' OR p.status_id='".$ss1."' OR p.status_id='".$ss2."' OR p.status_id='".$ss3."' OR p.address_id=".$addr1." OR a.barangay_id=".$addr2." OR b.locality_id=".$addr3." OR l.province_id=".$addr4." OR pa.activity_id=".$act." OR pd.document_id=".$doc." OR sa.area_id=".$saq." OR wa.area_id=".$waq." ".$rel_fltr.") ".$filter." GROUP BY p.profile_id LIMIT ".$offset.",10";
    }else{
      $queryc = "SELECT * FROM profile AS p JOIN address AS a ON a.address_id=p.address_id JOIN barangay AS b ON b.barangay_id=a.barangay_id JOIN locality AS l ON l.locality_id=b.locality_id JOIN province as v ON v.province_id=l.province_id JOIN profile_activities AS pa ON p.profile_id = pa.profile_id JOIN profile_documents AS pd ON p.profile_id = pd.profile_id JOIN profile_sleep_areas AS sa ON p.profile_id = sa.profile_id JOIN profile_work_areas AS wa JOIN profile_relatives AS pr ON p.profile_id = pr.profile_id WHERE p.name LIKE '%".$search."%' OR p.alias LIKE '%".$search."%' OR p.age LIKE '%".$search."%' OR p.child_phone LIKE '%".$search."%' OR p.status_id='".$ss1."' OR p.status_id='".$ss2."' OR p.status_id='".$ss3."' OR p.address_id=".$addr1." OR a.barangay_id=".$addr2." OR b.locality_id=".$addr3." OR l.province_id=".$addr4." OR pa.activity_id=".$act." OR pd.document_id=".$doc." OR sa.area_id=".$saq." OR wa.area_id=".$waq." ".$rel_fltr." GROUP BY p.profile_id";

      $query = "SELECT p.profile_id,p.status_id,p.name FROM profile AS p JOIN address AS a ON a.address_id=p.address_id JOIN barangay AS b ON b.barangay_id=a.barangay_id JOIN locality AS l ON l.locality_id=b.locality_id JOIN province as v ON v.province_id=l.province_id JOIN profile_activities AS pa ON p.profile_id = pa.profile_id JOIN profile_documents AS pd ON p.profile_id = pd.profile_id JOIN profile_sleep_areas AS sa ON p.profile_id = sa.profile_id JOIN profile_work_areas AS wa JOIN profile_relatives AS pr ON p.profile_id = pr.profile_id WHERE p.name LIKE '%".$search."%' OR p.alias LIKE '%".$search."%' OR p.age LIKE '%".$search."%' OR p.child_phone LIKE '%".$search."%' OR p.status_id='".$ss1."' OR p.status_id='".$ss2."' OR p.status_id='".$ss3."' OR p.address_id=".$addr1." OR a.barangay_id=".$addr2." OR b.locality_id=".$addr3." OR l.province_id=".$addr4." OR pa.activity_id=".$act." OR pd.document_id=".$doc." OR sa.area_id=".$saq." OR wa.area_id=".$waq." ".$rel_fltr." GROUP BY p.profile_id LIMIT ".$offset.",10";
    }

    $result = mysqli_query($con, $queryc);
    $total_rows = mysqli_num_rows($result);
    $total_pages = ceil($total_rows/10);
    paginateResult($results, $total_pages, $total_rows);

    $colNames = array("Profile No.", "Name", "Status");
    $width=array(20,60,20);
    printTableHead($colNames,$width);

    if($r = mysqli_query($con, $query)){
      while($row = mysqli_fetch_array($r)){
        if($row['status_id'] == 'New' || $row['status_id'] == 'Returnee' || $row['status_id'] == 'Old'){
          print_r("<tr class=\"clickable success\">");
          print_r("<td>". $row['profile_id'] ."</td>");
          print_r("<td>". $row['name']."</td>");
          print_r("<td>Active</td>");
        }
        else {
          print_r("<tr class=\"clickable danger\">");
          print_r("<td>". $row['profile_id'] ."</td>");
          print_r("<td>". $row['name']."</td>");
          print_r("<td>Inactive</td>");
        }
        print_r("</tr>");
      }
    }
    else{
      print_r("<tr>");
      print_r("<td>".mysqli_error($con)."</td>");
      print_r("</tr>");
    }
  printTableFoot();
  }catch(Exception $e){
    $_SESSION['query_error']="Exception occured: ".$e;
  }catch(Error $e){
    $_SESSION['query_error']="Error occured: ".$e;
  }finally{
    mysqli_close($con);
  }
}

function getProfileListSelection(){
  $con = getDatabaseConnection();
	$query = "SELECT profile_id, name FROM profile ORDER BY profile_id ASC";
	$r = mysqli_query($con,$query);
	$nrows = mysqli_num_rows($r);
	echo "<option selected value=0 disabled>-- select a profile --</option>";
	while ($nrows > 0){
		$row = mysqli_fetch_array($r);
		echo "<option value=".$row['profile_id'].">" . $row['profile_id'] . " - ". $row['name'] ."</option>";
		$nrows = $nrows - 1;
	}
	mysqli_close($con);
}

function getActiveProfileListSelection(){
  $con = getDatabaseConnection();
	$query = "SELECT * FROM active_profiles ORDER BY log_id ASC";
	$r = mysqli_query($con,$query);
	$nrows = mysqli_num_rows($r);
	echo "<option selected value=0 disabled>-- select a profile --</option>";
	while ($nrows > 0){
		$row = mysqli_fetch_array($r);
    $q = "SELECT name FROM profile WHERE profile_id=".$row['profile_id'];
    $nr = mysqli_query($con,$q);
    $nrow = mysqli_fetch_array($nr);
		echo "<option value=".$row['profile_id'].">" . $row['profile_id'] . " - ". $nrow['name'] ."</option>";
		$nrows = $nrows - 1;
	}
	mysqli_close($con);
}

function getInactiveProfileListSelection(){
  $con = getDatabaseConnection();
	$query = "SELECT * FROM inactive_profiles ORDER BY log_id ASC";
	$r = mysqli_query($con,$query);
	$nrows = mysqli_num_rows($r);
	echo "<option selected value=0 disabled>-- select a profile --</option>";
	while ($nrows > 0){
		$row = mysqli_fetch_array($r);
    $q = "SELECT name FROM profile WHERE profile_id=".$row['profile_id'];
    $nr = mysqli_query($con,$q);
    $nrow = mysqli_fetch_array($nr);
		echo "<option value=".$row['profile_id'].">" . $row['profile_id'] . " - ". $nrow['name'] ."</option>";
		$nrows = $nrows - 1;
	}
	mysqli_close($con);
}

function getEducationSelection(){
  $con = getDatabaseConnection();
	$query = "SELECT * FROM educational_level ORDER BY level_id ASC";
	$result = mysqli_query($con,$query);
	$nrows = mysqli_num_rows($result);
  echo "<option selected value=0 disabled>-- select highest educational attainment --</option>";
	while ($nrows > 0){
		$row = mysqli_fetch_array($result);
		echo "<option value=".$row['level_id'].">". $row['level_name'] ."</option>";
		$nrows = $nrows - 1;
	}
	mysqli_close($con);
}

function checkEducationByArray($eduArr){
  $con = getDatabaseConnection();
	$query = "SELECT * FROM education WHERE level_id =".$eduArr['lvl']." AND last_year_attended='".$eduArr['yr']."' AND last_school_attended='".$eduArr['school']."' AND still_studying='".$eduArr['stat']."'";
	$result = mysqli_query($con,$query);
  mysqli_close($con);
  if(mysqli_num_rows($result) > 0){
    return 1;
  }else{
	  return 0;
  }
}

function getEducationIdByArray($eduArr){
  $con = getDatabaseConnection();
  $query = "SELECT education_id FROM education WHERE level_id =".$eduArr['lvl']." AND last_year_attended='".$eduArr['yr']."' AND last_school_attended='".$eduArr['school']."' AND still_studying='".$eduArr['stat']."'";
	$result = mysqli_query($con,$query);
  $r = mysqli_fetch_array($result);
  mysqli_close($con);
  return $r['education_id'];
}

function checkRelativeByArray($relArr){
  $con = getDatabaseConnection();
	$query = "SELECT * FROM relative WHERE relative_name ='".$relArr['name']."' AND relationship_id=".$relArr['rel'];
	$result = mysqli_query($con,$query);
  mysqli_close($con);
  if(mysqli_num_rows($result) > 0){
    return 1;
  }else{
	  return 0;
  }
}

function getRelativeIdByArray($relArr){
  $con = getDatabaseConnection();
  $query = "SELECT relative_id FROM relative WHERE relative_name ='".$relArr['name']."' AND relationship_id=".$relArr['rel'];
  $result = mysqli_query($con,$query);
  $r = mysqli_fetch_array($result);
  mysqli_close($con);
  return $r['relative_id'];
}

function getRelationshipSelection(){
  $con = getDatabaseConnection();
	$query = "SELECT * FROM relationship ORDER BY relationship_id ASC";
	$result = mysqli_query($con,$query);
	$nrows = mysqli_num_rows($result);
  echo "<option selected disabled>-- select relationship to person --</option>";
	while ($nrows > 0){
		$row = mysqli_fetch_array($result);
		echo "<option value=".$row['relationship_id'].">". $row['relationship_name'] ."</option>";
		$nrows = $nrows - 1;
	}
	mysqli_close($con);
}

function getProfileByArray($arr){
  $con = getDatabaseConnection();
  $query = "SELECT * FROM profile WHERE name ='".$arr['name']."' AND address_id=".$arr['caddr']." AND age=".$arr['age'];
  $result = mysqli_query($con,$query);
  $r = mysqli_fetch_array($result);
  mysqli_close($con);
  return $r['profile_id'];
}

function checkProfileRelativesByArray($arr){
  $con = getDatabaseConnection();
	$query = "SELECT * FROM profile_relatives WHERE profile_id =".$arr['profile']." AND relative_id=".$arr['relative'];
	$result = mysqli_query($con,$query);
  mysqli_close($con);
  if(mysqli_num_rows($result) > 0){
    return 1;
  }else{
	  return 0;
  }
}

function checkProfileSleepAreasByArray($arr){
  $con = getDatabaseConnection();
	$query = "SELECT * FROM profile_sleep_areas WHERE profile_id =".$arr['profile']." AND area_id=".$arr['area_id'];
	$result = mysqli_query($con,$query);
  mysqli_close($con);
  if(mysqli_num_rows($result) > 0){
    return 1;
  }else{
	  return 0;
  }
}

function checkProfileWorkAreasByArray($arr){
  $con = getDatabaseConnection();
	$query = "SELECT * FROM profile_work_areas WHERE profile_id =".$arr['profile']." AND area_id=".$arr['area_id'];
	$result = mysqli_query($con,$query);
  mysqli_close($con);
  if(mysqli_num_rows($result) > 0){
    return 1;
  }else{
	  return 0;
  }
}

function checkSleepingAreaByAreaName($p){
  $con = getDatabaseConnection();
	$query = "SELECT * FROM sleeping_area WHERE area_name ='".$p."'";
  $result = mysqli_query($con,$query);
  mysqli_close($con);
  if(mysqli_num_rows($result) > 0){
    return 1;
  }else{
	  return 0;
  }
}

function checkWorkAreaByAreaName($p){
  $con = getDatabaseConnection();
	$query = "SELECT * FROM working_area WHERE area_name ='".$p."'";
  $result = mysqli_query($con,$query);
  mysqli_close($con);
  if(mysqli_num_rows($result) > 0){
    return 1;
  }else{
	  return 0;
  }
}

function getSleepAreaIdByAreaName($p){
  $con = getDatabaseConnection();
	$query = "SELECT area_id FROM sleeping_area WHERE area_name ='".$p."'";
  $result = mysqli_query($con,$query);
  $r = mysqli_fetch_array($result);
  mysqli_close($con);
  return $r['area_id'];
}

function getWorkAreaIdByAreaName($p){
  $con = getDatabaseConnection();
	$query = "SELECT area_id FROM working_area WHERE area_name ='".$p."'";
  $result = mysqli_query($con,$query);
  $r = mysqli_fetch_array($result);
  mysqli_close($con);
  return $r['area_id'];
}

function checkActivityByActivityName($p){
  $con = getDatabaseConnection();
	$query = "SELECT * FROM activity WHERE activity_name ='".$p."'";
  $result = mysqli_query($con,$query);
  mysqli_close($con);
  if(mysqli_num_rows($result) > 0){
    return 1;
  }else{
	  return 0;
  }
}

function checkProfileActivitiesByArray($arr){
  $con = getDatabaseConnection();
	$query = "SELECT * FROM profile_activities WHERE profile_id =".$arr['profile']." AND activity_id=".$arr['activity_id'];
	$result = mysqli_query($con,$query);
  mysqli_close($con);
  if(mysqli_num_rows($result) > 0){
    return 1;
  }else{
	  return 0;
  }
}

function getActivityIdByActivityName($p){
  $con = getDatabaseConnection();
	$query = "SELECT activity_id FROM activity WHERE activity_name ='".$p."'";
  $result = mysqli_query($con,$query);
  $r = mysqli_fetch_array($result);
  mysqli_close($con);
  return $r['activity_id'];
}

function checkDocumentByDocumentName($p){
  $con = getDatabaseConnection();
	$query = "SELECT * FROM document WHERE document_name ='".$p."'";
  $result = mysqli_query($con,$query);
  mysqli_close($con);
  if(mysqli_num_rows($result) > 0){
    return 1;
  }else{
	  return 0;
  }
}

function checkProfileDocumentsByArray($arr){
  $con = getDatabaseConnection();
	$query = "SELECT * FROM profile_documents WHERE profile_id =".$arr['profile']." AND document_id=".$arr['document_id'];
	$result = mysqli_query($con,$query);
  mysqli_close($con);
  if(mysqli_num_rows($result) > 0){
    return 1;
  }else{
	  return 0;
  }
}

function getDocumentIdByDocumentName($p){
  $con = getDatabaseConnection();
	$query = "SELECT document_id FROM document WHERE document_name ='".$p."'";
  $result = mysqli_query($con,$query);
  $r = mysqli_fetch_array($result);
  mysqli_close($con);
  return $r['document_id'];
}

function checkSacramentBySacramentName($p){
  $con = getDatabaseConnection();
	$query = "SELECT * FROM sacrament WHERE sacrament_name ='".$p."'";
  $result = mysqli_query($con,$query);
  mysqli_close($con);
  if(mysqli_num_rows($result) > 0){
    return 1;
  }else{
	  return 0;
  }
}

function checkProfileSacramentsByArray($arr){
  $con = getDatabaseConnection();
	$query = "SELECT * FROM profile_sacraments WHERE profile_id =".$arr['profile']." AND sacrament_id=".$arr['sacrament_id'];
	$result = mysqli_query($con,$query);
  mysqli_close($con);
  if(mysqli_num_rows($result) > 0){
    return 1;
  }else{
	  return 0;
  }
}

function getSacramentIdBySacramentName($p){
  $con = getDatabaseConnection();
	$query = "SELECT sacrament_id FROM sacrament WHERE sacrament_name ='".$p."'";
  $result = mysqli_query($con,$query);
  $r = mysqli_fetch_array($result);
  mysqli_close($con);
  return $r['sacrament_id'];
}

function checkProfileByArray($arr){
  $con = getDatabaseConnection();
	$query = "SELECT * FROM profile WHERE name ='".$arr['name']."' AND address_id=".$arr['caddr'];
	$result = mysqli_query($con,$query);
  mysqli_close($con);
  if(mysqli_num_rows($result) > 0){
    return 1;
  }else{
	  return 0;
  }
}

function getProfileByProfileId($id){
  $con = getDatabaseConnection();
  $q = "SELECT * FROM profile WHERE profile_id=".$id;
  $r = mysqli_query($con,$q);
  $s = mysqli_fetch_array($r);
  mysqli_close($con);
  return $s;
}

function getEducationByEducationId($id){
  $con = getDatabaseConnection();
  $q = "SELECT * FROM education WHERE education_id=".$id;
  $r = mysqli_query($con,$q);
  $s = mysqli_fetch_array($r);
  mysqli_close($con);
  return $s;
}
?>
