<?php
require_once($_SERVER['DOCUMENT_ROOT'].'/functions/funcDatabaseConnection.php');

function getMatName($matID){
  $con = getDatabaseConnection();
  $query = "SELECT materialName FROM materials WHERE materialID=".$matID;
  $result = mysqli_query($con, $query);
  $row = mysqli_fetch_array($result);
  mysqli_close($con);
  return $row['materialName'];
}

function fetchInventory(){
  $output = '';
  $con = getDatabaseConnection();

  $query = "SELECT i.* FROM inventory AS i JOIN materials AS ma ON ma.materialID=i.materialID JOIN models AS mo ON mo.modelID=i.modelID ORDER BY ma.materialName ASC, mo.modelName ASC, i.serialNumber ASC";
  $ret = mysqli_query($con, $query);
  $nrows = mysqli_num_rows($ret);
  while($nrows > 0){
    $row = mysqli_fetch_array($ret);
    $query = "SELECT materialName FROM materials WHERE materialID =" . $row['materialID'];
    $r1 = mysqli_query($con, $query);
    $mat = mysqli_fetch_array($r1);
    $query = "SELECT modelName FROM models WHERE modelID =" . $row['modelID'];
    $r2 = mysqli_query($con, $query);
    $mod = mysqli_fetch_array($r2);
    $query = "SELECT realname FROM accounts WHERE accountID =" . $row['custodian'];
    $r3 = mysqli_query($con, $query);
    $cus = mysqli_fetch_array($r3);
    $output .= "<tr>";
    $output .= "<th width='16%'>". $mat['materialName'] ."</th>";
    $output .=" <th width='16%'>". $mod['modelName']."</th>";
    $output .= "<th width='16%'>". $row['serialNumber'] . "</th>";
    $output .= "<th width='16%'>". $row['dateAcquired'] ."</th>";
    $output .= "<th width='16%'>". $cus['realname']."</th>";
    $output .= "<th width='16%'>". $row['remarks'] . "</th>";
    $output .= "</tr><hr>";
    $nrows -= 1;
  }
  mysqli_close($con);
  return $output;
}

?>
