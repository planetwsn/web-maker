<?php
require_once($_SERVER['DOCUMENT_ROOT'].'/functions/funcDatabaseConnection.php');

function getBarangaySelection(){
  $con = getDatabaseConnection();
  $query = "SELECT * FROM barangay ORDER BY barangay_name ASC";
  $result = mysqli_query($con,$query);
  $nrows = mysqli_num_rows($result);
  echo "<option selected value=0 disabled>-- select a barangay --</option>";
  while ($nrows > 0){
    $row = mysqli_fetch_array($result);
    echo "<option value=".$row['barangay_id'].">" . $row['barangay_name'] . "</option>";
    $nrows = $nrows - 1;
  }
  mysqli_close($con);
}

function getLocalitySelection(){
  $con = getDatabaseConnection();
  $query = "SELECT * FROM locality ORDER BY locality_name ASC";
  $result = mysqli_query($con,$query);
  $nrows = mysqli_num_rows($result);
  echo "<option selected value=0 disabled>-- select a locality --</option>";
  while ($nrows > 0){
    $row = mysqli_fetch_array($result);
    echo "<option value=".$row['locality_id'].">" . $row['locality_name'] . "</option>";
    $nrows = $nrows - 1;
  }
  mysqli_close($con);
}

function getProvinceSelection(){
  $con = getDatabaseConnection();
	$query = "SELECT * FROM province ORDER BY province_name ASC";
	$result = mysqli_query($con,$query);
	$nrows = mysqli_num_rows($result);
  echo "<option selected value=0 disabled>-- select a province --</option>";
	while ($nrows > 0){
		$row = mysqli_fetch_array($result);
		echo "<option value=".$row['province_id'].">" . $row['province_name'] . "</option>";
		$nrows = $nrows - 1;
	}
	mysqli_close($con);
}

function checkAddressByArray($addrArr){
  $con = getDatabaseConnection();
	$query = "SELECT * FROM address WHERE ";
  if($addrArr['addr1'] == 'NULL'){
    $query.="street_name IS NULL AND ";
  }else{
    $query.="street_name='".$addrArr['addr1']."' AND ";
  }
  if($addrArr['addr2'] == 'NULL'){
    $query.="barangay_id IS NULL AND ";
  }else{
    $query.="barangay_id=".$addrArr['addr2']." AND ";
  }
  if($addrArr['addr3'] == 'NULL'){
    $query.="locality_id IS NULL";
  }else{
    $query.="locality_id=".$addrArr['addr3'];
  }

	$result = mysqli_query($con,$query);
  if(mysqli_num_rows($result) > 0){
    $r = mysqli_fetch_array($result);
    mysqli_close($con);
    return 1;
  }else{
    mysqli_close($con);
	  return 0;
  }
}

function getAddressIdByArray($addrArr){
  $con = getDatabaseConnection();
  $query = "SELECT * FROM address WHERE ";
  if($addrArr['addr1'] == 'NULL'){
    $query.="street_name IS NULL AND ";
  }else{
    $query.="street_name='".$addrArr['addr1']."' AND ";
  }
  if($addrArr['addr2'] == 'NULL'){
    $query.="barangay_id IS NULL AND ";
  }else{
    $query.="barangay_id=".$addrArr['addr2']." AND ";
  }
  if($addrArr['addr3'] == 'NULL'){
    $query.="locality_id IS NULL";
  }else{
    $query.="locality_id=".$addrArr['addr3'];
  }
	$result = mysqli_query($con,$query);
  $r = mysqli_fetch_array($result);
  mysqli_close($con);
  if (!is_null($r)){
    return $r['address_id'];
  }else{
    return -1;
  }

}

function getAddressIdByProfileId($p){
  $con = getDatabaseConnection();
	$query = "SELECT address_id FROM profile WHERE profile_id=".$p;
	$result = mysqli_query($con,$query);
  $r = mysqli_fetch_array($result);
  mysqli_close($con);
  return $r['address_id'];
}
?>
