<?php
require_once($_SERVER['DOCUMENT_ROOT'].'/functions/funcDatabaseConnection.php');

function getPublishedPosts(){
  $posts = NULL;
  try{
    $con = getDatabaseConnection();
    $query = "SELECT * FROM posts WHERE published=true ORDER BY created_at DESC";
    $result = mysqli_query($con, $query);
    $posts = mysqli_fetch_all($result, MYSQLI_ASSOC);
  }catch(Exception $e){
    $_SESSION['query_error'] = $e;
  }catch(Error $e){
    $_SESSION['query_error'] = $e;
  }finally{
    mysqli_close($con);
  }
  return $posts;
}

function getDateCreatedByPostId($id){
  try{
    $con = getDatabaseConnection();
    $query = "SELECT created_at FROM posts WHERE post_id=".$id;
    $result = mysqli_query($con, $query);
    $r = mysqli_fetch_array($result);
  }catch(Exception $e){
    $_SESSION['query_error'] = $e;
  }catch(Error $e){
    $_SESSION['query_error'] = $e;
  }finally{
    mysqli_close($con);
    return $r['created_at'];
  }
  return 0;
}
 ?>
