$(document).ready(function(){
  toastr.options={
    "closeButton": false,
    "debug":false,
    "newestOnTop":true,
    "progressBar": true,
    "positionClass": "toast-top-right",
    "showDuration": "300",
    "hideDuration": "1000",
    "timeOut": "5000",
    "showMethod": "fadeIn",
    "hideMethod": "fadeOut"
  }
  if(sessionStorage.getItem('info') != null){
    toastr.info(sessionStorage.getItem('info'));
    sessionStorage.removeItem('info');
  }

  if(sessionStorage.getItem('check') != null){
    toastr.success(sessionStorage.getItem('check'));
    sessionStorage.removeItem('check');
  }

  if(sessionStorage.getItem('warn') != null){
    toastr.error(sessionStorage.getItem('warn'));
    sessionStorage.removeItem('warn');
  }
});
