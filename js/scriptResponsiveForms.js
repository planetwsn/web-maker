$(document).ready(function(){

  var uniqueAddID = (function() {
     var id = 0;
     function changeBy(val){
       id += val;
     }
     return{
       increment: function(){
         changeBy(1);
         return id;
       },
       decrement: function(){
         changeBy(-1);
         return id;
       },
       val: function(){
         return id;
       }
     };
  })();

  var uniqueEditID = (function() {
     var id = 0;
     function changeBy(val){
       id += val;
     }
     return{
       increment: function(){
         changeBy(1);
         return id;
       },
       decrement: function(){
         changeBy(-1);
         return id;
       },
       val: function(){
         return id;
       }
     };
  })();

  $('#freqarea-other-element').change(function(){
    if($(this).prop('checked') == true)
      $('#freqarea-other-text').prop('disabled',false);
    else {
      $('#freqarea-other-text').prop('disabled', true);
    }
  });

  $('#freqarea-other-new-element').change(function(){
    if($(this).prop('checked') == true)
      $('#freqarea-other-new-text').prop('disabled',false);
    else {
      $('#freqarea-other-new-text').prop('disabled', true);
    }
  });

  $('#sleepplace-other-element').change(function(){
    if($(this).prop('checked') == true)
      $('#sleepplace-other-text').prop('disabled',false);
    else {
      $('#sleepplace-other-text').prop('disabled', true);
    }
  });

  $('#sleepplace-other-new-element').change(function(){
    if($(this).prop('checked') == true)
      $('#sleepplace-other-new-text').prop('disabled',false);
    else {
      $('#sleepplace-other-new-text').prop('disabled', true);
    }
  });

  $('#streetact-other-element').change(function(){
    if($(this).prop('checked') == true)
      $('#streetact-other-text').prop('disabled',false);
    else {
      $('#streetact-other-text').prop('disabled', true);
    }
  });

  $('#activity-other-element').change(function(){
    if($(this).prop('checked') == true)
      $('#activity-other-text').prop('disabled',false);
    else {
      $('#activity-other-text').prop('disabled', true);
    }
  });

  $('input[type=radio][name=prof-religion]').change(function(){
    if($(this).val() == 'Other')
      $('#rel-other-text').prop('disabled',false);
    else {
      $('#rel-other-text').prop('disabled', true);
    }
  });

  $('input[type=radio][name=religion-new]').change(function(){
    if($(this).val() == 'Other')
      $('#rlgtext').prop('disabled',false);
    else {
      $('#rlgtext').prop('disabled', true);
    }
  });

  //province selection changed; localities list gets updated
  $('#addr4-new-element').change(function(){
    var prv = $(this).val(); //get material selected
    if (prv > 0){
      $.ajax({
        url: '../procs/procDynamicSelection.php',
        data:{'prvSelected': prv},
        type: 'POST',
        dataType: 'json',
        success: function(out){
          var len = out.length;
          $('#addr2-new-element').empty().prop('disabled', true).append('<option selected disabled>-- barangay --</option>');
          $('#addr3-new-element').empty().prop('disabled', false);
          $('#addr3-new-element').append("<option selected disabled>-- select a city/municipality --</option>");
          for(var i=0; i<len; i++){
            var id = out[i]['id'];
            var name = out[i]['name'];
            $('#addr3-new-element').append("<option value="+id+">"+name+"</option>");
          }
          $('#addr3-new-element').selectpicker('refresh');
          $('#addr2-new-element').selectpicker('refresh');
        },
        error: function(j, s, e){
          $.alert("Exception ("+s+") -- "+e,{closeTime: 5000,});
        }
      });
    }
    else{
      $('#addr3-new-element').selectpicker('refresh');
      $('#addr2-new-element').selectpicker('refresh');
    }
  });

  //updates barangay list when locality changes
  $('#addr3-new-element').change(function(){
    var loc = $(this).val(); //get material selected
    if (loc > 0){
      $.ajax({
        url: '../procs/procDynamicSelection.php',
        data:{'locSelected': loc},
        type: 'POST',
        dataType: 'json',
        success: function(out){
          var len = out.length;
          $('#addr2-new-element').empty().prop('disabled', false);
          $('#addr2-new-element').append("<option selected disabled>-- select a barangay --</option>");
          for(var i=0; i<len; i++){
            var id = out[i]['id'];
            var name = out[i]['name'];
            $('#addr2-new-element').append("<option value="+id+">"+name+"</option>");
          }
          $('#addr2-new-element').selectpicker('refresh');
        },
        error: function(j, s, e){
          $.alert("Exception ("+s+") -- "+e,{closeTime: 5000,});
        }
      });
    }
    else{
      $('#addr2-new-element').selectpicker('refresh');
    }
  });

  //province selection changed; localities list gets updated
  $('#baddr4-new-element').change(function(){
    var prv = $(this).val(); //get material selected
    if (prv > 0){
      $.ajax({
        url: '../procs/procDynamicSelection.php',
        data:{'prvSelected': prv},
        type: 'POST',
        dataType: 'json',
        success: function(out){
          var len = out.length;
          $('#baddr2-new-element').empty().prop('disabled', true).append('<option selected disabled>-- barangay --</option>');
          $('#baddr3-new-element').empty().prop('disabled', false);
          $('#baddr3-new-element').append("<option selected disabled>-- select a city/municipality --</option>");
          for(var i=0; i<len; i++){
            var id = out[i]['id'];
            var name = out[i]['name'];
            $('#baddr3-new-element').append("<option value="+id+">"+name+"</option>");
          }
          $('#baddr3-new-element').selectpicker('refresh');
          $('#baddr2-new-element').selectpicker('refresh');
        },
        error: function(j, s, e){
          $.alert("Exception ("+s+") -- "+e,{closeTime: 5000,});
        }
      });
    }
    else{
      $('#baddr3-new-element').selectpicker('refresh');
      $('#baddr2-new-element').selectpicker('refresh');
    }
  });

  //updates barangay list when locality changes
  $('#baddr3-new-element').change(function(){
    var loc = $(this).val(); //get material selected
    if (loc > 0){
      $.ajax({
        url: '../procs/procDynamicSelection.php',
        data:{'locSelected': loc},
        type: 'POST',
        dataType: 'json',
        success: function(out){
          var len = out.length;
          $('#baddr2-new-element').empty().prop('disabled', false);
          $('#baddr2-new-element').append("<option selected disabled>-- select a barangay --</option>");
          for(var i=0; i<len; i++){
            var id = out[i]['id'];
            var name = out[i]['name'];
            $('#baddr2-new-element').append("<option value="+id+">"+name+"</option>");
          }
          $('#baddr2-new-element').selectpicker('refresh');
        },
        error: function(j, s, e){
          $.alert("Exception ("+s+") -- "+e,{closeTime: 5000,});
        }
      });
    }
    else{
      $('#baddr2-new-element').selectpicker('refresh');
    }
  });

  $('#gaddr4-new-element').change(function(){
    var prv = $(this).val(); //get material selected
    if (prv > 0){
      $.ajax({
        url: '../procs/procDynamicSelection.php',
        data:{'prvSelected': prv},
        type: 'POST',
        dataType: 'json',
        success: function(out){
          var len = out.length;
          $('#gaddr2-new-element').empty().prop('disabled', true).append('<option selected disabled>-- barangay --</option>');
          $('#gaddr3-new-element').empty().prop('disabled', false);
          $('#gaddr3-new-element').append("<option selected disabled>-- select a city/municipality --</option>");
          for(var i=0; i<len; i++){
            var id = out[i]['id'];
            var name = out[i]['name'];
            $('#gaddr3-new-element').append("<option value="+id+">"+name+"</option>");
          }
          $('#gaddr3-new-element').selectpicker('refresh');
          $('#caddr2-new-element').selectpicker('refresh');
        },
        error: function(j, s, e){
          $.alert("Exception ("+s+") -- "+e,{closeTime: 5000,});
        }
      });
    }
    else{
      $('#caddr3-new-element').selectpicker('refresh');
      $('#caddr2-new-element').selectpicker('refresh');
    }
  });

  $('#gaddr3-new-element').change(function(){
    var loc = $(this).val(); //get material selected
    if (loc > 0){
      $.ajax({
        url: '../procs/procDynamicSelection.php',
        data:{'locSelected': loc},
        type: 'POST',
        dataType: 'json',
        success: function(out){
          var len = out.length;
          $('#gaddr2-new-element').empty().prop('disabled', false);
          $('#gaddr2-new-element').append("<option selected disabled>-- select a barangay --</option>");
          for(var i=0; i<len; i++){
            var id = out[i]['id'];
            var name = out[i]['name'];
            $('#gaddr2-new-element').append("<option value="+id+">"+name+"</option>");
          }
          $('#gaddr2-new-element').selectpicker('refresh');
        },
        error: function(j, s, e){
          $.alert("Exception ("+s+") -- "+e,{closeTime: 5000,});
        }
      });
    }
    else{
      $('#caddr2-new-element').selectpicker('refresh');
    }
  });

  //province selection changed; localities list gets updated
  $('#caddr4-element').change(function(){
    var prv = $(this).val(); //get material selected
    if (prv > 0){
      $.ajax({
        url: '../procs/procDynamicSelection.php',
        data:{'prvSelected': prv},
        type: 'POST',
        dataType: 'json',
        success: function(out){
          var len = out.length;
          $('#caddr2-element').empty().prop('disabled', true).append('<option selected disabled>-- barangay --</option>');
          $('#caddr3-element').empty().prop('disabled', false);
          $('#caddr3-element').append("<option selected disabled>-- select a city/municipality --</option>");
          for(var i=0; i<len; i++){
            var id = out[i]['id'];
            var name = out[i]['name'];
            $('#caddr3-element').append("<option value="+id+">"+name+"</option>");
          }
          $('#caddr3-element').selectpicker('refresh');
          $('#caddr2-element').selectpicker('refresh');
        },
        error: function(j, s, e){
          $.alert("Exception ("+s+") -- "+e,{closeTime: 5000,});
        }
      });
    }
    else{
      $('#caddr3-element').selectpicker('refresh');
      $('#caddr2-element').selectpicker('refresh');
    }
  });

  //province selection changed; localities list gets updated
  $('#baddr4-element').change(function(){
    var prv = $(this).val(); //get material selected
    if (prv > 0){
      $.ajax({
        url: '../procs/procDynamicSelection.php',
        data:{'prvSelected': prv},
        type: 'POST',
        dataType: 'json',
        success: function(out){
          var len = out.length;
          $('#baddr2-element').empty().prop('disabled', true).append('<option selected disabled>-- barangay --</option>');
          $('#baddr3-element').empty().prop('disabled', false);
          $('#baddr3-element').append("<option selected disabled>-- select a city/municipality --</option>");
          for(var i=0; i<len; i++){
            var id = out[i]['id'];
            var name = out[i]['name'];
            $('#baddr3-element').append("<option value="+id+">"+name+"</option>");
          }
          $('#baddr3-element').selectpicker('refresh');
          $('#baddr2-element').selectpicker('refresh');
        },
        error: function(j, s, e){
          $.alert("Exception ("+s+") -- "+e,{closeTime: 5000,});
        }
      });
    }
    else{
      $('#baddr3-element').selectpicker('refresh');
      $('#baddr2-element').selectpicker('refresh');
    }
  });

  //updates barangay list when locality changes
  $('#caddr3-element').change(function(){
    var loc = $(this).val(); //get material selected
    if (loc > 0){
      $.ajax({
        url: '../procs/procDynamicSelection.php',
        data:{'locSelected': loc},
        type: 'POST',
        dataType: 'json',
        success: function(out){
          var len = out.length;
          $('#caddr2-element').empty().prop('disabled', false);
          $('#caddr2-element').append("<option selected disabled>-- select a barangay --</option>");
          for(var i=0; i<len; i++){
            var id = out[i]['id'];
            var name = out[i]['name'];
            $('#caddr2-element').append("<option value="+id+">"+name+"</option>");
          }
          $('#caddr2-element').selectpicker('refresh');
        },
        error: function(j, s, e){
          $.alert("Exception ("+s+") -- "+e,{closeTime: 5000,});
        }
      });
    }
    else{
      $('#caddr2-element').selectpicker('refresh');
    }
  });

  //updates barangay list when locality changes
  $('#baddr3-element').change(function(){
    var loc = $(this).val(); //get material selected
    if (loc > 0){
      $.ajax({
        url: '../procs/procDynamicSelection.php',
        data:{'locSelected': loc},
        type: 'POST',
        dataType: 'json',
        success: function(out){
          var len = out.length;
          $('#baddr2-element').empty().prop('disabled', false);
          $('#baddr2-element').append("<option selected disabled>-- select a barangay --</option>");
          for(var i=0; i<len; i++){
            var id = out[i]['id'];
            var name = out[i]['name'];
            $('#baddr2-element').append("<option value="+id+">"+name+"</option>");
          }
          $('#baddr2-element').selectpicker('refresh');
        },
        error: function(j, s, e){
          $.alert("Exception ("+s+") -- "+e,{closeTime: 5000,});
        }
      });
    }
    else{
      $('#baddr2-element').selectpicker('refresh');
    }
  });

  $('#gaddr4-element').change(function(){
    var prv = $(this).val(); //get material selected
    if (prv > 0){
      $.ajax({
        url: '../procs/procDynamicSelection.php',
        data:{'prvSelected': prv},
        type: 'POST',
        dataType: 'json',
        success: function(out){
          var len = out.length;
          $('#gaddr2-element').empty().prop('disabled', true).append('<option selected disabled>-- barangay --</option>');
          $('#gaddr3-element').empty().prop('disabled', false);
          $('#gaddr3-element').append("<option selected disabled>-- select a city/municipality --</option>");
          for(var i=0; i<len; i++){
            var id = out[i]['id'];
            var name = out[i]['name'];
            $('#gaddr3-element').append("<option value="+id+">"+name+"</option>");
          }
          $('#gaddr3-element').selectpicker('refresh');
          $('#caddr2-element').selectpicker('refresh');
        },
        error: function(j, s, e){
          $.alert("Exception ("+s+") -- "+e,{closeTime: 5000,});
        }
      });
    }
    else{
      $('#caddr3-element').selectpicker('refresh');
      $('#caddr2-element').selectpicker('refresh');
    }
  });

  $('#gaddr3-element').change(function(){
    var loc = $(this).val(); //get material selected
    if (loc > 0){
      $.ajax({
        url: '../procs/procDynamicSelection.php',
        data:{'locSelected': loc},
        type: 'POST',
        dataType: 'json',
        success: function(out){
          var len = out.length;
          $('#gaddr2-element').empty().prop('disabled', false);
          $('#gaddr2-element').append("<option selected disabled>-- select a barangay --</option>");
          for(var i=0; i<len; i++){
            var id = out[i]['id'];
            var name = out[i]['name'];
            $('#gaddr2-element').append("<option value="+id+">"+name+"</option>");
          }
          $('#gaddr2-element').selectpicker('refresh');
        },
        error: function(j, s, e){
          $.alert("Exception ("+s+") -- "+e,{closeTime: 5000,});
        }
      });
    }
    else{
      $('#caddr2-element').selectpicker('refresh');
    }
  });

  //updates privileges choices depending on current account selected
  $('#select-account-element').change(function(){
    var val = $(this).val();
    if(val > 0){
      $.ajax({
        url: '../procs/procDynamicSelection.php',
        data:{'accSelected': val},
        type: 'POST',
        dataType: 'json',
        success: function(out){
          var len = out.length;
          $('#new-priv').empty();
          $('#new-priv').append("<option selected value=0 disabled>-- select new privilege --</option>");
          for(var i=0; i<len; i++){
            var id = out[i]['id'];
            var name = out[i]['name'];
            $('#new-priv').append("<option value="+id+">"+name+"</option>");
          }
          $('#new-priv').selectpicker('refresh');
        },
        error: function(j, s, e){
          $.alert("Exception ("+s+") -- "+e,{closeTime: 5000,});
        }
      });
    }
    else{
      $('#new-priv').selectpicker('refresh');
    }
  });

  //updates formEditIntake when a profile is selected
  $('#edit-item-element').change(function(){
    var val = $(this).val();
    if(val > 0){
      $.ajax({
        url: '../procs/procDynamicSelection.php',
        data:{'editprof': val},
        type: 'POST',
        dataType: 'json',
        success: function(out){
          var len = out.length;
          $('#edit-reset').prop('disabled', false);
          $('#name-old-element').val(out['name']);
          $('#name-new-element').prop('disabled', false);
          $('#alias-old-element').val(out['alias']);
          $('#alias-new-element').prop('disabled', false);
          $('#addr-old-element').val(out['address']);
          $('#addr1-new-element').prop('disabled', false);
          $('#addr4-new-element').prop('disabled', false).selectpicker('refresh');
          $('#age-old-element').val(out['age']);
          $('#age-new-element').prop('disabled', false);
          $('#gender-old-element').val(out['gender']);
          $('.gender-new-element').prop('disabled', false);
          $('#religion-old-element').val(out['religion']);
          $('input[type=radio][name=religion-new]').prop('disabled', false);
          $('#height-old-element').val(out['height']);
          $('#height-new-element').prop('disabled',false);
          $('#weight-old-element').val(out['weight']);
          $('#weight-new-element').prop('disabled',false);
          $('#childphone-old-element').val(out['child_phone']);
          $('#childphone-new-element').prop('disabled',false);
          $('#birthdate-old-element').val(out['birthdate']);
          $('#birthdate-new-element').prop('disabled',false);
          $('#birthplace-old-element').val(out['birthplace']);
          $('#baddr1-new-element').prop('disabled', false);
          $('#baddr4-new-element').prop('disabled', false).selectpicker('refresh');
          $('#birthplace-new-element').prop('disabled',false);
          $('#marks-old-element').val(out['distmarks']);
          $('#marks-new-element').prop('disabled',false);
          $('#education-old-element').val(out['edulevel']);
          $('#education-new-element').prop('disabled',false).selectpicker('refresh');
          $('#studying-old-element').val(out['studying']);
          $('input[type=radio][name=study-new]').prop('disabled', false);
          $('#year-old-element').val(out['yearattended']);
          $('#year-new-element').prop('disabled', false);
          $('#school-old-element').val(out['schoolstudied']);
          $('#school-new-element').prop('disabled', false);
          $('#nourishment-old-element').val(out['nourishment']);
          $('input[type=radio][name=nourishment-new]').prop('disabled', false);
          $('#family-old-element').val(out['family']);
          $('#add-new-element').prop('disabled', false);
          $('#edit-new-element').prop('disabled', false);
          $('#fatherstat-old-element').val(out['fatherstat']);
          $('input[type=radio][name=fatherstat-new]').prop('disabled', false);
          $('#motherstat-old-element').val(out['motherstat']);
          $('input[type=radio][name=motherstat-new]').prop('disabled', false);
          $('#childstat-old-element').val(out['childstat']);
          $('input[type=radio][name=childstat-new]').prop('disabled', false);
          $('#guardian-old-element').val(out['guardian']);
          $('#guardian-new-element').prop('disabled', false);
          $('#relguardian-old-element').val(out['relguardian']);
          $('#relguardian-new-element').prop('disabled', false).selectpicker('refresh');
          $('#gaddr-old-element').val(out['addrguardian']);
          $('#gaddr1-new-element').prop('disabled', false);
          $('#gaddr4-new-element').prop('disabled', false).selectpicker('refresh');
          $('#sleepplace-old-element').val(out['sleepareas']);
          $('input[type=checkbox][name=sleepplace\\[\\]').prop('disabled', false);
          $('#workarea-old-element').val(out['workareas']);
          $('input[type=checkbox][name=freqarea\\[\\]]').prop('disabled', false);
          $('#activity-old-element').val(out['activities']);
          $('input[type=checkbox][name=streetact\\[\\]]').prop('disabled', false);
          $('#reason-old-element').val(out['reason']);
          $('#reason-new-element').prop('disabled', false);
          $('#profilestat-old-element').val(out['profilestat']);
          $('input[type=radio][name=profilestat-new]').prop('disabled', false);
          $('#document-old-element').val(out['document']);
          $('input[type=checkbox][name=documents\\[\\]]').prop('disabled', false);
          $('#sacrament-old-element').val(out['sacrament']);
          $('input[type=checkbox][name=sacraments\\[\\]]').prop('disabled', false);
          $('#assessment-old-element').val(out['assessment']);
          $('#assessment-new-element').prop('disabled', false);
        },
        error: function(j, s, e){
          $.alert("Exception ("+s+") -- "+e,{closeTime: 5000});
        }
      });
    }
  });

  //family composition responses
  $('#add-fam-element').click(function(){
    $.ajax({
      url: '../procs/procDynamicSelection.php',
      data:{'selEdu': 1},
      type: 'POST',
      dataType: 'html',
      success: function(out){
        var html = '<li class="list-group-item"><input type="text" class="form-control" name="relativeName[]" placeholder="Enter relative name" required> <input type="text" class="form-control" name="relativeAge[]" placeholder="Enter relative age" required><input type="text" class="form-control" name="maritalStat[]" placeholder="Enter relative\'s marital status" required>'+ out +'<select class=\"form-control selectpicker seladdress2" name="relativeAddr2[]" data-live-search="true" data-size="5" disabled><option selected value=\"-1\" disabled>-- select city or municipality --</option></select><input type="text" class="form-control" name="relativeOccupation[]" placeholder="Enter relative\'s occupation" required><input type="text" class="form-control" name="relativeContact[]" placeholder="Enter relative\'s contact details" required><input type="text" class="form-control " name="relativeIncome[]" placeholder="Enter relative\'s monthly income" required><input type="text" class="form-control" name="relativeRemarks[]" placeholder="Enter some remarks" required><button type="button" class="btn btn-danger btn-sm remove">Cancel</button></li>';
        $('ul.fam-list').append(html);
        $('.selrelationship').selectpicker('refresh');
        $('.seleducation').selectpicker('refresh');
        $('.seladdress').selectpicker('refresh');
        $('.seladdress2').selectpicker('refresh');
      },
      error: function(j, s, e){
        $.alert("Exception ("+s+") -- "+e,{closeTime: 5000,});
      }
    });
  });

  $(document).on("change","select.seladdress",function(){
    if ($(this).val() != -1){
      var test = $(this).closest('li.list-group-item').find('select.seladdress2').get();
      $(test[0]).prop('disabled',false).selectpicker('refresh');

      $.ajax({
        url: '../procs/procDynamicSelection.php',
        data:{'selCity': 1, 'prv':$(this).val()},
        type: 'POST',
        dataType: 'html',
        success: function(html){
          $(test[0]).empty().append(html).selectpicker('refresh');
        },
        error: function(j, s, e){
          $.alert("Exception ("+s+") -- "+e,{closeTime: 5000,});
        }
      });
    }
  });

  $(document).on("click","button.remove",function(){
    var name = $(this).parent().eq(0).map(function(){ return this.id }).get();
    if(name[0].includes("addlist")){
      uniqueAddID.decrement();
    }
    if(name[0].includes('editlist')){
      uniqueEditID.decrement();
    }
    $(this).parent().eq(0).remove();
  });

  //family composition edit modal
  $('#add-new-element').click(function(){
    $.ajax({
      url: '../procs/procDynamicSelection.php',
      data:{'selEdu': 1},
      type: 'POST',
      dataType: 'html',
      success: function(out){
        var html = '<li class="list-group-item form-split" id="addlist-'+ uniqueAddID.increment() +'"><label class="control-label">Adding one more relative:</label><input type="text" class="form-control" name="relativeName[]" placeholder="Enter relative name" required> <input type="text" class="form-control" name="relativeAge[]" placeholder="Enter relative age" required><input type="text" class="form-control" name="maritalStat[]" placeholder="Enter relative\'s marital status" required>'+ out +'<select class=\"form-control selectpicker seladdress2" name="relativeAddr2[]" data-live-search="true" data-size="5" disabled><option selected value=\"-1\" disabled>-- select city or municipality --</option></select><input type="text" class="form-control" name="relativeOccupation[]" placeholder="Enter relative\'s occupation" required><input type="text" class="form-control" name="relativeContact[]" placeholder="Enter relative\'s contact details" required><input type="text" class="form-control " name="relativeIncome[]" placeholder="Enter relative\'s monthly income" required><input type="text" class="form-control" name="relativeRemarks[]" placeholder="Enter some remarks"><button type="button" class="btn btn-danger btn-sm remove">Cancel</button></li>';
        $('div#edit-area').append(html);
        $('.selrelationship').selectpicker('refresh');
        $('.seleducation').selectpicker('refresh');
        $('.seladdress').selectpicker('refresh');
      },
      error: function(j, s, e){
        $.alert("Exception ("+s+") -- "+e,{closeTime: 5000,});
      }
    });
  });

  $('#edit-new-element').click(function(){
    var html = "";
    var profile = $('#edit-item-element').val();
    $.ajax({
      url:'../procs/procDynamicSelection.php',
      data:{'selectFam': profile},
      type:'POST',
      dataType:'html',
      success: function(data){
        html += '<li class="list-group-item form-split" id="editlist-'+ uniqueEditID.increment() +'"><label class="control-label">Editing one existing relative:</label>'+data;
      },
      error: function(j, s, e){
        $.alert("Exception ("+s+") -- "+e,{closeTime: 5000,});
      }
    });
    $.ajax({
      url: '../procs/procDynamicSelection.php',
      data:{'selEdit': 1},
      type: 'POST',
      dataType: 'html',
      success: function(out){
        html += '<input type="text" class="form-control form-half" id="currentName" placeholder="select relative to update value" readonly><input type="text" class="form-control form-half" name="relativeNameEdit[]" placeholder="Enter relative name" disabled><input type="text" class="form-control form-half" id="currentAge" placeholder="select relative to update value" readonly><input type="text" class="form-control form-half" name="relativeAgeEdit[]" placeholder="Enter relative age" disabled><input type="text" class="form-control form-half" id="currentMaritalStatus" placeholder="select relative to update value" readonly><input type="text" class="form-control form-half" name="maritalStatEdit[]" placeholder="Enter relative\'s marital status" disabled>'+ out +'<input type="text" class="form-control form-half" id="currentAddress2" placeholder="select relative to update value" readonly><select class="form-half selectpicker seladdress2" name="relativeAddr2Edit[]" data-live-search="true" data-size="5"><option selected disabled>-- select city of municipality --</option><input type="text" class="form-control form-half" id="currentOccupation" placeholder="select relative to update value" readonly><input type="text" class="form-control form-half" name="relativeOccupationEdit[]" placeholder="Enter relative\'s occupation" disabled><input type="text" class="form-control form-half" id="currentContact" placeholder="select relative to update value" readonly><input type="text" class="form-control form-half" name="relativeContactEdit[]" placeholder="Enter relative\'s contact details" disabled><input type="text" class="form-control form-half" id="currentIncome" placeholder="select relative to update value" readonly><input type="text" class="form-control form-half" name="relativeIncomeEdit[]" placeholder="Enter relative\'s monthly income" disabled><input type="text" class="form-control form-half" id="currentRemarks" placeholder="select relative to update value" readonly><input type="text" class="form-control form-half" name="relativeRemarksEdit[]" placeholder="Enter some remarks" disabled><button type="button" class="btn btn-danger btn-sm remove">Cancel</button></li>';
        $('div#edit-area').append(html);
        $('li#editlist-'+uniqueEditID.val()+'>select.selrelative').selectpicker('refresh');
        $('li#editlist-'+uniqueEditID.val()+'>select[name*="Edit"]').prop('disabled', true).selectpicker('refresh');
      },
      error: function(j, s, e){
        $.alert("Exception ("+s+") -- "+e,{closeTime: 5000,});
      }
    });
  });

  $(document).on("change","select.selrelative",function(){
    var val = $(this).val();
    var id = $(this).parents("li").eq(0).map(function(){ return this.id; }).get();

    $.ajax({
      url: '../procs/procDynamicSelection.php',
      data:{'retrieveFamily': val},
      type: 'POST',
      dataType: 'json',
      success: function(out){
        $('li#'+id+'>input#currentName').val(out['name']);
        $('li#'+id+'>input#currentAge').val(out['age']);
        $('li#'+id+'>input#currentRelationship').val(out['relationship']);
        $('li#'+id+'>input#currentOccupation').val(out['occupation']);
        $('li#'+id+'>input#currentIncome').val(out['salary']);
        $('li#'+id+'>input#currentContact').val(out['contact']);
        $('li#'+id+'>input#currentRemarks').val(out['remarks']);
        $('li#'+id+'>input#currentMaritalStatus').val(out['maritalStat']);
        $('li#'+id+'>input#currentEducation').val(out['education']);
        $('li#'+id+'>input#currentAddress1').val(out['addr1']);
        $('li#'+id+'>input#currentAddress2').val(out['addr2']);
        $('li#'+id+'>input[name*="Edit"]').prop('disabled', false);
        var test = $('li#'+id).find('select[name*="Edit"]').get();
        for ($c=0; $c < 4; $c+=1){
          $(test[$c]).prop('disabled', false).selectpicker('refresh');
          console.log(test[$c]);
        }
      },
      error: function(j,s,e){}
    });
  });

  $('#education-element').change(function(){
    if(this.value == 1){
      $('input[type=radio][name=schoolingstatus]').filter('[value=No]').prop('checked',true);
      $('input[type=radio][name=schoolingstatus]').filter('[value=Yes]').prop('disabled', true);
      $('#year-element').val('N/A').prop('readonly', true);
      $('#school-element').val('N/A').prop('readonly', true);
    }else{
      if($('input[type=radio][name=schoolingstatus]').is('[disabled]')){
        $('input[type=radio][name=schoolingstatus]').filter('[value=Yes]').prop('disabled', false);
        $('input[type=radio][name=schoolingstatus]').filter('[value=Yes]').prop('checked',true);
      }
      if($('#year-element').val() == 'N/A'){
        $('#year-element').val('').prop('readonly', false);
      }
      if($('#school-element').val() == 'N/A'){
        $('#school-element').val('').prop('readonly', false);
      }
    }
  });

  $('#education-new-element').change(function(){
    $('input[type=radio][name=study-new]').prop('required', true);
    $('#year-new-element').prop('required', true);
    $('#school-new-element').prop('required', true);

    if(this.value == 1){
      $('input[type=radio][name=study-new]').filter('[value=No]').prop('checked',true);
      $('input[type=radio][name=study-new]').filter('[value=Yes]').prop('disabled', true);
      $('#year-new-element').val('N/A').prop('readonly', true);
      $('#school-new-element').val('N/A').prop('readonly', true);
    }else{
      if($('input[type=radio][name=study-new]').is('[disabled]')){
        $('input[type=radio][name=study-new]').filter('[value=Yes]').prop('disabled', false);
        $('input[type=radio][name=study-new]').filter('[value=Yes]').prop('checked',true);
      }
      if($('#year-new-element').val() == 'N/A'){
        $('#year-new-element').val('').prop('readonly', false);
      }
      if($('#school-new-element').val() == 'N/A'){
        $('#school-new-element').val('').prop('readonly', false);
      }
    }
  });

  $('input[type=radio][name=childparentage]').change(function(){
    if($(this).val() == 'Yes'){ //if child lives with parents, guardian equals parent
      $('#guardian-element').prop('disabled', true).prop('placeholder', "Make sure parent info is provided");
      $('#relguardian-element').prop('disabled', true).selectpicker('refresh');
      $('#gaddr1-element').prop('disabled', true).prop('placeholder', "Make sure parent's address is provided");
      $('#gaddr4-element').prop('disabled', true).selectpicker('refresh');
      toastr.info("Guardian information will be copied from one of the parents.");
    }else{
      $('#guardian-element').prop('disabled', false).prop('placeholder', "Enter child's current guardian");
      $('#relguardian-element').prop('disabled', false).selectpicker('refresh');
      $('#gaddr1-element').prop('disabled', false);
      $('#gaddr4-element').prop('disabled', false).selectpicker('refresh');
    }
  });
});
