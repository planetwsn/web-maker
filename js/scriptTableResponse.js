$(document).ready(function(){

  function validateCheckbox(){
    return $.ajax({
      url: '../procs/procDetailReveal.php',
      data:{'setcheck': 'true'},
      type: 'POST',
      dataType: 'html',
      success: function(data){
        //console.log(data);
      },
      error: function(j, s, e){
        sessionStorage.setItem('warn', "Exception ("+s+") -- "+e);
      }
    });
  }

  validateCheckbox().done(function(data){
    if(data == '1'){
      $("#active-view").bootstrapToggle('on');
    }else{
      $("#active-view").bootstrapToggle('off');
    }
  });

  $(document).on("click","tr.clickable", function(e){
    var values = $(this).find('td').map(function() {
        return $(this).text();
    });

    $.ajax({
      url: '../procs/procDetailReveal.php',
      data:{'allowview': 'true'},
      type: 'POST',
      dataType: 'html',
      success: function(data){
        if(data == '1'){
          displayDetails(values[0]);
        }
      },
      error: function(j, s, e){
        $.alert("Exception ("+s+") -- "+e,{closeTime: 5000,});
      }
    });
  });

  function displayDetails(val){
    $("ul.pagination").hide();
    $("table.table-striped").hide(); //hide profile list
    $.ajax({
      url: '../procs/procDetailReveal.php',
      data:{'selected': 'true', 'queryid': val},
      type: 'POST',
      dataType: 'html',
      success: function(str){
        $('#details').append(str);
      },
      error: function(j, s, e){
        $.alert("Exception ("+s+") -- "+e,{closeTime: 5000,});
      }
    });
  }

  function approvalReady() {
        return new Promise((resolve,reject)=>{

            var userAnswer = toastr.info("<button type='button' id='confirmationRevertYes' class='btn btn-info btn-sm btn-block'>Yes</button><button type='button' id='confirmationRevertNo' class='btn btn-info btn-sm btn-block'>No</button>",sessionStorage.getItem('check'),
              {
                closeButton: false,
                allowHtml: true
              });
            $('#confirmationRevertYes').click(function () {
                console.log('user clicked yes - he wants to delete the file')
                toastr.clear(userAnswer);
                resolve(true);
            });
            $('#confirmationRevertNo').click(function () {
                console.log('user clicked no - he doesnt want to delete the file')
                //Deleting info message from screen.
                toastr.clear(userAnswer);
                resolve(false);
            });
        });
    };

  $(document).on("click","tr.pending", function(e){
    var data = $(this).find('td').map(function() {
        return $(this).text();
    });
    sessionStorage.setItem('check', "Approve pending profile?");
    approvalReady().then(result => {
      if (result == true) {
        $.ajax({
          url: '../procs/procRegister.php',
          data:{'approved': 'true', 'queryid': data[0]},
          type: 'POST',
          dataType: 'html',
          success: function(){
            location.reload(true);
          },
          error: function(j, s, e){
          }
        });
      }
    });
    sessionStorage.removeItem('check');
  });

  $(document).on("click","button.remove-details",function(){
    $('#details').empty(); //remove details
    $("ul.pagination").show();
    $("table.table-striped").show(); //show hidden profile list
  });

  function settings_updated(data){
    return new Promise((resolve,reject)=>{
      $.ajax({
        url: '../procs/procDetailReveal.php',
        data:{'active': 'true', 'setting': data},
        type: 'POST',
        dataType: 'html',
        success: function(data){
          resolve(true);
        },
        error: function(j, s, e){
          reject(e);
        }
      });
    });
  };

  $(document).on("change","#active-view",function(){
    var data = 0;

    $('#details').empty(); //remove details
    $("ul.pagination").show();
    $("table.table-striped").show(); //show hidden profile list


    if($("#active-view").prop('checked') == true){
      data = 1;
    }else{
      data = 0;
    }

    settings_updated(data).then(result => {
      var urlParams = new URLSearchParams(window.location.search);
      if(!urlParams.get('search') && !urlParams.has('results')){
        $("#active-view").bootstrapToggle('enable');
        $("#main-table").load("../contents/contentProfileListMain.php"+"#main-table");
        if (data){
          sessionStorage.setItem('check', "Showing active profiles only");
          toastr.success(sessionStorage.getItem('check'));
          sessionStorage.removeItem('check');
        }else{
          sessionStorage.setItem('check', "Showing all profiles");
          toastr.success(sessionStorage.getItem('check'));
          sessionStorage.removeItem('check');
        }
      }else{
        $("#active-view").bootstrapToggle('disable');
      }
    });
  });

  $(document).on("click","#profile-picture-element", function(){
    $('#pp-preview').attr('src', $(this).attr('src'));
    $('#pp-modal').modal('show');
  });

  $(document).on("click","#fbody-picture-element", function(){
    $('#fb-preview').attr('src', $(this).attr('src'));
    $('#fb-modal').modal('show');
  });
});
