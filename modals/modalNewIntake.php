<div id="new-intake-modal" class="modal fade multi-step" role="dialog">

  <!-- modal content -->
  <div class="modal-dialog modal-md">
    <div class="modal-content">

      <!-- modal header -->
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">New Profile</h4>
        <h6 class="modal-title">Child Intake Form</h6>
      </div>

      <!-- modal body -->
      <div class="modal-body">
        <?php include($_SERVER['DOCUMENT_ROOT'].'/forms/formNewIntake.php'); ?>
      </div>

    </div>
  </div>

</div>
