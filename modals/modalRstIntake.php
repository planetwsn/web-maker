<div id="rstitem" class="modal fade" role="dialog">

  <!-- modal content -->
  <div class="modal-dialog modal-md">
    <div class="modal-content">

      <!-- modal header -->
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Reactivate Profile</h4>
        <h6 class="modal-title">Select a profile to reactivate</h6>
      </div>

      <!-- modal body -->
      <div class="modal-body">
        <?php include($_SERVER['DOCUMENT_ROOT'].'/forms/formRstIntake.php'); ?>
      </div>

    </div>
  </div>

</div>
