<div id="modal-photo-upload-element" class="modal fade" role="dialog">

  <!-- modal content -->
  <div class="modal-dialog modal-md">
    <div class="modal-content">

      <!-- modal header -->
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Upload Photo</h4>
      </div>

      <!-- modal body -->
      <div class="modal-body">
        <?php include($_SERVER['DOCUMENT_ROOT'].'/forms/formPhotoUpload.php'); ?>
      </div>

    </div>
  </div>

</div>
