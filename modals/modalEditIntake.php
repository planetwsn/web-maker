<div id="edititem" class="modal fade" role="dialog">

  <!-- modal content -->
  <div class="modal-dialog modal-md">
    <div class="modal-content">

      <!-- modal header -->
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Edit Profile</h4>
        <p>Elements in readonly are the current values saved in the edited profile</p>
      </div>

      <!-- modal body -->
      <div class="modal-body">
        <?php include($_SERVER['DOCUMENT_ROOT'].'/forms/formEditIntake.php'); ?>
      </div>

    </div>
  </div>

</div>
