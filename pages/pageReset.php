<?php include($_SERVER['DOCUMENT_ROOT'].'/procs/procSessionCheck.php'); ?>
<!DOCTYPE html>
<html lang="en">
<head>
  <?php include($_SERVER['DOCUMENT_ROOT'].'/headContent.php'); ?>
</head>

<body>
  <div class="container-fluid">
    <!-- header -->
    <div class="row">
      <nav class="navbar navbar-default navbar-fixed-top">
        <div class="container-fluid">
          <!-- Insert header style -->
          <?php include($_SERVER['DOCUMENT_ROOT'].'/headers/headerLogin.php'); ?>
        </div>
      </nav>
    </div>

    <!-- main body -->
    <div class="row" style="padding-top:60px;">
      <!-- Left-side column section-->
      <div class="col-sm-2 col-md-3"></div>

      <!-- Center column section-->
      <div class="col-sm-8 col-md-6">
        <!-- Validate input -->
        <?php
          if($_SERVER['REQUEST_METHOD'] == 'POST'){
            require($_SERVER['DOCUMENT_ROOT'].'/procs/procRegister.php');
          }
        ?>
        <h2 class="h2 text-center">Register</h2>
        <?php include($_SERVER['DOCUMENT_ROOT'].'/forms/formReset.php'); ?>
      </div>

      <!-- Right-side column section-->
      <div class="col-sm-2 col-md-3"></div>
    </div>

    <!-- Footer section-->
    <footer class="text-center row" style="padding-bottom:1px; padding-top:8px; margin-top:30px;">
      <?php include($_SERVER['DOCUMENT_ROOT'].'/footer.php'); ?>
      <?php include($_SERVER['DOCUMENT_ROOT'].'/notifications.php'); ?>
    </footer>
  </div>
</body>
