<div class="navbar-header">
  <!-- collapse button -->
  <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbarMain">
    <span class="icon-bar"></span>
    <span class="icon-bar"></span>
    <span class="icon-bar"></span>
  </button>
  <?php $img = "http://".$_SERVER['SERVER_NAME']."/img/logo.png" ?>
  <a class="navbar-brand" href="#" ><span><img src="<?php echo $img ?>" width="25px" height="25px" style="margin-top:-5px; margin-right:5px;" alt="pp.jpg"></img><?php include($_SERVER['DOCUMENT_ROOT'].'/websiteName.php'); ?></span></a>
</div>

<div class="collapse navbar-collapse" id="navbarMain">
  <ul class="nav navbar-nav">
    <?php include($_SERVER['DOCUMENT_ROOT'].'/navs/navMainMenu.php'); ?>
  </ul>

  <?php
    require_once($_SERVER['DOCUMENT_ROOT'].'/functions/funcAccountsMngt.php');
  ?>

  <ul class="nav navbar-nav navbar-right">
    <?php     if(isset($_SESSION['login-user'])){
      $imgLocation = $_SERVER['SERVER_NAME'].getAccountPhotoByAccountName($_SESSION['login-user'])."?".time(); ?>
    <li><a href="../pages/pageProfile.php"><span style="display:inline;">
      <img src="http://<?php echo $imgLocation ?>" width="25px" height="25px" style="margin-top:-5px; margin-right:5px; border-radius:25%" alt="pp.jpg"></img></span> Welcome, <?php echo $_SESSION['login-user']; ?>!</a></li>
    <li><a href="../procs/procLogin.php?logout"><span class="glyphicon glyphicon-log-out"></span> Logout</a></li>
  <?php }else{ ?>
    <li><a><span class="glyphicon glyphicon-user"></span> Welcome!</a></li>
    <li><a href="../pages/pageLogin.php"><span class="glyphicon glyphicon-log-in"></span> Login</a></li>
  <?php } ?>
  </ul>
</div>
