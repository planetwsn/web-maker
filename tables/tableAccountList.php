<?php
require_once($_SERVER['DOCUMENT_ROOT'].'/functions/funcDatabaseConnection.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/functions/funcGenericFunctions.php');

try{
  # setup pagination
  $con = getDatabaseConnection();
  if(isset($_GET['pageno'])){
    $pageno = $_GET['pageno'];
  }else{
    $pageno = 1;
  }
  $offset = ($pageno - 1)*10;
  $query = "SELECT COUNT(*) FROM account"; //replace database table name
  $result = mysqli_query($con, $query);
  $total_rows = mysqli_fetch_array($result);
  $total_pages = ceil($total_rows[0]/10);
  paginate($pageno, $total_pages);

  # print table head
  $colNames = array("Account Number","Account Name", "Email Address", "Contact Number", "Privilege"); //replace desired column names
  $widths=array(10, 40, 25, 15, 10); //change to desired widths
  printTableHead($colNames, $widths);


  # generate table content
  $query = "SELECT * from account ORDER BY account_id DESC LIMIT " . $offset .", 10";
  $ret = mysqli_query($con, $query);
  $nrows = mysqli_num_rows($ret);
  while($nrows > 0){
    $row = mysqli_fetch_array($ret);
    $query = "SELECT privilege_name FROM privilege WHERE privilege_id =" . $row['privilege_id'];
    $r1 = mysqli_query($con, $query);
    $prv = mysqli_fetch_array($r1);
    if ($row['privilege_id'] == 5){
      print_r("<tr class=\"pending\">");
    }else{
      print_r("<tr>");
    }
    print_r("<td>". $row['account_id'] ."</td>");
    print_r("<td>". $row['account_name']."</td>");
    print_r("<td>". $row['email'] . "</td>");
    print_r("<td>". $row['phone'] ."</td>");
    print_r("<td>". $prv['privilege_name']."</td>");
    print_r("</tr>");
    $nrows -= 1;
  }
  printTableFoot();
}catch(Exception $e){

}catch(Error $e){

}finally{
  mysqli_close($con);
}

?>
