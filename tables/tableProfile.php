<?php
require_once($_SERVER['DOCUMENT_ROOT'].'/functions/funcDatabaseConnection.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/functions/funcGenericFunctions.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/functions/funcAccountsMngt.php');

try{
  $con = getDatabaseConnection();
  $data = getAccountDetailsByAccountName($_SESSION['login-user']);
  $q = "SELECT * FROM privilege WHERE privilege_id=".$data['privilege_id'];
  $r = mysqli_query($con, $q);
  $pd = mysqli_fetch_array($r);

  $imgLocation = $_SERVER['SERVER_NAME'].$data['photo']."?".time();;

  print_r("<div class='row'>");
  #profile pic
  print_r("<div class='col-md-4 col-sm-4 col-xs-12' style='height:25vh; position:relative;margin-bottom:15%'>");
  print_r('<img src="http://'. $imgLocation.'" width="100%" height="100%" style="border-radius:5%; object-fit:cover; margin-top:10%;" alt="logo.jpg" id="profile-picture-element">');
  print_r('<button type="button" style="margin-top:2%;" class="btn btn-default btn-sm col-md-12 col-sm-12 col-xs-12" data-toggle="modal" data-target="#modal-photo-upload-element">Change Picture</button>');
  include($_SERVER['DOCUMENT_ROOT'].'/modals/modalPhotoUpload.php');
  print_r("</div>");
  print_r("<div class='col-md-8 col-sm-8 col-xs-12' style='position:relative;'>");
  $html = "";
  $html .= '
    <div class="table-responsive" style="margin-top:5%">
    <table class="table table-striped table-condensed table-bordered table-rounded table-sm">

    <tbody>
    <tr>
      <td>Name</td>
      <td>'.ucwords($data['account_name']).'</td>
    </tr>
    <tr>
      <td>Username</td>
      <td>'.$data['username'].'</td>
    </tr>
    <tr>
      <td>Password</td>
      <td>'.md5($data['password']).'</td>
    </tr>
    <tr>
      <td>Email</td>
      <td>'.$data['email'].'</td>
    </tr>
    <tr>
      <td>Phone</td>
      <td>'.$data['phone'].'</td>
    </tr>
    <tr>
      <td>Current Privilege</td>
      <td>'.$pd['privilege_name'].'</td>
    </tr>
    ';

  $html .=  '
    </tbody>
    </table>
    </div>
      ';
  print_r($html);
  print_r("</div>");

  #profile details

  print_r("</div>");
}catch(Exception $e){

}catch(Error $e){

}finally{
  mysqli_close($con);
}

?>
