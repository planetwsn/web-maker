<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">

<?php
include($_SERVER['DOCUMENT_ROOT'].'/siteTitle.php');
include($_SERVER['DOCUMENT_ROOT'].'/icon.php');

if($_SERVER['PHP_SELF'] == 'index.php'){
  echo '<link href="css/bootstrap.css" rel="stylesheet">';
  echo '<link href="css/bootstrap-select.css" rel="stylesheet">';
  echo '<link href="css/bootstrap-datepicker.standalone.css" rel="stylesheet">';
  echo '<link href="/../css/formsLayout.css" rel="stylesheet">';
  echo '<link href="css/toastr.css" rel="stylesheet">';
  echo '<script src="js/jquery.js"></script>';
  echo '<script src="js/bootstrap.js"></script>';
  echo '<script src="js/bootstrap-select.js"></script>';
  echo '<script src="js/bootstrap-datepicker.js"></script>';
  echo '<script src="js/toastr.js"></script>';
  echo '<script type="text/javascript" src="js/scriptResponsiveForms.js"></script>';
  echo '<script type="text/javascript" src="js/scriptTableResponse.js"></script>';
  echo '<script type="text/javascript" src="js/alert.js"></script>';
  echo '<script type="text/javascript" src="js/scriptFooter.js"></script>';
  echo '<script type="text/javascript" src="js/scriptButtons.js"></script>';
}else{
  echo '<link href="/../css/bootstrap.css" rel="stylesheet">';
  echo '<link href="/../css/bootstrap-select.css" rel="stylesheet">';
  echo '<link href="/../css/bootstrap-toggle.css" rel="stylesheet">';
  echo '<link href="/../css/bootstrap-datepicker.standalone.css" rel="stylesheet">';
  echo '<link href="/../css/fileinput.css" rel="stylesheet">';
  echo '<link href="/../css/formsLayout.css" rel="stylesheet">';
  echo '<link href="/../css/printLayout.css" rel="stylesheet">';
  echo '<link href="/../css/toastr.css" rel="stylesheet">';
  echo '<script src="/../js/jquery.js"></script>';
  echo '<script src="/../js/bootstrap.js"></script>';
  echo '<script src="/../js/bootstrap-select.js"></script>';
  echo '<script src="/../js/bootstrap-toggle.js"></script>';
  echo '<script src="/../js/bootstrap-datepicker.js"></script>';
  echo '<script src="/../js/fileinput.js"></script>';
  echo '<script src="/../js/scriptToast.js"></script>';
  echo '<script src="/../js/toastr.js"></script>';
  echo '<script type="text/javascript" src="/../js/scriptResponsiveForms.js"></script>';
  echo '<script type="text/javascript" src="/../js/scriptTableResponse.js"></script>';
  echo '<script type="text/javascript" src="/../js/alert.js"></script>';
  echo '<script type="text/javascript" src="/../js/scriptFooter.js"></script>';
  echo '<script type="text/javascript" src="/../js/scriptButtons.js"></script>';
  echo '<script type="text/javascript" src="/../js/scriptPicture.js"></script>';
}
?>
